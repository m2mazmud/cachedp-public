# CacheDP artifact

This artifact reproduces the DP inference engine in the following paper: 

> Mazmudar Miti, Humphries Thomas, Liu Jiaxiang, Rafuse Matthew, He Xi: Cache Me If You Can: Accuracy-Aware Inference Engine for Differentially Private Data Exploration. XXXX.

This artifact is in the form of two Docker containers. The `cachedp` container includes CacheDP, APEx, and Pioneer. The `apexsql` container runs a MySQL server. 

There are two reproducible modes: the fast mode and the full mode. The default mode is the fast mode. You may consider the hardware requirements to choose between the modes.  

## Hardware requirements and modes
These experiments have been reproduced on a machine with 1 TB RAM, 80 cores, 128 Gbps net. The fast mode takes 3 hours and the full mode takes 4-5 days. Both modes test all three systems mentioned above, and generate Figure 4a,4b,4c, 4f, Figure 5 and Table 2 from the paper. The full mode specification follows section 8.1.4 of the paper, whereas the fast mode has a smaller number of task runs and a smaller number of clients/queries in certain tasks. See the table below. (The IDEBench task runs different number of SQL queries in `dataset/IDEBench/sql_workflows/1n_1.sql`)

| Task - Dataset (Figure)      | Fast mode (3 runs)               | Full mode (100 runs)        |
|------------------------------|----------------------------------|-----------------------------|
| BFS, DFS - Adult (Fig 4a,4b) | 5 clients                        | 25 clients                  |
| RRQ - Synthetic (Fig 4c)     | 500 queries                      | 50,000 queries              |
| IDEBench - Planes (Fig 4f)   | 3 clients (1st 10 SQL)           | 10 clients (all SQL queries)|

We note that we do not provide the NYC Taxi dataset since the Lat/Long attributes have since been redacted. To replicate Fig 4d, 4e from the paper, first place a synthetic dataset with Lat/Long attributes in the dataset/ directory. Then run steps 1-2 as usual and run step 4 below as `./docker-attach.sh --location=1`. 

Details for our Pioneer implementation are under `pioneer/Implementation_Details.pdf` and our bugfixes for APEx are listed at the end of `apex/README.md`. 

## Run experiments 
1. Check if you have `docker` and `docker-compose`. If you don't, then you can install [Docker Desktop](https://www.docker.com/products/docker-desktop/). (If you are on a Windows host, you should install `winpty` and prefix these commands with `winpty bash`)
2. Run `./docker-build.sh`. This script should build and start both `cachedp` and `apexsql` containers. Run `docker-compose ps` and verify that there are two containers with these names in the last (NAMES) column. 
3. Run `./docker-attach.sh`. This script runs the SQL server in the `apexsql` container. It also runs the experiments in _fast_ mode through the Python script `run_all.py` in the `cachedp` container. After finishing the experiments, this script attaches your terminal to a running `cachedp` container for debugging and viewing results. **Note** to run the experiments in the full mode, you may pass the `--full_mode` flag to the script: `./docker-attach.sh --full_mode=1`
4. The `experiments_graphs` directory in the `cachedp` container contains the plots mentioned above, in a zip file named `results-<timestamp>.zip`. You may copy the zip file to your _host_ as follows: `cp experiment_graphs/results-<timestamp>.zip ./ && unzip results-<timestamp>.zip`. The unzipped folder should contain: `Fig<4a,4b,4c,4f,5>.png` and `Table2.csv` (readable by any user). 

## Interpreting results 
Due to the small number of runs in the default fast mode, the confidence intervals in the experiment graphs will be large. We set our run number as the seed offset for randomness to our DP algorithms, and so the full mode should fully reproduce the figures in the paper. To change this offset, you may pass the `--seed_offset` to the attach script: `./docker-attach.sh --seed_offset=42`.

