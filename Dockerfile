# syntax=docker/dockerfile:1
FROM python:3.10.5-bullseye

COPY ./requirements.txt /app/

WORKDIR /app

# For older versions of numpy and scipy in requirements.txt: apt-get install gfortran libblas3 liblapack3
RUN apt-get update && \
    apt-get install -y build-essential python3 python3-pip  && \
    pip install -r requirements.txt

COPY . /app

CMD tail -f /dev/null
