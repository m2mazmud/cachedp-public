import math
import numpy as np

def compute_naive_epsilon(target_variance):
    return math.sqrt(2 / target_variance)

# e.g. {5,6,7,8,9} -> '56789'
def set_to_str(range_query):
    res = ''
    for i in sorted(list(range_query)):
        res += str(i)
    return res

# check if query overlaps query set
def query_overlap(query_set, query):
    q_phi = set()
    # first loop to determine the union of past queries
    for q in query_set:
        query_range = q.query_range
        q_phi = q_phi.union(range_to_set(query_range))

    if q_phi.intersection(range_to_set(query.query_range)):
        # There is overlap
        return True
    else:
        return False

# check if 2 queries overlap
def is_overlap(q1, target_range):
    q1_range = q1.query_range
    if range_to_set(q1_range).intersection(range_to_set(target_range)):
        # overlaps!
        return True
    return False


# given query set and q_phi, compute q_phi \ query_set
def query_diff(query_set, q_phi):
    # print(q_phi)
    q_set = set()
    # first loop to determine the union of past queries
    for query in query_set:
        query_range = query.query_range
        q_set = q_set.union(range_to_set(query_range))
    compli_set = q_phi.difference(q_set)
    # print(set_to_range(compli_set), set_to_str(compli_set))
    return set_to_range(compli_set)


def from_matrix_to_tuple(workload: np.array):
    tuples = []
    for row in workload:
        starting_ind = np.where(row == 1)[0][0]
        ending_ind = starting_ind + int(sum(row))

        tuples.append((starting_ind, ending_ind))

    return tuples


def from_tuple_to_matrix(domain_size, tuples):
    matrix = []
    row = np.zeros(domain_size)
    for (starting_ind, ending_ind) in tuples:
        row[starting_ind:ending_ind] = 1

    matrix.append(row)

    return np.array(matrix)


def set_to_range(range_set):
    remaining_range = sorted(list(range_set))
    range_query, cur_range = [], []
    for ele in remaining_range:
        if len(cur_range) == 0 or cur_range[-1] == ele - 1:
            cur_range.append(ele)
            if remaining_range[-1] == ele:
                range_query.append((cur_range[0], cur_range[-1] + 1))
        else:
            range_query.append((cur_range[0], cur_range[-1] + 1))
            cur_range = [ele]
            if remaining_range[-1] == ele:
                range_query.append((cur_range[0], cur_range[-1] + 1))
    return range_query


def range_to_set(range_query):
    q_set = set()
    for query in range_query:
        for i in range(query[0], query[1], 1):
            q_set.add(i)
    return q_set

# compute the sum of variance
def query_sum(query_set):
    variance_sum = 0
    for query in query_set:
        if query.variance is None:
            continue
        else:
            variance_sum += query.variance
    return variance_sum


# sum (1/SUM_i)
def sum_recip_query_sets(query_sets):
    res = 0
    for query_set in query_sets:
        res += 1 / query_sum(query_set)
    return res


def print_query_set(query_sets):
    for query_set in query_sets:
        for query in query_set:
            print('The range of the query is: ', query.query_range)
            print('the variance of the query is: ', query.variance)
            print('the desired variance of the query is: ', query.desired_variance)
            print('the ts of the query is: ', query.logical_timestamp)
            print('the noisy answer of the query is: ', query.noisy_answer)
    print('-' * 50)

def print_cache(cache):
    past_queries = list(cache.cached_query.values())
    print_query_set([past_queries])
