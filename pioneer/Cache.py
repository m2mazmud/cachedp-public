from pioneer.PioneerQuery import Query

class PioneerCache(object):
    def __init__(self, data_vector):
        self.data_vector = data_vector
        self.cached_query = {}
        self.global_ts = 0

    # we will use the actual range tuple to index the query
    def set_cache(self, ind, query_range, noisy_answer, variance):
        self.global_ts += 1
        logical_timestamp = self.global_ts
        query = Query(variance,
                      query_range=query_range,
                      logical_timestamp=logical_timestamp,
                      noisy_answer=noisy_answer)
        self.cached_query[ind] = query

    def get_cache_size(self):
        return len(self.cached_query.keys())
