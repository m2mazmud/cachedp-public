class Query:
    def __init__(self, variance: float, query_range: tuple, logical_timestamp: int, noisy_answer: float):
        self.query_range = query_range
        self.variance = variance
        self.logical_timestamp = logical_timestamp
        self.noisy_answer = noisy_answer
        self.desired_variance = variance  # the variance we want to noise down to during execution
        self.true_answer = None
        self.score = None

    def set_desired_variance(self, target_variance):
        self.desired_variance = target_variance

    def set_noisy_answer(self, noisy_answer):
        self.noisy_answer = noisy_answer

    def set_true_answer(self, true_answer):
        self.true_answer = true_answer

    def set_variance(self, variance):
        self.variance = variance

    def set_score(self, score):
        self.score = score
