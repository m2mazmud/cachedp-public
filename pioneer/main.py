from pioneer.query_execution import *
from pioneer.query_preprocess import *
import sys
import time
import pandas as pd
sys.path.append('../')
from apex.run_query import *
from cachedp.algorithm.main import *
from cachedp.api.util_fns import *
import argparse
from cachedp.api.data_vector import *
from numpy import linalg as LA
import scipy.stats as st

v_diff = 10
sample_size = 10000

"""Estimate the cost of the strategy matrix A, using MC."""
def estimate_tight_variance(alpha, beta, cache, domain_size, query, v_min, rng):
    v_max = 100000000
    while (v_max - v_min) > v_diff:
        v = (v_max + v_min) / 2.0
        beta_e_adjust = estimate_beta_mc(alpha, beta, cache, domain_size, query, v, rng)

        if beta_e_adjust > beta: # Failure probability is higher than beta (not accurate enough)
            v_max = v
        else:
            v_min = v

    return v_min


def estimate_beta_mc(alpha, beta, cache, domain_size, query, v, rng):
    counter_fail = 0

    for i in range(sample_size):
        # if i == 0: print(len(query))
        error_vec = run_query_dryrun(cache, domain_size, [query[0]], v, rng, len(query))
        L_inf_norm = LA.norm(error_vec, np.inf)
        if L_inf_norm > alpha:
            counter_fail += 1

    # From APEx
    beta_e = 1.0 * counter_fail / sample_size
    conf_p = beta / 100.0
    zscore = st.norm.ppf(1.0 - conf_p / 2.0)
    delta_beta = zscore * np.sqrt(beta_e * (1.0 - beta_e) / sample_size)
    beta_e_adjust = beta_e + delta_beta + conf_p / 2

    return beta_e_adjust

def noise_down_operator(old_noises, old_b_inv, new_b_inv, rng):
    assert new_b_inv > old_b_inv
    new_noises = [] * len(old_noises)
    for old_lap_noise in old_noises:
        sign = np.sign(old_lap_noise)
        pdf = [old_b_inv / new_b_inv * np.exp((old_b_inv - new_b_inv) * abs(old_lap_noise)),
               (new_b_inv - old_b_inv) / (2.0 * new_b_inv),
               (old_b_inv + new_b_inv) / (2.0 * new_b_inv) * (
                           1.0 - np.exp((old_b_inv - new_b_inv) * abs(old_lap_noise)))]

        p = rng.random()
        z = rng.random()
        if p <= pdf[0]:
            new_noise = old_lap_noise

        elif p <= pdf[0] + pdf[1]:
            new_noise = np.log(z) / (old_b_inv + new_b_inv)
            new_noise = new_noise * sign

        elif p <= pdf[0] + pdf[1] + pdf[2]:
            new_noise = np.log(
                z * (np.exp(abs(old_lap_noise) * (old_b_inv - new_b_inv)) - 1.0) + 1.0) / (old_b_inv - new_b_inv)
            new_noise = new_noise * sign

        else:
            new_noise = abs(old_lap_noise) - np.log(1.0 - z) / \
                        (new_b_inv + old_b_inv)
            new_noise = new_noise * sign

        new_noises.append(new_noise)
    return new_noises

def answer_queries_pioneer(args, unmultiplied_alpha, alpha, infoTupleList, data_vector,
                           workload_tuples, cache, logger, rng, target_variance=None, is_correlated=False):
    start = time.time()
    sigma = alpha / math.sqrt(1/args.beta)
    v_min = sigma**2
    noisy_answers, true_answers = [], []
    eps_acc = []
    if target_variance is None:
        var_table = pickle.load(open('pioneer/variance_lookup.p', 'rb'))
    else: variance = target_variance
    for query in workload_tuples:
        if target_variance is None:
            assert round(unmultiplied_alpha, 3) in var_table.keys()
            variance = var_table[round(unmultiplied_alpha, 3)][len(workload_tuples)]
        true_response, noisy_answer, eps = run_query(cache, len(data_vector.data_vector), query[0], variance, rng)
        noisy_answers = np.concatenate((noisy_answers, noisy_answer))
        true_answers = np.concatenate((true_answers, true_response))
        eps_acc.append(eps)
    end = time.time()
    return np.array(true_answers), np.array(noisy_answers), eps_acc, '', 0, end - start, len(cache.cached_query), 0, 0

def run_query(cache, domain_size, query, target_variance, rng):
    past_queries = [x.query_range for x in cache.cached_query.values()]
    query_sets, q_n, v_prime, eps = build_query_plan(past_queries, query, cache, target_variance)
    noisy_answer = 0
    cur_ind = set_to_str(range_to_set(query))
    W = from_tuple_to_matrix(domain_size, query)
    true_response = W @ cache.data_vector
    if query_sets == -1:
        eps = compute_naive_epsilon(target_variance)
        b = math.sqrt(target_variance / 2)
        noisy_answer = true_response + rng.laplace(scale=b, size=len(true_response))
    elif cur_ind not in cache.cached_query.keys():
        noisy_answer, q_prime_noisy_answer = execute_query_plan(domain_size, cache.data_vector, query_sets, q_n, v_prime,
                                                                target_variance, query, rng)
    else:
        if cache.cached_query[cur_ind].variance <= target_variance:
            noisy_answer = cache.cached_query[cur_ind].noisy_answer
        else:
            old_noise = cache.cached_query[cur_ind].noisy_answer - true_response
            old_b_inv = 1 / math.sqrt(cache.cached_query[cur_ind].variance / 2)
            new_b_inv = 1 / math.sqrt(target_variance / 2)
            noisy_answer = noise_down_operator(old_noise, old_b_inv, new_b_inv, rng) + true_response
            # print('This is the noisy answer if we use noise down ', noisy_answer)

    cache.set_cache(cur_ind, query_range=query,
                    noisy_answer=noisy_answer,
                    variance=target_variance)

    # clear empty query if exists
    if '' in cache.cached_query.keys():
        del cache.cached_query['']
    # print_cache(cache)
    return true_response, noisy_answer, eps

def run_query_dryrun(cache, domain_size, query, target_variance, rng, q_len):
    past_queries = [x.query_range for x in cache.cached_query.values()]
    q_sets, q_n, v_prime, eps = build_query_plan(past_queries, query, cache, target_variance)
    query_sets = copy.copy(q_sets)
    noise = 0
    cur_ind = set_to_str(range_to_set(query))
    W = from_tuple_to_matrix(domain_size, query)
    true_response = W @ cache.data_vector
    if query_sets == -1:
        eps = compute_naive_epsilon(target_variance)
        b = math.sqrt(target_variance / 2)
        noise = rng.laplace(scale=b, size=q_len)
    elif cur_ind not in cache.cached_query.keys():
        noisy_response, q_prime_noisy_answer = execute_query_plan_dry(domain_size, cache.data_vector, query_sets, q_n, v_prime,
                                                                      target_variance, query, rng)
        noise = noisy_response - true_response
    else:
        b = math.sqrt(cache.cached_query[cur_ind].variance / 2)
        if cache.cached_query[cur_ind].variance <= target_variance:
            noise = rng.laplace(scale=b, size=len(true_response))
        else:
            old_noise = rng.laplace(scale=b, size=len(true_response))
            old_b_inv = 1 / b
            new_b_inv = 1 / math.sqrt(target_variance / 2)
            noise = noise_down_operator(old_noise, old_b_inv, new_b_inv, rng)
            # print('This is the noisy answer if we use noise down ', noise)

    return noise


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    # 1 means the module is enabled
    parser.add_argument('--save_data', type=int, default=0)
    parser.add_argument('--mmm', type=int, default=1)
    parser.add_argument('--relax_privacy', type=int, default=1)
    parser.add_argument('--strategy_expander', type=int, default=1)
    parser.add_argument('--proactive', type=int, default=1)
    parser.add_argument('--use_cache', type=int, default=1)
    parser.add_argument('--run_apex', type=int, default=0)
    parser.add_argument('--verbose', type=str, default='1')
    parser.add_argument('--arity', type=int, default=2)
    parser.add_argument('--alpha', type=float, default=0.001)
    parser.add_argument('--beta', type=float, default=0.05)
    parser.add_argument('--se_parameter', type=float, default=1)
    parser.add_argument('--max_strategy', type=float, default=500)
    parser.add_argument('--sorted_strategy', type=int, default=1)
    parser.add_argument('--parent_child', type=int, default=0)
    args = parser.parse_args()
    domain_size = 1000
    RUNS = 1
    df = pd.DataFrame(np.arange(0, domain_size), columns=['column1'])
    data_vector = DataVector(df, 'column1', 1)
    avg_costs, avg_naive_costs = [], []
    for i in range(RUNS):
        cache = PioneerCache(data_vector)
        cache_sys = init(data_vector)
        costs, naive_costs, dp_cache_costs = [], [], []
        rng = np.random.default_rng(seed=i)
        for run in range(10000):
            # print(run)
            s_start = int(rng.normal(domain_size / 2, 10))
            while s_start < 0 or s_start >= domain_size:
                s_start = int(rng.normal(domain_size / 2, 10))
            l_range = int(rng.normal(320, 10))
            while l_range < 0:
                l_range = int(rng.normal(320, 10))

            if s_start + l_range <= domain_size:
                query = [(s_start, s_start + l_range)]
            else:
                query = [(s_start, domain_size)]

            target_variance = rng.normal(250000, 25000)
            while target_variance <= 0:
                target_variance = rng.normal(250000, 25000)

            b = math.sqrt(target_variance / 2)
            alpha = (-b) * math.log(args.beta, math.e)
            # beta = math.exp(- args.alpha * domain_size / b)

            res1, eps_costs = run_query(cache, domain_size, query, target_variance, rng)
            res2, eps_dp_cache, _, _, _, _, _ = answer_queries(args, domain_size, query, alpha, args.beta, cache_sys, rng)
            if costs == []:
                costs.append(eps_costs)
                naive_costs.append(compute_naive_epsilon(target_variance))
                dp_cache_costs.append(eps_dp_cache)
            else:
                costs.append(costs[-1] + eps_costs)
                dp_cache_costs.append(dp_cache_costs[-1] + eps_dp_cache)
                naive_costs.append(naive_costs[-1] + compute_naive_epsilon(target_variance))
            if run == 79 or run == 399 or run == 1999 or run == 9999 or run == 49999:
                print(len(cache.cached_query.keys()))
                print(costs[-1], dp_cache_costs[-1])
        if len(avg_naive_costs) == 0:
            avg_costs = np.array(costs)
            avg_naive_costs = np.array(naive_costs)
            avg_dp_cache_costs = np.array(dp_cache_costs)
        else:
            avg_costs += np.array(costs)
            avg_naive_costs += np.array(naive_costs)
            avg_dp_cache_costs += np.array(dp_cache_costs)
    avg_costs, avg_naive_costs, avg_dp_cache_costs = avg_costs / RUNS, avg_naive_costs / RUNS, avg_dp_cache_costs / RUNS
    print(costs[-1])
    print(naive_costs[-1])
    x = [80, 400, 2000, 10000, 50000]
    y1 = [avg_costs[79], avg_costs[399], avg_costs[1999], avg_costs[9999], avg_costs[49999]]
    y2 = [avg_naive_costs[79], avg_naive_costs[399], avg_naive_costs[1999], avg_naive_costs[9999],
          avg_naive_costs[49999]]
    print(x)
    print(y1)
    print(y2)
    plt.plot(x, y1, color='b')
    plt.plot(x, y2, color='r')
    # plt.ylim(bottom=0)
    plt.xscale('log')
    plt.yscale('log')
    plt.show()
