from pioneer.PioneerQuery import Query
from pioneer.utils import *
import math

'''Group past queries into query sets, and build the composite plan'''

SMALL_VALUE = 1e-5

# range_queries are queries in the cache
# returns q_phi and q_n
# each query can be written as [(range1), (range2)]
#TH you pass range queries but do not use it as you have the cache
def gen_query_sets(range_queries, q_range, cache, target_variance, k=4):
    in_cache = False
    q_n = None
    past_queries = list(cache.cached_query.values())
    # print(past_queries)
    # print(set_to_str(range_to_set(q_range)))
    if len(past_queries) == 0:
        return -1, -1, -1, in_cache
    elif set_to_str(range_to_set(q_range)) in cache.cached_query.keys():
        if target_variance > cache.cached_query[set_to_str(range_to_set(q_range))].variance:
            # print('Found query in cache')
            return 0, cache.cached_query[set_to_str(range_to_set(q_range))], 0, True
        else:
            in_cache = True
            q_n = cache.cached_query[set_to_str(range_to_set(q_range))]
    q_phi = set()
    # take the top k most accurate queries
    past_queries.sort(key=lambda x: x.variance)
    past_queries = past_queries[:k]
    for query in past_queries:
        query_range = query.query_range
        q_phi = q_phi.union(range_to_set(query_range))

    # q_n = q_phi - q'
    q_set = range_to_set(q_range)
    q_phi = q_phi.union(q_set)
    if not in_cache:
        q_n = q_phi.difference(q_set)
        # set q_n in the cache if it does not exist there
        q_n = Query(variance=None,
                    query_range=set_to_range(q_n),
                    logical_timestamp=cache.global_ts + 1,
                    noisy_answer=None)
    # Determine each query sets

    past_queries.sort(key=lambda x: x.logical_timestamp)
    query_sets = []
    # second loop to determine the each query sets
    # #TH why is there a second loop??
    # create query sets where each set contains queries of the same variance
    # print(q_phi)
    # print(len(past_queries))
    while len(past_queries) > 0:
        cur_query_set = []
        # print(len(past_queries))
        for i in range(len(past_queries)):
            # print(cur_query_set)
            # Last query itself forms a set
            if len(cur_query_set) == 0 and i == len(past_queries) - 1:
                # last query itself is a query set, along with its compliment
                # set the timestamp to be the same
                compli_range = query_diff(past_queries, q_phi)
                compli_query = Query(variance=past_queries[i].variance,
                                     query_range=compli_range,
                                     logical_timestamp=past_queries[i].logical_timestamp,
                                     noisy_answer=None)
                # append the query in query set
                query_sets.append([past_queries[i], compli_query])
                past_queries = past_queries[i + 1:]
                break
            # last query along with previous queries form a set
            elif cur_query_set and \
                    past_queries[i].logical_timestamp == cur_query_set[-1].logical_timestamp and \
                    i == len(past_queries) - 1:
                if query_overlap(cur_query_set, past_queries[i]):
                    # if overlaps, we skip this query, i.e. discard it
                    continue
                compli_range = query_diff(cur_query_set, q_phi)
                compli_query = Query(variance=cur_query_set[-1].variance,
                                     query_range=compli_range,
                                     logical_timestamp=cur_query_set[-1].logical_timestamp,
                                     noisy_answer=None)
                # append the query in query set
                query_sets.append(cur_query_set + [compli_query])
                past_queries = past_queries[i + 1:]
                break
            # This is the case where we append the current query to current query set
            elif len(cur_query_set) == 0 or \
                    past_queries[i].logical_timestamp == cur_query_set[-1].logical_timestamp:
                if query_overlap(cur_query_set, past_queries[i]):
                    # if overlaps, we skip this query, i.e. discard it
                    continue
                cur_query_set.append(past_queries[i])
            # current query is not at the same timestamp
            else:
                compli_range = query_diff(cur_query_set, q_phi)
                compli_query = Query(variance=cur_query_set[-1].variance,
                                     query_range=compli_range,
                                     logical_timestamp=cur_query_set[-1].logical_timestamp,
                                     noisy_answer=None)
                # append the query in query set
                query_sets.append(cur_query_set + [compli_query])
                cur_query_set = []
                past_queries = past_queries[i:]
                break
    # print_query_set(query_sets)
    return q_phi, q_n, query_sets, in_cache


# Note there is weights here, but they are encoded in the formulas
# v_sp = 1 / (sum (1 / sum_i))
def get_variance_sp(query_sets):
    v_sp_r = 0
    for query_set in query_sets:
        query_set_sum = query_sum(query_set)
        v_sp_r += 1 / query_set_sum
    return 1 / v_sp_r

# returns v'
def solve_v_prime(query_sets, target_variance):
    # print(query_sets)
    v, v_sp = target_variance, get_variance_sp(query_sets)
    v_prime = np.Inf
    if v < v_sp:
        v_prime = (-(v_sp - 2*v) + math.sqrt(v_sp**2 + 4 * v**2)) / 2
    # print("v, v_sp, v_prime: ", v,v_sp,v_prime)
    # print((-(v_sp - 2*v) + math.sqrt(v_sp**2 + 4 * v**2)) / 2)
    return (-(v_sp - 2*v) + math.sqrt(v_sp**2 + 4 * v**2)) / 2

# Compute the score of queries in the query set
# mutate the query structure in query sets
# query_sets is the list of query sets without q_n
def compute_query_scores(query_sets, q_n, v_prime, target_variance):
    # set all variance to be the actual variance of the query
    # this is handled by the structure of the queires
    # Compute the scores for each query set
    v_sp = get_variance_sp(query_sets)
    v_plus = v_prime + v_sp
    for query_set in query_sets:
        query_set_sum = query_sum(query_set)
        query_set_score = (target_variance / v_plus) ** 2 * (v_sp / query_set_sum) ** 2
        for query in query_set:
            query.set_score(query_set_score)

    q_n_score = (target_variance / v_prime) ** 2 * v_prime * math.sqrt(2 * v_prime)
    q_n.set_score(q_n_score)
    # rank query_set by scores
    query_sets.sort(key=lambda x: x[0].score, reverse=True)
    return query_sets

# Given a query set
def compute_noise_down(query_sets, q_n, v_prime, target_variance):
    v_sp = get_variance_sp(query_sets)
    v_x_star = (v_prime * v_sp - target_variance * v_sp - target_variance * v_prime) \
              / (target_variance - v_prime)
    q_n.set_desired_variance(v_x_star)
    composite_epsilon = compute_epsilon(query_sets, q_n, v_prime)
    naive_epsilon = math.sqrt(2 / v_prime)
    return composite_epsilon


# the current variance of q_n is v', which is what we pay for
def compute_epsilon(query_sets, v_prime):
    num_noise_down_sets = 0  # use this variable as a sanity check
    epsilon_costs = 0
    for query_set in query_sets:
        query = query_set[0]
        if query.desired_variance + SMALL_VALUE < query.variance:
            epsilon_costs += math.sqrt(2/query.desired_variance) - math.sqrt(2/query.variance)
            num_noise_down_sets += 1
    assert num_noise_down_sets < 1
    # pay for q'
    epsilon_costs += math.sqrt(2 / v_prime)  # v'
    return epsilon_costs


# the function that groups all the steps above
def build_query_plan(range_queries, q, cache, target_variance):
    q_phi, q_n, query_sets, in_cache = gen_query_sets(range_queries, q, cache, target_variance)
    if q_phi == -1:
        # cache is empty
        return -1, -1, -1, -1
    elif q_phi == 0:
        # the query is in cache
        cache_variance = cache.cached_query[set_to_str(range_to_set(q))].variance
        return 0, q_n, 0, 0

    v_prime = solve_v_prime(query_sets, target_variance)
    if in_cache:
        if v_prime > q_n.variance:
            eps = 0
        else:
            eps = math.sqrt(2 / v_prime) - math.sqrt(2 / q_n.variance)
        return 0, q_n, 0, eps
    # set the variance for q_n
    q_n.set_desired_variance(v_prime)
    # update the cache
    # eps = compute_noise_down(query_sets, q_n, v_prime, target_variance)
    eps = math.sqrt(2 / v_prime)
    return query_sets, q_n, v_prime, eps



if __name__ == '__main__':
    target_variance = 0.05
    range_queries = [[(1, 5)], [(5, 7)], [(1, 3)], [(3, 7)]]
    variance = [0.1, 0.2, 0.3, 0.4]
    q_range = [(2, 6)]
    cache = Cache()
    for i in range(len(range_queries)):
        cache.set_cache(ind=set_to_str(range_to_set(range_queries[i])),
                        query_range=range_queries[i],
                        noisy_answer=None, variance=variance[i])
    build_query_plan(range_queries, q_range, cache, target_variance)
