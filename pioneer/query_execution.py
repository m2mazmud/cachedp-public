from pioneer.query_preprocess import *
import copy

def compute_noisy_answer(query, domain_size, v_prime, data_vector, rng, is_q=False, q=None):
    noisy_response, q_prime_noisy_response = None, None
    W = from_tuple_to_matrix(domain_size, query.query_range)
    true_response = W @ data_vector
    if query.noisy_answer is not None:
        noisy_response = query.noisy_answer
    else:
        b = math.sqrt(query.desired_variance / 2)
        query.set_true_answer(true_response)
        if query.query_range == []:
            noisy_response = true_response
        else:
            noisy_response = true_response + rng.laplace(scale=math.sqrt(query.desired_variance / 2), size=len(true_response))
    if is_q:
        b = math.sqrt(v_prime / 2)
        q_prime_true_response = from_tuple_to_matrix(domain_size, q) @ data_vector
        q_prime_noisy_response = from_tuple_to_matrix(domain_size, q) @ data_vector + \
                                 rng.laplace(scale=b, size=len(q_prime_true_response))
    return noisy_response, q_prime_noisy_response

def compute_noisy_answer_dry(query, domain_size, v_prime, data_vector, rng, is_q=False, q=None):
    noisy_response, q_prime_noisy_response = None, None
    W = from_tuple_to_matrix(domain_size, query.query_range)
    true_response = W @ data_vector
    if query.noisy_answer is not None:
        b = math.sqrt(query.variance / 2)
        noisy_response = rng.laplace(scale=b, size=len(true_response))
    else:
        b = math.sqrt(query.desired_variance / 2)
        query.set_true_answer(true_response)
        if query.query_range == []:
            noisy_response = 0
        else:
            noisy_response = rng.laplace(scale=b, size=len(true_response))
    if is_q:
        b = math.sqrt(v_prime / 2)
        q_prime_true_response = from_tuple_to_matrix(domain_size, q) @ data_vector
        q_prime_noisy_response = rng.laplace(scale=b, size=len(q_prime_true_response))
    return noisy_response, q_prime_noisy_response

# use the current result for each query
def compute_query_result(query_sets, q_n, target_variance, q_prime_noisy_answer):
    query_set_sum = None
    variance_sp = get_variance_sp(query_sets)
    for query_set in query_sets:
        cur_sum = None
        for query in query_set:
            if cur_sum is None:
                cur_sum = copy.copy(query.noisy_answer)
            else:
                cur_sum += copy.copy(query.noisy_answer)
        # compute weights for weighted average
        cur_weight = variance_sp / query_sum(query_set)
        if query_set_sum is None:
            query_set_sum = cur_weight * cur_sum
        else:
            query_set_sum += cur_weight * cur_sum
    subplan_weight = target_variance / (variance_sp + q_n.desired_variance)
    # print(query_set_sum, q_n.noisy_answer, q_prime_noisy_answer)
    noisy_res = subplan_weight * (query_set_sum - q_n.noisy_answer) + \
                (1 - subplan_weight) * q_prime_noisy_answer
    return noisy_res

def execute_query_plan_dry(domain_size, data_vector, query_sets, q_n, v_prime, target_variance, q_range, rng):
    for query_set in query_sets:
        for query in query_set:
            noisy_response, _ = compute_noisy_answer_dry(query, domain_size, v_prime, data_vector, rng)
            query.set_noisy_answer(noisy_response)
    noisy_response, q_prime_noisy_answer = compute_noisy_answer_dry(q_n, domain_size, v_prime,
                                                                    data_vector, rng, is_q=True, q=q_range)
    q_n.set_noisy_answer(noisy_response)
    noisy_answer = compute_query_result(query_sets, q_n, target_variance, q_prime_noisy_answer)
    return noisy_answer, q_prime_noisy_answer

def execute_query_plan(domain_size, data_vector, query_sets, q_n, v_prime, target_variance, q_range, rng):
    for query_set in query_sets:
        for query in query_set:
            noisy_response, _ = compute_noisy_answer(query, domain_size, v_prime, data_vector, rng)
            query.set_noisy_answer(noisy_response)
    noisy_response, q_prime_noisy_answer = compute_noisy_answer(q_n, domain_size, v_prime,
                                                                data_vector, rng, is_q=True, q=q_range)
    q_n.set_noisy_answer(noisy_response)
    noisy_answer = compute_query_result(query_sets, q_n, target_variance, q_prime_noisy_answer)
    return noisy_answer, q_prime_noisy_answer


if __name__ == '__main__':
    target_variance = 0.05
    # b = math.sqrt(target_variance / 2)
    domain_size = 10
    range_queries = [[(1, 5)], [(5, 7)], [(1, 3)], [(3, 7)]]
    variance = [0.1, 0.2, 0.3, 0.4]
    q_range = [(2, 6)]
    data_vector = np.arange(0, domain_size)
    cache = PioneerCache()
    for i in range(len(range_queries)):
        cache.set_cache(ind=set_to_str(range_to_set(range_queries[i])),
                        query_range=range_queries[i],
                        noisy_answer=None, variance=variance[i])
    query_sets, q_n, v_prime, eps = build_query_plan(range_queries, q_range, cache, target_variance)
    # if all 3 args above are None, the case where there is nothing in cache
    for run in range(1):
        rng = np.random.default_rng(seed=run)
        # print(rng.laplace(scale=b, size=20))
        execute_query_plan(cache, domain_size, data_vector, query_sets, q_n, v_prime, target_variance, q_range, rng)
