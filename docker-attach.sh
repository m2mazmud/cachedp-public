#!/usr/bin/env bash

docker exec -i apexsql mysql -uroot -prootPass < dataset/schema_setup.sql
docker-compose exec cachedp python3 run-all.py "$@"
docker-compose exec cachedp bash
