from cachedp.api.local_types import *

SMALL_VALUE = 10e-8
# get the range tuple of query given domain and granularity
# Note we are sure this data vector supports the workload
def get_range_tuple(d_max, d_min, granularity):
    possible_endpoints, cur_endpoint = [], d_min
    while cur_endpoint + granularity <= d_max + SMALL_VALUE:
        possible_endpoints.append(round(cur_endpoint, 3))
        cur_endpoint += granularity
    possible_endpoints.append(d_max)
    return np.array(possible_endpoints)

def matrix_to_index_set(workload: np.array) -> list():
    indices_list = []
    for row in workload:
        index_set = set(np.where(row == 1)[0])
        indices_list.append(index_set)
    return indices_list
