
#from __future__ import annotations
from binarytree import Node
from typing import List, Tuple
from cachedp.api.Cache.cache import Cache
from cachedp.api.RangeTupleHelpers import match, intersect, get_children_ranges_helper
from cachedp.api.local_types import Marginal, RangeTuple, NamedMarginalsList


class KaryTreeNode(Node):
    children: List["KaryTreeNode"]

    def __init__(self, start: int, end: int, attribute_name: str,
                 parent_attribute_names: List[str],
                 parent_attribute_range: Marginal):
        super().__init__(value=0)
        # attribute name is the current splitting attribute
        self.attribute_name = attribute_name
        self.parent_attribute_names = parent_attribute_names
        self.end = end
        self.start = start
        self.max_tree_sensitivity = 0
        self.children = []
        self.parent_attribute_range : Marginal = parent_attribute_range
        self.children_are_next_marginal = False
        self.next_attribute_range = (-1, -1)
        self.next_attribute_name = ''

    def set_next_attribute_range_name(self, m: RangeTuple, name: str):
        self.next_attribute_range = m
        self.next_attribute_name = name

    def get_children_ranges(self, arity, min_bucket_width):
        return get_children_ranges_helper(arity, min_bucket_width, self.start, self.end)

    def get_children(self):
        return self.children

    def add_subtree_for_attribute(self, subtree_root): # KaryTreeNode):
        # Only add children to leaves of parent attribute.
        assert (self.children == [])
        self.children = subtree_root.get_children()
        self.children_are_next_marginal = True

    def get_own_attribute_leaves(self, skip_self: bool = False):
        children = self.get_children()
        if not skip_self:
            if not children or self.children_are_next_marginal:
                return [self]

        leaves = []
        for child in children:
            child_leaves = child.get_own_attribute_leaves()
            leaves.extend(child_leaves)

        return leaves

    def update_max_tree_sensitivity(self) -> int:
        if self.value == 1:
            self.max_tree_sensitivity += 1

        #check_only_own_attribute=False as we want to iterate over the entire tree, across all attributes.
        #children_subtree_sensitivities = self.run_function_on_all_children(False, self.__update_max_tree_sensitivity)
        children_subtree_sensitivities = []
        children = self.get_children()
        for child in children:
            child_sensitivity = child.update_max_tree_sensitivity()
            # print(child.attribute_name, child.start, child.end, child_sensitivity)
            children_subtree_sensitivities.append(child_sensitivity)
            # print(children_subtree_sensitivities)
        if children:
            self.max_tree_sensitivity += max(children_subtree_sensitivities)
        return self.max_tree_sensitivity

    def _generate_full_marginal_attribute_ranges_names(self) \
        -> Tuple[Marginal, List[str]]:
        attribute_names = self.parent_attribute_names + [self.attribute_name]
        own_full_marginal = self.parent_attribute_range + [(self.start, self.end)]
        if self.children_are_next_marginal:
            own_full_marginal = own_full_marginal + [self.next_attribute_range]
            attribute_names = attribute_names + [self.next_attribute_name]
        return own_full_marginal, attribute_names

    def get_proactive_for_subtree_at_node(self, sensitivity: int, cache: Cache) -> \
            NamedMarginalsList:
        # This algorithm never calls itself recursively when remaining_sensitivity is 0.
        # So this condition is only hit when the strategy is an empty list, and the input is called on the root.
        if sensitivity == 0:
            return []

        own_subtree_sensitivity = self.max_tree_sensitivity
        remaining_sensitivity = sensitivity
        own_proactive = list()

        own_full_marginal, attribute_names = self._generate_full_marginal_attribute_ranges_names()
        # Node is not included in current query and
        if self.value == 0:
            (free_paid_vector, _) = cache.get_entries_plain([[1]], attribute_names, own_full_marginal)
            if sensitivity > own_subtree_sensitivity and free_paid_vector[0] == 1:
                # we can include it as it does not increase overall sensitivity.
                own_proactive = [(own_full_marginal, attribute_names)]
                remaining_sensitivity -= 1
            # Else case here: Node is not included in current query and
            # we cannot include it as it would increase overall sensitivity.
            # We do not decrease the sensitivity with which its children are called.
        else:  # Node is already included in current query.
            remaining_sensitivity -= 1

        if remaining_sensitivity > 0:
            children = self.get_children()
            for child in children:
                child_proactive_list = child.get_proactive_for_subtree_at_node(remaining_sensitivity, cache)
                own_proactive.extend(child_proactive_list)
        return own_proactive


    def check_marked_nodes(self):
        # if self.value == 1:
        #     if not self.children_are_next_marginal:
        #         print(self.start, self.end, self.attribute_name, self.parent_attribute_names, self.parent_attribute_range)
        #     else:
        #         print(self.next_attribute_range, self.attribute_name, self.parent_attribute_names, self.parent_attribute_range)
        own_full_marginal, own_attribute_names = self._generate_full_marginal_attribute_ranges_names()
        if self.value == 1:
            print("marked", own_full_marginal, own_attribute_names)
        for child in self.get_children():
            child.check_marked_nodes()

    def _mark_node(self, value: int):
        self.value = value
        if value == 0:
            self.max_tree_sensitivity = 0

    def mark_nodes_in_subtree(self, m: RangeTuple, value: int):
        if self.children_are_next_marginal:
            comparison_range = self.next_attribute_range
        else:
            comparison_range = (self.start, self.end)
        matching, _ = match(comparison_range, [m])
        if matching:
            self._mark_node(value)

        children = self.get_children()
        for child in children:
            child_range = (child.start, child.end)
            intersection = intersect(m, child_range)
            if intersection:
                child.mark_nodes_in_subtree(m, value)
