import math
from typing import Tuple, List, TypeVar
from cachedp.api.helpers_sql_tuple import get_range_tuple
import itertools
T = TypeVar('T')      # Declare type variable

from cachedp.api.local_types import RangeTuple, RangeTupleList, Marginal


def match(given_tuple: RangeTuple, l: RangeTupleList) -> Tuple[bool, int]:
    if given_tuple in l:
        return True, l.index(given_tuple)
    return False, -1

def match_marginals(given_marginal: Marginal, l:List[Marginal]):
    if given_marginal in l:
        return True
    return False

def intersect(l: RangeTuple, n: RangeTuple):
    (ls, le) = l
    (ns, ne) = n

    if ls < ne and le > ns:
        return max(ls, ns), min(le, ne)
    else:
        return None

def intersect_areas(workload_lat_long: Marginal, node_lat_long: Marginal):
    intersected_lat_long = []
    assert(len(workload_lat_long) == len(node_lat_long))
    for workload_tuple, node_tuple in zip(workload_lat_long, node_lat_long):
        intersection = intersect(workload_tuple, node_tuple)
        if not intersection:
            return None
        intersected_lat_long.append(intersection)
    return intersected_lat_long

def sort_ranges(l: RangeTupleList):
    end_points_descending = sorted(l, key=lambda tuple: tuple[1], reverse=True)
    starting_points_ascending = sorted(
        end_points_descending, key=lambda tuple: tuple[0])
    return starting_points_ascending

def get_children_ranges_helper(arity, min_bucket_width, start, end):
    range_tuples = get_range_tuple(end, start, min_bucket_width)

    current_width = len(range_tuples) - 1
    children_width = int(current_width / arity)

    children_ranges: RangeTupleList = []
    # Can have children nodes
    if children_width >= 1 or (children_width == 0 and current_width > 1):
        # Define ranges of children nodes for this node
        for k in range(0, arity):
            range_start = k * max(children_width, 1)
            if k == arity - 1:
                range_end = current_width
            else:
                range_end = (k + 1) * max(children_width, 1)
            if range_start < range_end:
                children_ranges.append((round(range_tuples[range_start], 3), round(range_tuples[range_end], 3)))
    return children_ranges

def cross_product_for_marginals(per_attr_decomp: List[T]) -> List[T]:
    # Outputs cross product in the form of a list of tuples.
    marginals_decomp_list_of_tuples = list(itertools.product(*per_attr_decomp))
    # Convert a list of tuples to a list of list
    marginals_decomp_list_of_lists = [list(e) for e in marginals_decomp_list_of_tuples]
    return marginals_decomp_list_of_lists

