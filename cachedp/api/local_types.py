from typing import List, Tuple
import numpy as np

from cachedp.api.Cache.CacheEntry import CacheEntry

FREE = 0
PAID = 1
EPSILON_INFINITY = 1000
SMALL_VALUE = 1e-10
CacheReturnType = Tuple[np.array, CacheEntry]
RelaxPrivacyReturnType = (float, float, float, np.array,
                          List[CacheEntry], np.array)

RangeTuple = Tuple[int, int]
RangeTupleList = List[RangeTuple]
Marginal = RangeTupleList
NamedMarginalsList = List[Tuple[Marginal, List[str]]]

#For range tuple, arity, min bucket width
AttributeInfoTuple = Tuple[RangeTuple, int, float, str]
AttributeInfoTupleList = List[AttributeInfoTuple]

#TODO: Thomas confirm these are unused and remove them
INCOME_CAPGAIN_BOUND = 100000
NON_EXISTENT_EPSILON = -1000
