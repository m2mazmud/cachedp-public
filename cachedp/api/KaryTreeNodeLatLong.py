from cachedp.api.KaryTreeNode import *
from cachedp.api.RangeTupleHelpers import match_marginals, intersect_areas, cross_product_for_marginals

class KaryTreeNodeLatLong(KaryTreeNode):
    correlated_attributes_ranges: Marginal
    correlated_attribute_names: List[str]

    def __init__(self, correlated_attributes_ranges: Marginal, correlated_attributes_names: List[str]):
        super().__init__(-1, -1, attribute_name='', parent_attribute_names=[], parent_attribute_range=[])
        self.correlated_attributes_ranges = correlated_attributes_ranges
        self.correlated_attributes_names = correlated_attributes_names

    def get_children_ranges(self, arity, min_bucket_width):
        sets_of_children_ranges = []
        for tup in self.correlated_attributes_ranges:
            children_ranges = get_children_ranges_helper(arity, min_bucket_width, tup[0], tup[1])
            if children_ranges == []: children_ranges = [tup]
            sets_of_children_ranges.append(children_ranges)

        own_children_ranges = cross_product_for_marginals(sets_of_children_ranges)
        if len(own_children_ranges) == 1: return []
        return own_children_ranges

    def _generate_full_marginal_attribute_ranges_names(self) \
            -> Tuple[Marginal, List[str]]:
        own_full_marginal = self.correlated_attributes_ranges
        attribute_names = self.correlated_attributes_names
        return own_full_marginal, attribute_names

    def mark_nodes_in_subtree(self, m: Marginal, value: int):
        matching = match_marginals(self.correlated_attributes_ranges, [m])
        if matching:
            self._mark_node(value)

        children = self.get_children()
        for child in children:
            intersection = intersect_areas(workload_lat_long=m,
                                           node_lat_long=child.correlated_attributes_ranges)
            if intersection:
                child.mark_nodes_in_subtree(m, value)

if __name__ == '__main__':
    node = KaryTreeNodeLatLong([(0, 4), (0, 4)], ['Latitude', 'Longitude'])
    children_ranges = node.get_children_ranges(arity=2, min_bucket_width=1)
    print(children_ranges)

    #
    # def __init__(self, lat_start: int, lat_end: int, long_start: int, long_end: int, attribute_name: str,
    #              parent_attribute_names: List[str], parent_attribute_range: Marginal):
    #     KaryTreeNode.__init__(lat_start, lat_end,
    #                           attribute_name, parent_attribute_names, parent_attribute_range)
    #     self.node_area = [(lat_start, lat_end), (long_start, long_end)]
    #
    # def get_children_ranges(self, min_bucket_width):
    #     (cur_lat_min, cur_lat_max), (cur_long_min, cur_long_max) = self.node_area[0], self.node_area[1]
    #     cur_lat_width, cur_long_width = cur_lat_max - cur_lat_min, cur_long_max - cur_long_min
    #     # Note that each node might not be a square
    #     children_ranges: List(RangeTupleList) = []
    #     if cur_lat_width >= 2 and cur_long_width >= 2:
    #         children = [[(cur_lat_min, cur_lat_min + cur_lat_width/2), (cur_long_min, cur_long_min + cur_long_width/2)],
    #                     [(cur_lat_min + cur_lat_width/2, cur_lat_max), (cur_long_min, cur_long_min + cur_long_width/2)],
    #                     [(cur_lat_min, cur_lat_min + cur_lat_width/2), (cur_long_min + cur_long_width/2, cur_long_max)],
    #                     [(cur_lat_min + cur_lat_width/2, cur_lat_max), (cur_long_min + cur_long_width/2, cur_long_max)]]
    #     elif cur_lat_width >= 2:
    #         children = [[(cur_lat_min, cur_lat_min + cur_lat_width/2), (cur_long_min, cur_long_max)],
    #                     [(cur_lat_min + cur_lat_width/2, cur_lat_max), (cur_long_min, cur_long_max)]]
    #     elif cur_long_width >= 2:
    #         children = [[(cur_lat_min, cur_lat_max), (cur_long_min, cur_long_min + cur_long_width/2)],
    #                     [(cur_lat_min, cur_lat_max), (cur_long_min + cur_long_width/2, cur_long_max)]]
    #     else:
    #         children = []
    #     children_ranges += children
    #     return children_ranges
