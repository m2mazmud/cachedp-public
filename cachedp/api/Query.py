from cachedp.api.local_types import *

class QueryInputs():
    alpha: float
    beta: float
    variance: float
    workload: np.array

    def __init__(self, alpha: float, beta: float, workload: np.array, variance: float):
        self.alpha = alpha
        self.beta = beta
        self.workload = workload
        if variance is not None:
            self.variance = variance
        else:
            self.variance = None

from cachedp.api.data_vector import DataVector
from cachedp.api.Cache.cache import Cache
from cachedp.api.Modules.MMM.MMM_SE import NewMMMStrategyAndCacheEntries
from cachedp.api.Modules.RelaxPrivacy.RelaxPrivacy import RPStrategyAndCacheEntries, RPComparer
from cachedp.api.StrategyGenerator import get_strategy_tuples

class Query:
    is_correlated: bool
    data_vector: DataVector
    workload_tuples: RangeTupleList
    strategy_tuples: RangeTupleList
    infoTupleList: AttributeInfoTupleList
    domain_root_forest: RangeTupleList
    query_inputs: QueryInputs
    rng: np.random.Generator
    free_paid_vector_plain: np.array
    cached_entries_plain = None

    strategy_obj: NewMMMStrategyAndCacheEntries = None
    se_strategy_obj: NewMMMStrategyAndCacheEntries = None
    se_parameter: float = 1.0
    proactive_strategy: np.array = []
    proactive_named_marginals: NamedMarginalsList = []

    rp_comparer: RPComparer = None

    # workload1: (1,1) attr1 attr2, workload2: attr1 attr2 (2,2)
    # we parititon x so x is smaller, and A is also smaller -> saving privacy budget

    def __init__(self, workload_tuples: NamedMarginalsList, alpha: float, beta: float, variance: float,
                 infoTupleList: AttributeInfoTupleList, data_vector: DataVector, se_parameter: float,
                 rng, is_correlated):
        self.is_correlated = is_correlated
        self.workload_tuples = workload_tuples
        self.data_vector = data_vector
        self.infoTupleList = infoTupleList
        self.se_parameter = se_parameter
        self.rng = rng
        workload =  self.data_vector.from_tuple_to_matrix(list(list(zip(*workload_tuples))[0]), self.is_correlated)
        query_inputs = QueryInputs(alpha, beta, workload, variance)
        self.query_inputs = query_inputs
        # Get the workload and strategy from the tuples
        self.domain_root_forest, self.strategy_tuples = get_strategy_tuples(self.workload_tuples,
                                                                            infoTupleList, self.is_correlated)
        # strategy_matrix is not guaranteed to be full ranked, it has a workload dependent granularity
        # workload-dependent datavector & strategy + bucketing algorithm
        # full datavector + bucketing algo
        strategy_matrix = self.data_vector.from_tuple_to_matrix(self.strategy_tuples, self.is_correlated)
        strategy_obj = NewMMMStrategyAndCacheEntries(strategy_matrix, self.strategy_tuples, query_inputs)
        strategy_obj.set_loose_target_b_epsilon()
        self.strategy_obj = strategy_obj
        WA_plus = strategy_obj.get_transformed_WA_plus()
        rp_strategy_obj = RPStrategyAndCacheEntries(strategy_matrix, self.strategy_tuples, query_inputs)
        rp_strategy_obj.set_loose_target_b_epsilon(WA_plus)
        self.rp_comparer = RPComparer(rp_strategy_obj)
        self.proactive_strategy = np.array([])

    '''Set which entries are free and which are paid (no noise-parameter thresholding)'''
    def set_free_paid_and_cache_entries(self, cache: Cache):
        # Look up which strategy matrix rows are in the cache.
        self.free_paid_vector_plain, self.cached_entries_plain = cache.get_entries_plain(matrix=self.strategy_obj.get_strategy())

    '''Determines whether relax privacy can be applied or not.'''
    def can_relax_privacy(self):
        # All entries are not in the cache (at least one paid entry)
        if sum(self.free_paid_vector_plain) == 0:
            return True
        else:
            return False

    '''Fetches (thresholded) cached responses for responding to a strategy matrix via MMM (and SE)'''
    def get_cached_responses(self, strategy_expander: bool) -> np.array:
        strategy_obj = self._get_strategy_obj(strategy_expander)
        if strategy_expander:
            return np.array([entry.noisy_value for entry in strategy_obj.get_free_caches()] +
                            [entry.noisy_value for entry in strategy_obj.get_extended_cached_entries()])
        return np.array([entry.noisy_value for entry in strategy_obj.get_free_caches()])

    '''Returns (thresholded) free paid vector for responding to a strategy matrix via MMM (and SE).'''
    def get_free_paid_vector(self, strategy_expander: bool):
        strategy_obj = self._get_strategy_obj(strategy_expander)
        return strategy_obj.get_free_paid_vector()

    '''Internal function to get the strategy object
    used in estimating and executing MMM, SE.'''
    def _get_strategy_obj(self, strategy_expander: bool) -> np.array:
        if strategy_expander:
            strategy_obj = self.se_strategy_obj
        else:
            strategy_obj = self.strategy_obj
        return strategy_obj

    '''Returns the tight b *bound* for the original strategy, assuming cache *is* used.'''
    def get_tight_bound(self, strategy_expander: bool) -> float:
        strategy_obj = self._get_strategy_obj(strategy_expander)
        return strategy_obj.get_tight_b()

    '''Returns the tight epsilon *cost* for the original strategy, assuming cache *is* used.'''
    def get_tight_cost(self, strategy_expander: bool) -> float:
        strategy_obj = self._get_strategy_obj(strategy_expander)
        return strategy_obj.get_tight_cost()

    '''Returns the tight b bound for the original strategy, assuming cache is *not* used.'''
    def get_original_strategy_no_cache_tight_b(self):
        return self.strategy_obj.get_original_tight_b()

    '''Strategy Expander: Returns rows in a that are unique to it (not in b)'''
    def _check_matrix_a_in_b(self, a: np.array, b: np.array) -> np.array:
        indices = []
        for i in range(len(a)):
            is_contain = np.isin(b, a[i]).all(axis=1)
            if True in is_contain:
                indices.append(i)
        return indices

    '''Strategy Expander: Remove rows from entries fetched from the cache that are already in the original strategy.'''
    def deduplicate_and_set_expanded_strategy(self, extra_strategy_rows_cache_entries : Tuple[np.array, List[CacheEntry]]):
        strategy_matrix, strategy_tuples = self.strategy_obj.get_strategy(), self.strategy_obj.get_strategy_tuples()
        (extra_strategy_rows, extra_cache_entries) = extra_strategy_rows_cache_entries

        expanded_strategy = np.vstack((strategy_matrix, extra_strategy_rows))
        expanded_free_paid_vector = np.concatenate(([PAID] * len(strategy_matrix),
                                                    [FREE] * len(extra_strategy_rows)))
        expanded_cache_entries = extra_cache_entries
        self.se_strategy_obj = NewMMMStrategyAndCacheEntries(expanded_strategy, strategy_tuples, self.query_inputs)
        self.se_strategy_obj.set_loose_target_b_epsilon()
        self.se_strategy_obj.set_noise_parameters_cached_entries(self.strategy_obj.get_noise_parameters(),
                                                                 self.strategy_obj.get_noise_parameter_indices(),
                                                                 self.strategy_obj.get_cached_entries())
        self.se_strategy_obj.set_extended_free_paid_cached_entries(expanded_free_paid_vector, expanded_cache_entries)

    '''Sets the proactive strategy using either the original MMM matrix or SE matrix'''
    def set_proactive_strategy(self, cache: Cache, strategy_expander: bool) -> NamedMarginalsList:
        strategy_obj = self._get_strategy_obj(strategy_expander)
        # strategy_matrix = strategy_obj.get_paid_strategy()
        # strategy_tuples = self.data_vector.from_matrix_to_tuple(strategy_matrix, self.is_correlated)
        strategy_tuples = strategy_obj.get_paid_strategy_tups()
        # print('This is the strategy tups: ', strategy_tuples)
        if len(strategy_tuples) == 0:
            return []
        proactive_named_marginals = self.domain_root_forest.get_proactive(strategy_tuples, cache)
        # strategy_tree = BinaryTreeProactive(arity=2, bounds=self.domain_root_forest, query_list=strategy_tuples)
        # proactive_tuples = strategy_tree.get_proactive_list(cache)
        if proactive_named_marginals != []:
            self.proactive_named_marginals = proactive_named_marginals
            proactive_strategy = []
            for tups in proactive_named_marginals:
                row = self.data_vector.transform_query(tups[1], tups[0])
                proactive_strategy.append(row)
            self.proactive_strategy = np.array(proactive_strategy)
        return proactive_named_marginals

    '''Answering a given strategy matrix - Laplace mechanism - ground truth + noise'''
    def _answer_strategy(self, strategy: np.array,
                         data_vector: np.array,
                         b: float) -> List[float]:
        strat_obj = self._get_strategy_obj(False)
        strat_obj._set_transform_buckets(strategy)
        b_matrix = strat_obj._transform_matrix(strategy)
        b_dv = strat_obj._transform_dv(data_vector)

        # ground_truth = np.matmul(strategy, data_vector)
        ground_truth = np.matmul(b_matrix, b_dv)
        noises = self.rng.laplace(scale=b, size=len(ground_truth))
        noisy_responses = np.add(ground_truth, noises)

        return noisy_responses

    def get_paid_sensitivity(self, strategy_expander):
        if strategy_expander:
            return self.se_strategy_obj.get_paid_sensitivity()
        else:
            return self.strategy_obj.get_paid_sensitivity()

    def get_true_response(self, cache):
        return self.query_inputs.workload @ self.data_vector.data_vector

    #TODO: Set data vector initially?
    #TODO: Get best directly by calling b_est = query.get_tight_bound(strategy_expander)
    '''Answers proactive strategy using given data vector'''
    def answer_proactive_strategy(self, strategy_expander: bool, data_vector: np.array) \
            -> Tuple[NamedMarginalsList, List[float]]:
        b_est = self.get_tight_bound(strategy_expander)

        # Handle proactive strategy entries.
        proactive_strategy = self.proactive_strategy
        proactive_responses = np.zeros(proactive_strategy.shape[0])
        if proactive_strategy.shape[0] > 0:
            # Even though proactive strategy entries are 'free' for cost estimation,
            # we get proactive strategy responses from DB and add noise to them.
            proactive_responses = self._answer_strategy(self.proactive_strategy, data_vector, b_est)

        return self.proactive_named_marginals, proactive_responses

    '''Answers MMM or SE strategy using given data vector.'''
    def answer_MMM_or_SE_strategy(self, strategy_expander: bool, data_vector: np.array)\
            -> Tuple[NamedMarginalsList, List[float]]:
        strategy_obj = self._get_strategy_obj(strategy_expander)
        b_est = strategy_obj.get_tight_b()

        paid_matrix = strategy_obj.get_paid_strategy()
        return paid_matrix, self._answer_strategy(paid_matrix, data_vector, b_est)

    #0 - MMM, 1 - SE, 2 - RP
    def answer_workload(self, mmm_or_se_or_rp: int,
                        strategy_responses: np.array) -> np.array:
        if mmm_or_se_or_rp == 0: #MMM
            WA_plus = self._get_strategy_obj(False).get_transformed_WA_plus()
        elif mmm_or_se_or_rp == 1: #SE
            WA_plus = self._get_strategy_obj(True).get_transformed_WA_plus()
        else:
            WA_plus = self.rp_comparer.get_best_old_strategy().get_transformed_WA_plus()
        return np.matmul(WA_plus, strategy_responses)


    '''For debugging display purposes only.'''
    def get_chosen_strategy_info(self, strategy_expander: bool):
        strategy_obj = self._get_strategy_obj(strategy_expander)
        free_paid_vector = strategy_obj.get_free_paid_vector()
        strategy_tuples = self.data_vector.from_matrix_to_tuple(strategy_obj.get_strategy(),
                                                                is_correlated=self.is_correlated)
        return strategy_tuples, free_paid_vector

    '''Gets RP strategy object - used by estimate RP, execute_RP, debugging display in main.py'''
    def get_rp_comparer(self) -> RPComparer:
        return self.rp_comparer
