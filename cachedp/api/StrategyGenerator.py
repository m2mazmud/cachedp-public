from typing import Union
from cachedp.api.KaryTreeLatLong import KaryTreeLatLong
from cachedp.api.KaryTree import KaryTree
from cachedp.api.local_types import *
from more_itertools import unique_everseen


def get_strategy_tuples(workload_marginals_list: NamedMarginalsList,
                        attributeInfoList: AttributeInfoTupleList,
                        is_correlated: bool = False) -> Tuple[Union[KaryTree, KaryTreeLatLong], List[Marginal]]:

    workload_marginals_ranges_only = list(list(zip(*workload_marginals_list))[0])
    attributes_in_workload_marginals = list(list(zip(*workload_marginals_list))[1])
    nested_unique_attribute_pairs_in_workload = list(unique_everseen(attributes_in_workload_marginals))
    #Flattening the nested list
    unique_attribute_pairs_in_workload = [item for sublist in nested_unique_attribute_pairs_in_workload for item in sublist]
    attributes_in_infolist = list(list(zip(*attributeInfoList))[3])
    #We assume each workload consists of marginals over the entire set of attributes used to build the multi-attribute tree.
    #I.e. if tree is to be built over (a1, a2, a3) and marginal is only over (a1, a3),
    # then make a new workload and tree over only (a1, a3) for that marginal.
    if unique_attribute_pairs_in_workload != attributes_in_infolist:
        raise NotImplementedError
    if is_correlated:
        tree = KaryTreeLatLong(attributeInfoList)
    else:
        tree = KaryTree(attributeInfoList)

    # level_order_traversal = self.print_level_order(self.root, [])
    # for node in level_order_traversal:
    #     print(node)
    strategy_marginals_list = []
    for workload_marginal in workload_marginals_ranges_only:
        strategy_marginals = tree.decompose_multi_attribute(workload_marginal)
        strategy_marginals_list.extend(strategy_marginals)

    #Each strategy marginal should be unique.
    unique_strategy_marginals_list = [i for n, i in enumerate(strategy_marginals_list) if i not in strategy_marginals_list[:n]]
    return tree, unique_strategy_marginals_list
