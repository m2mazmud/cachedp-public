from collections import deque
from cachedp.api.KaryTreeNode import KaryTreeNode
from cachedp.api.Cache.cache import Cache
from cachedp.api.RangeTupleHelpers import *
from cachedp.api.local_types import RangeTuple, AttributeInfoTupleList, Marginal, NamedMarginalsList

class KaryTree:
    root: KaryTreeNode

    def __init__(self, _attributeInfoList: AttributeInfoTupleList):
        assert (len(_attributeInfoList) > 0)
        self.root = None

        temp_leaves_at_each_level = []
        for attributeInfo in _attributeInfoList:
            (range_tuple, arity, min_bucket_width, attr_name) = attributeInfo
            if not self.root:
                attribute_tree_root = self._build_tree(range_tuple, arity, min_bucket_width, attr_name, [], [])
                self.root = attribute_tree_root
                temp_leaves_at_each_level.append([attribute_tree_root])
                parent_leaves = attribute_tree_root.get_own_attribute_leaves()
                temp_leaves_at_each_level.append(parent_leaves)
            else:
                parent_leaves = temp_leaves_at_each_level[-1]
                leaves_at_next_level = []
                parent_attribute_names = parent_leaves[0].parent_attribute_names + [parent_leaves[0].attribute_name]
                for parent_leaf in parent_leaves:
                    parent_attribute_ranges = parent_leaves[0].parent_attribute_range + [(parent_leaf.start, parent_leaf.end)]
                    attribute_subtree_root = self._build_tree(range_tuple, arity, min_bucket_width, attr_name,
                                                               parent_attribute_names, parent_attribute_ranges)
                    parent_leaf.add_subtree_for_attribute(attribute_subtree_root)
                    parent_leaf.set_next_attribute_range_name(range_tuple, attr_name)
                    next_leaves_for_subtree = attribute_subtree_root.get_own_attribute_leaves()
                    leaves_at_next_level.extend(next_leaves_for_subtree)
                temp_leaves_at_each_level.append(leaves_at_next_level)

    def _build_tree(self, range_tuple: RangeTuple, arity: int, min_bucket_width: float, attribute_name: str,
                     parent_attribute_names: List[str], parent_attribute_ranges: Marginal) -> KaryTreeNode:
        start = range_tuple[0]
        end = range_tuple[1]

        node = KaryTreeNode(start, end, attribute_name, parent_attribute_names, parent_attribute_ranges)
        children_ranges = node.get_children_ranges(arity, min_bucket_width)

        if children_ranges:
            for j in range(0, len(children_ranges)):
                child_subtree = self._build_tree(children_ranges[j], arity, min_bucket_width, attribute_name,
                                                  parent_attribute_names,
                                                  parent_attribute_ranges)
                node.children.append(child_subtree)
        return node

    def _decompose_per_attribute(self, attribute_tuple_in_marginal: RangeTuple, node: KaryTreeNode,
                                  get_leaves: bool,
                                  encountered_root_once=False) -> List[KaryTreeNode]:
        (workload_start, workload_end) = attribute_tuple_in_marginal
        # If n == l, via recursive calls below, return n or l (both are same)
        # If l contains n (forest case, first call only) return n
        if node.children_are_next_marginal and not encountered_root_once:
            comparison_range = node.next_attribute_range
            #encountered_root_once = True
        else:
            comparison_range = (node.start, node.end)

        if workload_start <= comparison_range[0] and comparison_range[1] <= workload_end:
            if get_leaves:
                leaves = node.get_own_attribute_leaves()
                return leaves
            else:
                return [node]

        decomposition = []
        #Second condition necessary for subtrees for 2nd and 3rd attributes (rooted at a node where children_are_next_marginal is true)
        #encountered_root_once is False (only first time this func is run) and children are next marginal is true
        if not node.children_are_next_marginal or (node.children_are_next_marginal and not encountered_root_once):
            for child in node.get_children():
                child_range = (child.start, child.end)
                intersection = intersect(attribute_tuple_in_marginal, child_range)
                if intersection:
                    decomposition_for_child = self._decompose_per_attribute(intersection, child, get_leaves, True)
                    decomposition.extend(decomposition_for_child)

        return decomposition


    def _get_unique_attr_level_leaves(self, per_level_decomp: List[List[KaryTreeNode]]) -> List[Marginal]:
        per_level_decomp_ranges = []
        for attribute_level_list in per_level_decomp:
            temp_list = self._list_of_node_to_rangetuples(attribute_level_list)
            per_level_decomp_ranges.append(temp_list)

        unique_attr_level_leaves = []
        for attribute_leaves_list in per_level_decomp_ranges:
            unique_leaves_for_attr = []
            for attr_range in attribute_leaves_list:
                if attr_range not in unique_leaves_for_attr:
                    unique_leaves_for_attr.append(attr_range)
            unique_attr_level_leaves.append(unique_leaves_for_attr)

        return unique_attr_level_leaves

    def _list_of_node_to_rangetuples(self, attribute_level_list: List[KaryTreeNode]) -> RangeTupleList:
        temp_list = []
        for node in attribute_level_list:
            temp_list.append((node.start, node.end))
        return temp_list


    '''Input is a single marginal range, i.e. a list of k tuples, one tuple for each attribute in the marginal.
        Output is a list of k decompositions. Each decomposition includes the attribute root and a list of 
        nodes on the tree for that attribute. 
    '''

    def decompose_multi_attribute(self, workload_tuple_with_marginals: Marginal) -> List[Marginal]:
        if len(workload_tuple_with_marginals) == 1: get_leaves = False
        else: get_leaves=True
        #First go through the root, get the next leaves.
        attr1_leaf_level_decomp = self._decompose_per_attribute(workload_tuple_with_marginals[0], self.root, get_leaves)

        per_level_decomp = [attr1_leaf_level_decomp]
        #Then in a for loop across all other marginals
        for attr_ctr in range(1, len(workload_tuple_with_marginals)):
            #For each matching node, run the decompose_per_attribute function for that marginal, with that matching node as the tree root.
            marginal = workload_tuple_with_marginals[attr_ctr]
            next_parents = []
            if attr_ctr == len(workload_tuple_with_marginals) - 1:
                get_leaves = False

            for parent in per_level_decomp[-1]:
                next_leaf_level_decomp = self._decompose_per_attribute(marginal, parent, get_leaves)
                next_parents.extend(next_leaf_level_decomp)
            per_level_decomp.append(next_parents)

        per_level_decomp_ranges = self._get_unique_attr_level_leaves(per_level_decomp)
        #Generate output list through a cross-product across all lists in per_attr_leaves.
        marginals_decomp_range_tuple_list = cross_product_for_marginals(per_level_decomp_ranges)
        return marginals_decomp_range_tuple_list



    # https://stackoverflow.com/questions/1894846/printing-bfs-binary-tree-in-level-order-with-specific-formatting
    def print_level_order(self, head: KaryTreeNode, l, queue=deque(), accum: int = 0):
        if head is None:
            return
        if accum > 550:
            return l
        t = (head.start, head.end, head.attribute_name, head.parent_attribute_names, head.parent_attribute_range,
             head.children_are_next_marginal, head.next_attribute_range)
        l.append(t)
        accum = accum + 1
        [queue.append(node) for node in head.get_children()]
        if queue:
            self.print_level_order(queue.popleft(), l, queue, accum)
        return l

    def _naive_marker(self, marginal: Marginal, value: int = 1, parent_node: KaryTreeNode = None,
                       ctr:int=0):
        if not parent_node:
            parent_node = self.root
        next_marginal_node_range = marginal[ctr]
        if ctr < len(marginal) - 1: #We can still get to next leaves
            all_leaves = parent_node.get_own_attribute_leaves(skip_self=True)
            all_leaves_ranges = [(node.start, node.end) for node in all_leaves]
            bool_match, matching_index = match(next_marginal_node_range, all_leaves_ranges)
            assert(bool_match)
            next_parent = all_leaves[matching_index]
            self._naive_marker(marginal, value, next_parent, ctr + 1)
        else:
            parent_node.mark_nodes_in_subtree(next_marginal_node_range, value)

    def _run_mark_marginal_nodes(self, unique_marginals_list: List[Marginal]):
        for marginal in unique_marginals_list:
            self._naive_marker(marginal)

    def get_proactive(self, paid_strategy_marginals: List[Marginal], cache: Cache) -> NamedMarginalsList:
        # level_order_traversal = self.print_level_order(self.root, [])
        # for node in level_order_traversal:
        #     print(node)
        #print("**before**")
        #self.root.check_marked_nodes()
        self._run_mark_marginal_nodes(paid_strategy_marginals)
        #print("**after**")
        #self.root.check_marked_nodes()
        paid_sensitivity = self.root.update_max_tree_sensitivity()
        proactive_list = self.root.get_proactive_for_subtree_at_node(paid_sensitivity, cache)
        return proactive_list

if __name__ == '__main__':
    print("Run test cases in StrategyGenproactiveTester.py. Don't import that class - circular dependency")