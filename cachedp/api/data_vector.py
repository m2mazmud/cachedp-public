import pandas as pd
import numpy as np
from cachedp.api.helpers_sql_tuple import *

def next_power_of_2(x):
    return 1 if x == 0 else 2**(x - 1).bit_length()

def gen_datavector(dataframe, attribute_names, domains, granularities, is_full=False):
    partitions = []
    attribute_ranges = {}
    df, dv_length = dataframe[attribute_names], 1
    for i in range(len(attribute_names)):
        attr_arr = df[attribute_names[i]].to_numpy()
        (d_min, d_max) = domains[attribute_names[i]][0]
        attribute_range = get_range_tuple(d_max, d_min, granularities[i])
        dv_length *= len(attribute_range) - 1
        attribute_ranges[attribute_names[i]] = attribute_range
        partitions.append(pd.cut(df[attribute_names[i]], attribute_range, right=False))
    if not is_full:
        data_vector = df.groupby(partitions).count()[attribute_names[0]].to_numpy()
    else:
        data_vector = np.zeros(dv_length)
    return attribute_ranges, data_vector

# The attribute_ranges is splitted in the same order as the workload
# connection operators are 'and'
def get_single_dim_query_and(attribute_ranges, row):
    current_total_margin_lengths = len(row)
    single_query_ranges = []
    single_attr_indices = [[] for i in range(len(attribute_ranges))]
    for i in range(len(row)):
        if row[i] > 0:
            remain_margin_size, remain_index = current_total_margin_lengths, i
            for j in range(len(attribute_ranges)):
                cur_attr_dsize = len(attribute_ranges[j]) - 1
                assert current_total_margin_lengths % cur_attr_dsize == 0
                remain_margin_size /= cur_attr_dsize
                attr_ind = remain_index // remain_margin_size
                single_attr_indices[j].append(int(attr_ind))
                remain_index %= remain_margin_size
    return [sorted(list(set(ele))) for ele in single_attr_indices]


class DataVector:
    # domains: {attribute_name: (min, max)}
    def __init__(self, dataframe, attrbuteNames, domains, granularities, is_full=False):
        attribute_ranges, data_vector = gen_datavector(dataframe, attrbuteNames, domains, granularities, is_full=is_full)
        self.is_full = is_full
        self.data_vector = data_vector  # the datavector wrt in granularity
        self.range_tuples = attribute_ranges  # dictionary: (attribute name, attribute partitions)

        self.step_size = {attrbuteNames[i]: granularities[i] for i in range(len(attrbuteNames))}
        self.attrbuteName = attrbuteNames

    # takes in the attribute name (subset of total attributes) and queries, transform into datavector
    def transform_query(self, attribute_names: list(), attribute_queries: RangeTupleList, to_cache=False):
        # to cache flag is called with True specifically in cache.set_entries.
        # In that case, some margins might be over a subset of self.attributeName
        assert(len(attribute_queries) == len(attribute_names))
        current_total_margin_lengths = 1
        if to_cache:
            for name in attribute_names: current_total_margin_lengths *= len(self.range_tuples[name]) - 1
        else: current_total_margin_lengths = len(self.data_vector)
        q = np.zeros((current_total_margin_lengths), dtype='uint8')
        q_inds = []
        for name in self.attrbuteName:
            if name in attribute_names:
                i = attribute_names.index(name)
                (start_val, end_val) = attribute_queries[i]
            elif to_cache: continue
            else: (start_val, end_val) = (self.range_tuples[name][0], self.range_tuples[name][-1])
            query_ind = np.where(np.logical_and(self.range_tuples[name] >= start_val,
                                                self.range_tuples[name] < end_val))
            current_total_margin_lengths /= len(self.range_tuples[name]) - 1
            if len(q_inds) == 0:
                q_inds = np.array(query_ind[0]) * current_total_margin_lengths
            else:
                new_q_inds = []
                for ele in q_inds:
                    new_q_inds += list(np.array(query_ind[0]) * current_total_margin_lengths + ele)
                q_inds = new_q_inds

        if not to_cache: assert (len(self.data_vector) == len(q))
        q[np.array(q_inds).astype(int)] = 1
        return q

    def from_tuple_to_matrix(self, tuples: List[Marginal], iscorrelated: bool=False):
        matrix = []
        if iscorrelated:
            range_tuples_lat, range_tuples_long = self.range_tuples['latitude'], self.range_tuples['longitude']
            ll_size = len(range_tuples_lat) - 1
            for query in tuples:
                # print(query)
                lat_range, long_range = query[0], query[1]
                workload_matrix = np.zeros((ll_size, ll_size))
                for i in range(len(range_tuples_lat)):
                    for j in range(len(range_tuples_long)):
                        if range_tuples_long[j] >= long_range[0] and range_tuples_long[j] < long_range[1] and \
                                range_tuples_lat[i] >= lat_range[0] and range_tuples_lat[i] < lat_range[1]:
                            workload_matrix[i][j] = 1
                row = workload_matrix.flatten()
                matrix.append(row)
        else:
            for query in tuples:
                hd_query = self.transform_query(self.attrbuteName, query)
                matrix.append(hd_query)
        return np.array(matrix)

    def from_matrix_to_tuple(self, workload, is_correlated=False, attribute_names=None):
        tups = []
        for row in workload:
            if is_correlated:
                ll_length = len(self.range_tuples['latitude']) - 1
                lat_indices, long_indices = [], []
                for i in range(len(row)):
                    if row[i] > 0:
                        lat_indices.append(i // ll_length)
                        long_indices.append(i % ll_length)
                lat_indices, long_indices = sorted(list(set(lat_indices))), sorted(list(set(long_indices)))
                assert len(lat_indices) == lat_indices[-1] - lat_indices[0] + 1
                assert len(long_indices) == long_indices[-1] - long_indices[0] + 1
                tups.append([(self.range_tuples['latitude'][lat_indices[0]],
                              self.range_tuples['latitude'][lat_indices[-1]+1]),
                             (self.range_tuples['longitude'][long_indices[0]],
                              self.range_tuples['longitude'][long_indices[-1]+1])])
            else:
                cur_tup = []
                if attribute_names is not None: attr_ranges = [self.range_tuples[a] for a in attribute_names]
                else: attr_ranges = [self.range_tuples[a] for a in self.attrbuteName]
                single_dim_indices = get_single_dim_query_and(attr_ranges, row)
                for i in range(len(single_dim_indices)):
                    cur_range_tup, s_indices = self.range_tuples[self.attrbuteName[i]], single_dim_indices[i]
                    assert len(single_dim_indices[i]) == single_dim_indices[i][-1] - single_dim_indices[i][0] + 1
                    cur_tup.append((cur_range_tup[s_indices[0]], cur_range_tup[s_indices[-1]+1]))

                tups.append(cur_tup)
                # print(tups)
        return tups

    # given a list of sql queries, returns the workload
    def sql_to_workload(self, sql_conditions, attribute_names):
        range_tuples = []
        for condition in sql_conditions:
            start_value = float(condition.partition('<=')[0].strip(' '))
            end_value = float(condition.rpartition('<')[-1].strip(' '))
            range_tuples.append([(start_value, end_value)])

        # input for below: [[tuples]...], list of queries (list of list of tuples)
        workload = []
        for query in range_tuples:
            cur_workload = self.transform_query(attribute_names, query)
            workload.append(cur_workload)
        return workload

    def tuple_to_sql(self, workload_tups):
        sql_queries = []
        for tups in workload_tups:
            margins, attr_names = tups[0], tups[1]
            cur_sql_query = ''
            for i in range(len(margins)):
                if i == 0:
                    cur_sql_query = attr_names[i] + ' >= ' + str(round(margins[i][0], 3)) + ' and ' \
                                    + attr_names[i] + ' < ' + str(round(margins[i][1], 3))
                else:
                    cur_sql_query += ' and ' + attr_names[i] + ' >= ' + str(round(margins[i][0], 3)) + ' and ' \
                                    + attr_names[i] + ' < ' + str(round(margins[i][1], 3))
            sql_queries.append(cur_sql_query)
        return sql_queries



    # Pass in the wordload and the attributeNames involved in the workload
    def workload_to_sql(self, workload):
        attribute_ranges = [self.range_tuples[name] for name in self.attrbuteName]
        sql_queries = []
        for row in workload:
            single_dim_margins = get_single_dim_query_and(attribute_ranges, row)
            cur_sql_query = ''
            for i in range(len(single_dim_margins)):
                if i == 0:
                    cur_sql_query += self.workload_to_sql_single(single_dim_margins[i],
                                                                 attribute_ranges[i], self.attrbuteName[i])
                else:
                    cur_sql_query += ' and ' + \
                                     self.workload_to_sql_single(single_dim_margins[i],
                                                                 attribute_ranges[i], self.attrbuteName[i])
            sql_queries.append(cur_sql_query)
        return sql_queries


    def workload_to_sql_single(self, index_list, attribute_range, name):
        # attribute_range = np.r_[self.range_tuple, self.range_tuple[-1] + self.step_size]
        # print(workload)
        assert len(index_list) - 1 == index_list[-1] - index_list[0]
        starting_ind, ending_ind = index_list[0], index_list[-1] + 1
        if '-' not in name:
            query = f"{attribute_range[starting_ind]} <= {name} and {name} < {attribute_range[ending_ind]}"
        else:
            query = f"{attribute_range[starting_ind]} <= '{name}' and '{name}' < {attribute_range[ending_ind]}"

        return query



if __name__ == "__main__":
    d = {'longitude': [0, 2, 3, 7, 6, 5], 'latitude': [1, 2, 3, 3, 7, 2]}
    attribute_lists = ['longitude', 'latitude']
    attribute_ranges = [(1, 4), (1, 4)]
    # we will assume the data vector of higher dimension is like
    # (attr1.val1, attr2.val1) (attr1.val1, attr2.val2) ... (attr2.val1, attr2.val1) ...
    domains = {'latitude': (0, 8), 'longitude': (0, 8)}
    df = pd.DataFrame(data=d)
    long_lat_data = DataVector(df, attribute_lists, domains, [2, 2])
    row = long_lat_data.lat_long_tuple_to_matrix(attribute_ranges)
    # build_datavector(pd.read_csv('dataset/adult.csv'), 'sex', 4)
    tuples = [(0, 1), (1, 8), (2, 3), (3, 4)]
    # print(age_data.range_vector[0])
    sql_conditions = ['17 <= age and age < 21', '21 <= age and age < 49', '25 <= age and age < 29', '29 <= age and age < 33']
    matrix = age_data.sql_to_workload(sql_conditions, ['age'])
    print(matrix)
    sql_2 = age_data.workload_to_sql(matrix, ['age'])
    print(sql_2)
