from cachedp.api.KaryTree import KaryTree
from cachedp.api.local_types import Marginal, List
from cachedp.api.KaryTreeNodeLatLong import KaryTreeNodeLatLong
from cachedp.api.RangeTupleHelpers import *

class KaryTreeLatLong(KaryTree):
    root: KaryTreeNodeLatLong

    def __init__(self, infotuplelist):
        attribute_domains = list(list(zip(*infotuplelist))[0])
        attribute_names = list(list(zip(*infotuplelist))[3])

        #Check that the arities and min_bucket_widths are identical (might need tiny tweeks to calls to build_latlong_tree below to make it work otherwise)
        attribute_arities = list(list(zip(*infotuplelist))[1])
        attribute_min_bucket_widths = list(list(zip(*infotuplelist))[2])
        assert(min(attribute_arities) == max(attribute_arities))
        assert(min(attribute_min_bucket_widths) == max(attribute_min_bucket_widths))

        self.root = self._build_latlong_tree(range_tuple_list=attribute_domains,
                                              arity=attribute_arities[0],
                                              min_bucket_width=attribute_min_bucket_widths[1],
                                              attribute_names=attribute_names)

    def _build_latlong_tree(self, range_tuple_list: Marginal, arity: int, min_bucket_width: float,
                             attribute_names: List[str]) -> KaryTreeNodeLatLong:
        node = KaryTreeNodeLatLong(range_tuple_list, attribute_names)

        children_ranges = node.get_children_ranges(arity, min_bucket_width)

        if children_ranges:
            for j in range(0, len(children_ranges)):
                child_subtree = self._build_latlong_tree(children_ranges[j], arity, min_bucket_width, attribute_names)
                node.children.append(child_subtree)
        return node

    def _run_mark_marginal_nodes(self, unique_marginals_list: List[Marginal]):
        for marginal in unique_marginals_list:
            self.root.mark_nodes_in_subtree(marginal, value=1)

    #This function could have returned the node.correlated_attribute_ranges field directly,
    #but am returning the entire node as the 1 attribute decomposition function had to be modified
    # for the multi-attribute case to return the node instead of the tuple.
    def _decompose_correlated_attributes(self, workload_marginal: Marginal, node: KaryTreeNodeLatLong) -> List[KaryTreeNodeLatLong]:
        matching = match_marginals(workload_marginal, [node.correlated_attributes_ranges])
        if matching:
            return [node]

        decomposition = []
        children = node.get_children()
        for child in children:
            intersection = intersect_areas(workload_lat_long=workload_marginal,
                                           node_lat_long=child.correlated_attributes_ranges)
            if intersection:
                decomposition_for_child = self._decompose_correlated_attributes(intersection, child)
                decomposition.extend(decomposition_for_child)

        return decomposition

    def _list_of_node_to_correlated_marginals(self, node_list: List[KaryTreeNodeLatLong]) -> List[Marginal]:
        temp_list = []
        for node in node_list:
            temp_list.append(node.correlated_attributes_ranges)
        return temp_list

    def decompose_multi_attribute(self, workload_tuple_with_marginals: Marginal) -> List[Marginal]:
        list_of_nodes = self._decompose_correlated_attributes(workload_tuple_with_marginals, self.root)
        return self._list_of_node_to_correlated_marginals(list_of_nodes)


if __name__ == '__main__':
    #We take in a normal infotupleList, i.e. [((0, 4), 2, 1, 'latitude'), ((0, 4), 2, 1, 'longitude')]
    info_tuple_list = [((0, 4), 2, 1, 'Latitude'), ((0, 4), 2, 1, 'Longitude')]
    tree = KaryTreeLatLong(info_tuple_list)
    first_level = tree.root.get_children()
    for node in first_level:
        print(node.correlated_tuple_ranges)
        for child in node.get_children():
            print(child.correlated_tuple_ranges)
