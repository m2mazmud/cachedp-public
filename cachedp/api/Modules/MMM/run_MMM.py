from cachedp.api.Cache.cache import Cache
from cachedp.api.Query import Query
from cachedp.api.local_types import *

def run_mmm(query: Query, cache: Cache,
            strategy_expander: bool) -> np.array:
    cached_responses = query.get_cached_responses(strategy_expander)
    epsilon_est = query.get_tight_cost(strategy_expander)

    free_paid_vector = query.get_free_paid_vector(strategy_expander)

    if epsilon_est == 0.0:
        return cached_responses

    b_est = query.get_tight_bound(strategy_expander)
    strategy_to_cache, paid_responses = \
        query.answer_MMM_or_SE_strategy(strategy_expander, query.data_vector.data_vector)
    responses_to_cache = paid_responses

    # Handle proactive strategy entries.
    # Even though proactive strategy entries are 'free' for cost estimation,
    # we get proactive strategy responses from DB and add noise to them.
    proactive_strategy_tups, proactive_responses = \
            query.answer_proactive_strategy(strategy_expander, query.data_vector.data_vector)

    epsilon_est = query.get_paid_sensitivity(strategy_expander) / b_est
    # print('This is the results we put in cache: ', strategy_to_cache, responses_to_cache)
    cache.set_entries(matrix=strategy_to_cache,
                      noisy_values=responses_to_cache,
                      epsilon=epsilon_est,
                      noise_parameter=b_est,
                      proactive=proactive_strategy_tups,
                      proactive_noisy_values=proactive_responses,
                      is_correlated=query.is_correlated)

    if len(cached_responses) > 0:
        free_responses = cached_responses
    else:
        free_responses = []

    # Generate the final response to the strategy using the free and paid responses.
    # Proactive query responses are not included.
    # Expanded strategy responses are included (in paid indices)
    strategy_responses = np.ones(free_paid_vector.size)
    paid_ind = np.where(free_paid_vector == PAID)[0]
    free_ind = np.where(free_paid_vector == FREE)[0]
    np.put(strategy_responses, paid_ind, paid_responses)
    np.put(strategy_responses, free_ind, free_responses)

    return strategy_responses
