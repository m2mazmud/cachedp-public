import numpy as np
from typing import Union

from cachedp.api.local_types import EPSILON_INFINITY
from cachedp.api.Cache.cache import Cache
from cachedp.api.Query import Query

'''Modified matrix mechanism --- estimate cost for the MMM module'''
def estimate_MMM_cost(query: Query, cache: Cache, args) -> tuple[float, float, float]:
    (noise_parameters, noise_parameter_indices, cache_entries_of_accurate_strategy_rows) = \
            cache.check_noise_param_cached_entries(query.free_paid_vector_plain,
                                                   query.cached_entries_plain)

    query.strategy_obj.set_noise_parameters_cached_entries(noise_parameters,
                                                           noise_parameter_indices,
                                                           cache_entries_of_accurate_strategy_rows)
    query.strategy_obj.set_tight_bound_new(strategy_expander=False, use_variance=args.use_variance, rng=query.rng)
    # query.strategy_obj.set_free_paid_cached_entries(query.strategy_obj.free_paid_vector,
    #                                                 cache_entries_of_accurate_strategy_rows)
    if noise_parameters:
        b_min = np.min(noise_parameters)
    else:
        b_min = -1
    tight_cost = query.strategy_obj.get_tight_cost()
    if tight_cost > 0:
        b_p = query.strategy_obj.get_original_tight_b()
    else:
        b_p = 0    
    return query.strategy_obj.get_tight_cost(), b_min, b_p

'''The caller should only call this if use_cache is true'''
def estimate_SE_cost(query: Query, cache: Cache, args)-> Union[str, float]:
    b_mmm_est_worst_case = query.strategy_obj.get_original_tight_b()

    original_strategy_matrix = query.strategy_obj.get_strategy()
    extra_strategy_rows_cache_entries = cache.get_expanded_strategy(b_mmm_est_worst_case * args.se_parameter,
                                                                    args.max_strategy,
                                                                    original_strategy_matrix)
    if extra_strategy_rows_cache_entries[0].size > 0:
        query.deduplicate_and_set_expanded_strategy(extra_strategy_rows_cache_entries)
        query.se_strategy_obj.set_tight_bound_new(strategy_expander=True, use_variance=args.use_variance, rng=query.rng)
        return query.se_strategy_obj.get_tight_cost()
    else:
        return EPSILON_INFINITY
    # return '''Please run this function after calling estimate_MMM_cost,
    # so that the original strategy is set and the tight bound for the no-cache case can be computed.'''
