from typing import List

import numpy as np

from cachedp.api.Cache.CacheEntry import CacheEntry
from cachedp.api.Modules.basic_estimation import estimate_tight_b, estimate_tight_b_new
from cachedp.api.Modules.matrix_ops import compute_sensitivity
from cachedp.api.local_types import PAID, FREE, EPSILON_INFINITY, Marginal, List
from cachedp.api.Modules.BasicStrategyClass import BasicStrategyClass
from cachedp.api.Query import QueryInputs

class NewMMMStrategyAndCacheEntries(BasicStrategyClass):
    free_paid_vector: np.array
    cached_responses: List[CacheEntry]
    extended_cached_responses: List[CacheEntry]
    paid_strategy_sensitivity: int
    paid_strategy: np.array
    paid_strategy_tuples: List[Marginal]

    noise_parameters: List[float] = []
    sorted_cache_indices: List[int] = []
    tight_b_bounds: List[float] = []
    tight_cost: float

    def __init__(self, strategy_matrix: np.array, strategy_tuples: List[Marginal], query_inputs: QueryInputs):
        BasicStrategyClass.__init__(self, strategy=strategy_matrix, strategy_tuples=strategy_tuples, query_inputs=query_inputs)
        # Not necessary per se as set_free_paid_cached_entries is always called before get_free_paid_vector
        # Added only as a precaution.
        self.free_paid_vector = np.ones(len(strategy_matrix))
        self.paid_strategy = self.strategy[np.where(self.free_paid_vector == PAID)[0]]
        self.paid_strategy_sensitivity = compute_sensitivity(self.paid_strategy)
        self.noise_parameters = []
        self.tight_b_bounds = []
        self.cached_responses = []
        self.tight_cost = EPSILON_INFINITY

    def set_noise_parameters_cached_entries(self, noise_parameters: List[float], noise_parameter_indices: List[int],
                                            cached_responses: List[CacheEntry]):
        self.noise_parameters = noise_parameters
        self.sorted_cache_indices = noise_parameter_indices
        self.cached_responses = cached_responses

    '''The caller will set the free paid vector after the Monte Carlo Process'''
    def set_free_paid_vector(self, free_paid_vector: np.array):
        self.free_paid_vector = free_paid_vector
        paid_ind = np.where(self.free_paid_vector == PAID)[0]
        self.paid_strategy = self.strategy[paid_ind]
        self.paid_strategy_tuples = [self.strategy_tuples[i] for i in paid_ind]
        # Add paid strategy in tuples here
        self.paid_strategy_sensitivity = compute_sensitivity(self.paid_strategy)

    '''The caller is supposed to ensure that the cached entries in cached_responses
    are in the order in free_paid_vector'''
    def set_extended_free_paid_cached_entries(self, free_paid_vector: np.array, extended_cached_responses: List[CacheEntry]):
        self.free_paid_vector = free_paid_vector
        self.extended_cached_responses = extended_cached_responses
        paid_ind = np.where(self.free_paid_vector == PAID)[0]
        self.paid_strategy = self.strategy[paid_ind]
        self.paid_strategy_tuples = [self.strategy_tuples[i] for i in paid_ind]
        self.paid_strategy_sensitivity = compute_sensitivity(self.paid_strategy)

    '''Returns noise parameters for the free entries in F/P vector.
    Used for (second) MC tight bound.'''
    def get_extended_cached_noise_params(self):
        return [cache_entry.noise_parameter for cache_entry in self.extended_cached_responses]

    '''Returns free paid vector - used in MC.'''
    def get_free_paid_vector(self):
        return self.free_paid_vector

    def get_free_caches(self):
        for i in range(len(self.free_paid_vector)):
            if self.free_paid_vector[i] == PAID:
                return self.cached_responses[:i]
        return self.cached_responses

    '''Returns all cache entries.
    Used for initializing the cached entries for SE object.'''
    def get_cached_entries(self) -> List[CacheEntry]:
        return self.cached_responses

    def get_extended_cached_entries(self) -> List[CacheEntry]:
        return self.extended_cached_responses

    def get_noise_parameters(self) -> List[float]:
        return self.noise_parameters

    def get_noise_parameter_indices(self) -> List[int]:
        return self.sorted_cache_indices

    def get_paid_strategy_tups(self) -> List[Marginal]:
        return self.paid_strategy_tuples

    def get_paid_strategy(self) -> np.array:
        return self.paid_strategy

    def get_free_strategy(self) -> np.array:
        return self.strategy[np.where(self.free_paid_vector == FREE)[0]]

    '''Returns the sensitivity of the paid part of the matrix.'''
    def get_paid_sensitivity(self) -> int:
        return self.paid_strategy_sensitivity

    def set_tight_b(self, b: float):
        self.tight_b_bounds.append(b)

    def get_original_tight_b(self) -> float:
        return self.tight_b_bounds[0]

    def get_tight_b(self):
        return self.tight_b_bounds[-1]

    def set_tight_cost(self, cost: float):
        self.tight_cost = cost

    def get_tight_cost(self):
        return self.tight_cost

    def set_tight_bound_new(self, strategy_expander: bool, use_variance: bool, rng):
        loose_b = self.get_loose_target_b()
        free_paid_vector = self.get_free_paid_vector()
        noise_b = np.full((len(free_paid_vector), 1), loose_b)
        if strategy_expander:
            free_indices = np.where(free_paid_vector == FREE)[0]
            noise_params = self.get_extended_cached_noise_params()
            assert(len(free_indices) == len(noise_params))
            np.put(noise_b, free_indices, noise_params)
        tight_b = estimate_tight_b_new(self, noise_b, loose_b, strategy_expander, use_variance, rng)
        if tight_b == 0.0:
            self.set_tight_cost(0.0)
        elif tight_b == -1.0:
            self.set_tight_cost(EPSILON_INFINITY)
        else:
            self.set_tight_b(tight_b)
            paid_sensitivity = self.get_paid_sensitivity()
            tight_cost = paid_sensitivity / tight_b
            self.set_tight_cost(tight_cost)

        return tight_b

    '''Sets the tight bound for the original MMM or SE matrix, 
    by running MC using the loose bound.'''
    def set_tight_bound(self, strategy_expander: bool, rng):
        loose_b = self.get_loose_target_b()
        free_paid_vector = self.get_free_paid_vector()
        are_any_free = np.any((free_paid_vector == FREE))
        noise_b = np.full((len(free_paid_vector), 1), loose_b)

        if are_any_free:
            free_indices = np.where(free_paid_vector == FREE)[0]
            noise_params = self.get_cached_noise_params()
            assert(len(free_indices) == len(noise_params))
            np.put(noise_b, free_indices, noise_params)

        tight_b = estimate_tight_b(self, noise_b, free_paid_vector,
                                   loose_b, strategy_expander, rng)
        if tight_b == 0.0:
            self.set_tight_cost(0.0)
        elif tight_b == -1.0:
            self.set_tight_cost(EPSILON_INFINITY)
        else:
            self.set_tight_b(tight_b)
            paid_sensitivity = self.get_paid_sensitivity()
            tight_cost = paid_sensitivity / tight_b
            self.set_tight_cost(tight_cost)

        return tight_b