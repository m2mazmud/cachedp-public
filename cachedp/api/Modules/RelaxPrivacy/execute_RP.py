import numpy as np

from cachedp.api.Cache.cache import Cache
from cachedp.api.Modules.RelaxPrivacy.RelaxPrivacy import RPComparer
from cachedp.api.Query import Query

'''This function maps the strategy rows from the chosen strategy to the original strategy'''
def _get_mapping_old_to_new_strategy_order(cache: Cache,
                                           small_strategy: np.array,
                                           superset_strategy: np.array):
    mapping_of_cached_responses = []
    superset_strategy_row_cache_ids = [cache.indexer(row) for row in superset_strategy]
    for row in small_strategy:
        row_id = cache.indexer(row)
        idx = superset_strategy_row_cache_ids.index(row_id)
        mapping_of_cached_responses.append(idx)
    return mapping_of_cached_responses

def run_relax_privacy(query: Query, cache: Cache) -> np.array:
    return run_relax_privacy_internal(query.get_rp_comparer(), cache, query)


def run_relax_privacy_internal(rp_comparer: RPComparer, cache: Cache, query) -> np.array:
    old_responses = rp_comparer.get_best_old_strategy().get_cached_noisy_responses()
    best_old_strategy_matrix = rp_comparer.get_best_old_strategy().get_strategy()
    original_strategy = rp_comparer.get_original_strategy().get_strategy()

    mapping_of_cached_responses = \
        _get_mapping_old_to_new_strategy_order(cache, original_strategy,
                                               best_old_strategy_matrix)

    if rp_comparer.get_best_old_strategy().get_tight_cost() == 0.0:
        return old_responses[mapping_of_cached_responses]

    best_old_strategy = rp_comparer.get_best_old_strategy().get_strategy()

    ground_truth = np.matmul(best_old_strategy, query.data_vector.data_vector)
    # Get old laplacian noise values
    old_lap_noises = np.array(old_responses) - ground_truth

    # Get the old inverted noise parameter from the cache.
    old_b_inv = 1 / rp_comparer.get_best_old_strategy().get_cached_noise_parameter()
    # Get the new inverted noise parameter and the tight final MC epsilon.
    new_b, tight_target_epsilon = \
        rp_comparer.get_best_old_strategy().get_tight_target_b_epsilon()
    new_b_inv = 1 / new_b
    # Get the new Laplacian noise values using the noise down operator.
    new_noises = noise_down_operator(
        old_lap_noises, old_b_inv, new_b_inv, query.rng)
    # print('This is the ground truth and noises: ', ground_truth, new_noises)
    relaxed_cached_responses = ground_truth + new_noises

    # Update Cache
    cache.set_entries(matrix=best_old_strategy,
                      noisy_values=relaxed_cached_responses,
                      epsilon=tight_target_epsilon,
                      noise_parameter=new_b)

    strategy_responses = relaxed_cached_responses[mapping_of_cached_responses]
    return strategy_responses


''' Modified from APEx's implementation of Algorithm 1 of Koufogiannis et al.
Modifications: 
- added a for loop over noises
- uses rng
- instead of eps we use 1/b (to account for sensitivty)
Note old_b_inv, new_b_inv correspond to 1/b '''

def noise_down_operator(old_noises, old_b_inv, new_b_inv, rng):
    assert new_b_inv > old_b_inv
    new_noises = [] * len(old_noises)
    for old_lap_noise in old_noises:
        sign = np.sign(old_lap_noise)
        pdf = [old_b_inv / new_b_inv * np.exp((old_b_inv - new_b_inv) * abs(old_lap_noise)),
               (new_b_inv - old_b_inv) / (2.0 * new_b_inv),
               (old_b_inv + new_b_inv) / (2.0 * new_b_inv) * (
                           1.0 - np.exp((old_b_inv - new_b_inv) * abs(old_lap_noise)))]

        p = rng.random()
        z = rng.random()
        if p <= pdf[0]:
            new_noise = old_lap_noise

        elif p <= pdf[0] + pdf[1]:
            new_noise = np.log(z) / (old_b_inv + new_b_inv)
            new_noise = new_noise * sign

        elif p <= pdf[0] + pdf[1] + pdf[2]:
            new_noise = np.log(
                z * (np.exp(abs(old_lap_noise) * (old_b_inv - new_b_inv)) - 1.0) + 1.0) / (old_b_inv - new_b_inv)
            new_noise = new_noise * sign

        else:
            new_noise = abs(old_lap_noise) - np.log(1.0 - z) / \
                        (new_b_inv + old_b_inv)
            new_noise = new_noise * sign

        new_noises.append(new_noise)
    return new_noises
