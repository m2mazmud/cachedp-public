from typing import List, Union, Set
from cachedp.api.Modules.RelaxPrivacy.RelaxPrivacy import RPComparer, RPStrategyAndCacheEntries
from cachedp.api.Cache.CacheEntry import CacheEntry
from cachedp.api.Cache.cache import Cache

EPSILON_INFINITY = 1000.0


'''This function goes through the cached strategy rows. 
It generates a list of timestamps that are common to all rows.'''
def get_common_timestamps(cached_entries_of_strategy_rows) \
        -> Set[int]:
    number_of_strategy_rows = len(cached_entries_of_strategy_rows)
    common_ts_so_far = set

    for row_ctr in range(number_of_strategy_rows):
        timestamps_for_current_row = [
            entry.logical_timestamp for entry in cached_entries_of_strategy_rows[row_ctr]]
        timestamps_for_current_row = set(timestamps_for_current_row)
        if not common_ts_so_far.intersection(timestamps_for_current_row):
            return set()
        elif common_ts_so_far:
            common_ts_so_far = common_ts_so_far.intersection(timestamps_for_current_row)
        else:
            common_ts_so_far = timestamps_for_current_row
    return common_ts_so_far

def get_relax_privacy_strategy(comparer: RPComparer, cache: Cache,
                               cached_entries_of_strategy_rows: List[List[CacheEntry]]) -> Union[
    str, RPStrategyAndCacheEntries]:
    common_timestamps = get_common_timestamps(cached_entries_of_strategy_rows)
    if len(common_timestamps) == 0:
        return "All strategy rows were not found at a single common timestamp."

    # Fetch *all* strategies at this particular timestamp
    # Type of output: List[(np.array, List[CacheEntry])]
    old_strategies_cache_entries = [cache.fetch_all_at_timestamp(ts) for ts in common_timestamps]
    best_old_strategy = comparer.set_best_old_strategy(old_strategies_cache_entries, cache)

    return best_old_strategy
