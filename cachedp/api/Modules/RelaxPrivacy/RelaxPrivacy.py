import numpy as np #For typing
from cachedp.api.Cache.cache import Cache
from cachedp.api.Cache.CacheEntry import CacheEntry
from typing import List, Tuple

from cachedp.api.Modules.BasicStrategyClass import BasicStrategyClass
from cachedp.api.Modules.basic_estimation import estimate_tight_b
from cachedp.api.Query import QueryInputs
from cachedp.api.local_types import PAID, EPSILON_INFINITY, Marginal, List


class RPStrategyAndCacheEntries(BasicStrategyClass):
    cached_epsilon: float
    cached_responses: List[CacheEntry]
    '''Additional *loose* cost to relax strategy'''
    loose_cost: float = EPSILON_INFINITY
    '''Additional *tight* cost to relax strategy'''
    tight_cost: float = EPSILON_INFINITY
    '''Tight target b from MC'''
    tight_target_b: float
    '''Target epsilon to relax strategy to - uses MC'''
    tight_target_epsilon: float
    strategy_cache_index: np.array=[]

    def __init__(self, strategy: np.array, strategy_tuples: List[Marginal], query_inputs: QueryInputs):
        BasicStrategyClass.__init__(self, strategy=strategy, strategy_tuples=strategy_tuples, query_inputs=query_inputs)

    def set_cached_responses_loose_cost(self, cached_responses: List[CacheEntry]):
        self.cached_responses = cached_responses
        self.cached_epsilon = cached_responses[0].epsilon
        loose_cost = self.get_loose_target_epsilon() - self.cached_epsilon
        self.loose_cost = max(loose_cost, 0.0)
        self.tight_cost = self.loose_cost

    '''Returns loose additional cost, which was calculated during the strategy comparison process.
    If loose cost is 0.0 then MC does not need to be done'''
    def get_loose_cost(self):
        return self.loose_cost

    '''Returns number of rows in the strategy matrix. 
    Used for making F/P vector for MC.'''
    def get_strategy_num_rows(self) -> int:
        return self.get_strategy().shape[0]

    def set_cache_index(self, best_old_strategy) -> None:
        self.strategy_cache_index = best_old_strategy

    def get_cache_index(self) -> np.array:
        return self.strategy_cache_index

    '''Takes as input tight target b output from MC.
    Sets tight target epsilon (using sensitivity) and 
    additional tight cost (using cached epsilon).'''
    def set_tight_target_b_epsilon_final_cost(self, tight_target_b: float):
        #estimate_tight_b will return positive values only for RP.
        #The <= is just sanity checking
        if tight_target_b <= 0.0:
            self.tight_target_b = 0.0
            self.tight_cost = 0.0
        else:
            self.tight_target_b = tight_target_b
            self.tight_target_epsilon = self.get_strategy_sensitivity() / tight_target_b
            self.tight_cost = max(self.tight_target_epsilon - self.cached_epsilon, 0)

    def set_tight_cost(self, rng, use_variance: bool):
        # # Estimating a tight worst-case b if all queries were answered without cache
        # Results in a tight bound of the target b that we should relax to.
        len_original_strategy = self.get_transformed_WA_plus().shape[1]
        best_rp_target_b = self.get_loose_target_b()
        noise_b = np.full((len_original_strategy, 1), best_rp_target_b)
        paid_indices = [PAID] * len_original_strategy

        tight_target_b = estimate_tight_b(
            self, noise_b, paid_indices, best_rp_target_b, False, rng, use_variance)
        self.set_tight_target_b_epsilon_final_cost(tight_target_b)

        # tight additional cost = tight target epsilon - cached epsilon
        self.tight_cost = self.get_tight_cost() #TODO: Should not need this
        return self.tight_cost

    '''Returns the tight target b inverse, tight target epsilon'''
    def get_tight_target_b_epsilon(self) -> Tuple[float, float]:
        return self.tight_target_b, self.tight_target_epsilon

    '''Returns tight additional cost (= tight target epsilon - cached epsilon)'''
    def get_tight_cost(self) -> float:
        return self.tight_cost

    '''Returns cached noisy responses.
     Used to respond to the strategy'''
    def get_cached_noisy_responses(self) -> np.array:
        return np.array([cache_entry.noisy_value for cache_entry in self.cached_responses])

    def get_cached_noise_parameter(self) -> float:
        return self.cached_responses[0].noise_parameter


# Take new_strategy, cached_responses, WAB, compute costs for each.
class RPComparer():
    query_inputs: QueryInputs
    original_strategy: RPStrategyAndCacheEntries
    temp_old_strategy: RPStrategyAndCacheEntries
    lowest_loose_cost_strategy: RPStrategyAndCacheEntries = None
    best_old_strategy: RPStrategyAndCacheEntries

    def __init__(self, strategy: RPStrategyAndCacheEntries):
        self.original_strategy = strategy
        self.query_inputs = strategy.get_query_inputs()

    def set_temp_strategy_cache_entries(self, strategy, cached_responses):
        #Set cached epsilon in original_strategy obj to calculate new loose delta cost.
        self.original_strategy.set_cached_responses_loose_cost(cached_responses)
        #Use the same workload, alpha, beta as for the old strategy
        #for computing the loose bounds
        self.temp_old_strategy = RPStrategyAndCacheEntries(strategy, self.query_inputs)
        # print(self.temp_old_strategy.get_strategy_sensitivity())
        # print(self.original_strategy.get_strategy_sensitivity())
        self.temp_old_strategy.set_loose_target_b_epsilon()
        self.temp_old_strategy.set_cached_responses_loose_cost(cached_responses)

    def compare_temp_old_strategy_costs_to_min(self):
        cost_to_relax_original_strategy = self.original_strategy.get_loose_cost()
        cost_to_relax_old_strategy = self.temp_old_strategy.get_loose_cost()

        lower_cost = min(cost_to_relax_original_strategy, cost_to_relax_old_strategy)
        if self.lowest_loose_cost_strategy is None or lower_cost <= self.lowest_loose_cost_strategy.get_loose_cost():
            if cost_to_relax_original_strategy <= cost_to_relax_old_strategy:
                self.lowest_loose_cost_strategy = self.original_strategy
            else:
                self.lowest_loose_cost_strategy = self.temp_old_strategy
            self.best_old_strategy = self.temp_old_strategy

    def get_lowest_loose_cost_strategy(self) -> RPStrategyAndCacheEntries:
        return self.lowest_loose_cost_strategy

    def get_best_old_strategy(self) -> RPStrategyAndCacheEntries:
        return self.best_old_strategy

    def get_original_strategy(self) -> RPStrategyAndCacheEntries:
        return self.original_strategy

    def set_best_old_strategy(self, arr_of_old_strategies_cache_entries:  List[tuple], cache: Cache):
        # Out of all past strategies that contain the current strategy, pick the old strategy whose cached responses to use,
        # by picking the most accurate one with the smallest noise parameter.
        epsilons : List[float] = [cached_entries[0].epsilon for (old_strategy, cached_entries)
                            in arr_of_old_strategies_cache_entries]
        index_of_best_old_strategy = epsilons.index(min(epsilons))
        best_old_strategy = arr_of_old_strategies_cache_entries[index_of_best_old_strategy][0]
        cached_responses = arr_of_old_strategies_cache_entries[index_of_best_old_strategy][1]

        best_old_strategy_tups = cache.get_cur_full_dv().from_matrix_to_tuple(best_old_strategy)
        best_old_strategy_cur_dv = cache.cur_data_vector.from_tuple_to_matrix(best_old_strategy_tups)
        # Set the best_old_strategy_obj by reusing some fields from the original strategy
        # We use the same W, alpha, beta as the original strategy
        original_query_inputs = self.original_strategy.get_query_inputs()
        best_old_strategy_obj = RPStrategyAndCacheEntries(best_old_strategy_cur_dv, best_old_strategy_tups, original_query_inputs)

        # We use the same WA plus as the original strategy
        original_WA_plus = self.original_strategy.get_transformed_WA_plus()
        best_old_strategy_obj.set_loose_target_b_epsilon(original_WA_plus)
        best_old_strategy_obj.set_cache_index(best_old_strategy)
        # We use the cached responses of the past strategy.
        best_old_strategy_obj.set_cached_responses_loose_cost(cached_responses)
        self.best_old_strategy = best_old_strategy_obj
        return self.best_old_strategy