from typing import Union, List

from cachedp.api.Modules.RelaxPrivacy.RelaxPrivacy import RPComparer
from cachedp.api.Modules.RelaxPrivacy.strategy_RP import get_relax_privacy_strategy
from cachedp.api.Cache.cache import Cache
from cachedp.api.Query import Query


def estimate_relax_privacy_cost(query: Query, cache: Cache, args) -> Union[str, float]:
    return estimate_RP_cost_internal(query.get_rp_comparer(), query.cached_entries_plain, cache, query.rng, args)

def estimate_RP_cost_internal(rp_comparer: RPComparer,
                              cached_entries_of_strategy_rows,
                              cache: Cache, rng, args) -> Union[str, float]:
    rp_strategy_cache_entries = get_relax_privacy_strategy(rp_comparer, cache, cached_entries_of_strategy_rows)
    # We ran into an error (e.g. no common timestamps).
    if isinstance(rp_strategy_cache_entries, str):
        return rp_strategy_cache_entries

    # No extra privacy budget needs to be spent as the queries are in the cache at the right accuracy.
    if rp_strategy_cache_entries.get_loose_cost() == 0.0:
        return 0.0

    tight_additional_cost = rp_strategy_cache_entries.set_tight_cost(rng, args.use_variance)

    return tight_additional_cost
