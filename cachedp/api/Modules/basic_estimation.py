from typing import Union

import numpy as np
import scipy.stats as st
from numpy import linalg as LA

# from api.Modules.MMM.MMM_SE import NewMMMStrategyAndCacheEntries
# from api.Modules.RelaxPrivacy.RelaxPrivacy import RPStrategyAndCacheEntries

b_diff = 0.1
sample_size = 10000

"""New MC to search for the noise parameter of strategy matrix A"""
def estimate_tight_b_new(strategy_obj, noise_b: np.array, b_loose: float, strat_expander: bool, use_variance: bool, rng):
    noise_b = noise_b.flatten()
    noise_parameters = strategy_obj.get_noise_parameters()
    noise_parameter_indices = strategy_obj.get_noise_parameter_indices()
    min_ind, max_ind = 0, len(noise_parameters)
    if use_variance: target_error = strategy_obj.query_inputs.variance
    else: target_error = strategy_obj.query_inputs.beta
    while min_ind < max_ind:
        mid_ind = int((min_ind + max_ind) / 2)
        # set the first several indices of noise b to its own noise parameters
        np.put(noise_b, noise_parameter_indices[:mid_ind], noise_parameters[:mid_ind])
        np.put(noise_b, noise_parameter_indices[mid_ind:], [noise_parameters[mid_ind]] * (len(noise_b) - mid_ind))

        if use_variance: current_error = compute_variance(strategy_obj, noise_b)
        else: current_error = estimate_beta_mc(strategy_obj, noise_b, rng)

        if current_error > target_error:
            max_ind = mid_ind
        else:
            min_ind = mid_ind + 1

    paid_indices = strategy_obj.get_free_paid_vector()
    paid_indices[noise_parameter_indices[:max_ind]] = 0
    b_min = noise_parameters[min_ind - 1] if min_ind > 0 else b_loose
    b_max = noise_parameters[max_ind] if max_ind <= len(noise_parameters) - 1 else 50000
    if np.sum(paid_indices) == 0: b_worst = 0
    else:
        b_worst = estimate_tight_b(strategy_obj, noise_b, paid_indices, b_min, strat_expander, rng, use_variance, b_max)
    strategy_obj.set_free_paid_vector(paid_indices)
    return b_worst


"""Estimate the cost of the strategy matrix A, using MC."""
def estimate_tight_b(strategy_obj, noise_b: np.array, paid_indices: np.array,
                     b_min: float, strat_expander: bool, rng, use_variance: bool, b_max=1000000):
    noise_b = noise_b.flatten()
    if use_variance:
        target_error = strategy_obj.query_inputs.variance
    else:
        target_error = strategy_obj.query_inputs.beta
    while (b_max - b_min) > b_diff:
        b = (b_max + b_min) / 2.0
        noise_b = np.where(paid_indices, b, noise_b)

        if use_variance: current_error = compute_variance(strategy_obj, noise_b)
        else: current_error = estimate_beta_mc(strategy_obj, noise_b, rng)

        if current_error > target_error: 
            b_max = b
            if sum(paid_indices) == 0:  # Nothing is paid = Everything is free'''
                if strat_expander: # SE has added rows that are not accurate enough (and everything in original strategy is also free)
                    # No paid elements to offset inaccurate free elements.
                    return -1.0  # We want Epsilon to be infinity.
                else: # MMM case - due to randomness, we can't find a tighter b & we trust the first tight bound (no cache case).
                    return 0.0 # We want epsilon to be zero as everything is free based on the first tight bound.
        else:
            #Everything is free and accurate enough (MMM or RP).
            if sum(paid_indices) == 0:
                return 0.0 # We want epsilon to be zero.
            b_min = b

    return b_max


def estimate_beta_mc(strategy_obj, noise_b: np.array, rng):
    counter_fail = 0
    # Either original A's WAplus, or expanded strategy WAplus or RP old full strategy WAplus.
    WA_plus = strategy_obj.get_transformed_WA_plus()

    for i in range(sample_size):
        eta_i = rng.laplace(scale=noise_b)
        error_vec = np.matmul(WA_plus, eta_i)
        L_inf_norm = LA.norm(error_vec, np.inf)
        if L_inf_norm > strategy_obj.query_inputs.alpha:
            counter_fail += 1

    # From APEx
    beta_e = 1.0 * counter_fail / sample_size
    conf_p = strategy_obj.query_inputs.beta / 100.0
    zscore = st.norm.ppf(1.0 - conf_p / 2.0)
    delta_beta = zscore * np.sqrt(beta_e * (1.0 - beta_e) / sample_size)
    beta_e_adjust = beta_e + delta_beta + conf_p

    return beta_e_adjust

def compute_variance(strategy_obj, noise_b: np.array):
    WA_plus = strategy_obj.get_transformed_WA_plus()
    _L, _ell = np.shape(WA_plus)
    workload_variances = [2 * sum([(WA_plus[i,j]**2) * (noise_b[j]**2) for j in range(_ell)]) for i in range(_L)]
    return max(workload_variances)
