import numpy as np
from numpy import linalg as LA
from cachedp.api.helpers_sql_tuple import *
from cachedp.api.StrategyGenerator import get_strategy_tuples

'''compute the var_min for the variance mode'''
def variance_b_min(strategy_obj, variance_requirment):
    WA_plus = strategy_obj.get_transformed_WA_plus()
    _L, _ell = np.shape(WA_plus)
    workload_variances = [sum([(WA_plus[i, j] ** 2) for j in range(_ell)]) for i in range(_L)]
    b = np.sqrt(variance_requirment / (2 * max(workload_variances)))
    return b

'''Compute W * pseudoinverse of A:'''
def compute_WAplus(workload: np.array, strategy: np.array) -> np.array:
    # try:
    strategy_pseudo_inverse = LA.pinv(strategy)
    # except:
    #     strategy_pseudo_inverse = linalg.pinv(strategy)
    WA_plus = np.matmul(workload, strategy_pseudo_inverse)
    return WA_plus

'''get WA^+ without the strategy'''
def approx_WA_plus(workload_tuples, domain_size):
    strategy_arity = 2
    root_forest, strategy_tuples = get_strategy_tuples(workload_tuples, (0, domain_size), strategy_arity)
    workload = from_tuple_to_matrix(domain_size, workload_tuples) 
    strategy = from_tuple_to_matrix(domain_size, strategy_tuples)
    return compute_WAplus(workload, strategy)

'''Estimate b through Chernoff bound that uses accuracy params and W * pseudoinverse of A'''
def compute_chernoff_bound_ugly_b(WA_plus: np.array,
                                  alpha: float, beta: float) -> float:
    return (alpha * np.sqrt(beta / 2.0)) / LA.norm(WA_plus) 

'''Estimate the alpha needed for a given workload whilst maintaining the same b'''
def compute_alpha(wa_plus, b, beta):
    return (b * LA.norm(wa_plus)) / np.sqrt(beta / 2.0)

def compute_sensitivity(matrix: np.array):
    return int(LA.norm(matrix, 1))
