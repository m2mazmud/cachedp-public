import numpy as np

from cachedp.api.local_types import *
from cachedp.api.Modules.matrix_ops import variance_b_min, compute_sensitivity, compute_WAplus, compute_chernoff_bound_ugly_b
from cachedp.api.Query import QueryInputs
from cachedp.api.helpers_sql_tuple import matrix_to_index_set


class BasicStrategyClass:
    strategy: np.array
    strategy_tuples: NamedMarginalsList
    strategy_sensitivity: int

    query_inputs: QueryInputs

    transformed_strategy: np.array
    transformed_strategy_sensitivity: int
    transformed_workload: np.array
    transformed_WA_plus: np.array
    '''Target b from loose bound'''
    loose_target_b: float
    '''Target epsilon to relax strategy to - uses loose bound'''
    loose_target_epsilon: float
    '''buckets to transform strategy'''
    strat_buckets: list

    def __init__(self, strategy: np.array, strategy_tuples: List[Marginal], query_inputs: QueryInputs):
        self.strategy_tuples = strategy_tuples
        self.strategy = strategy
        self.strategy_sensitivity = compute_sensitivity(strategy)
        self.query_inputs = query_inputs
        self.strat_buckets = []

    def get_strategy(self) -> np.array:
        return self.strategy

    def get_strategy_tuples(self) -> List[Marginal]:
        return self.strategy_tuples

    def set_strategy_sensitivity(self, target_sensitivity):
        self.strategy_sensitivity = target_sensitivity

    def get_strategy_sensitivity(self) -> int:
        return self.strategy_sensitivity

    def get_query_inputs(self) -> QueryInputs:
        return self.query_inputs

    def set_loose_target_b_epsilon(self, WA_plus: np.array = None):
        if WA_plus is not None:
            self.strategy_WA_plus = WA_plus
        else:
            self._set_transformed_fields()
            self.strategy_WA_plus = compute_WAplus(self.transformed_workload, self.transformed_strategy)
        if self.query_inputs.variance is not None:
            self.loose_target_b = variance_b_min(self, self.query_inputs.variance)
        else:
            self.loose_target_b = compute_chernoff_bound_ugly_b(self.strategy_WA_plus,
                                                                self.query_inputs.alpha, self.query_inputs.beta)
        self.loose_target_epsilon = self.strategy_sensitivity / self.loose_target_b

    def get_loose_target_b(self) -> float:
        return self.loose_target_b

    def get_loose_target_epsilon(self) -> float:
        return self.loose_target_epsilon

    def get_transformed_WA_plus(self) -> np.array:
        return self.strategy_WA_plus

    def _set_transformed_fields(self):
        self._set_transform_buckets(self.strategy)
        self.transformed_workload = self._transform_matrix(self.query_inputs.workload)
        self.transformed_strategy = self._transform_matrix(self.strategy)
        self.transformed_strategy_sensitivity = compute_sensitivity(self.transformed_strategy)

    # helper for bucketing algo
    def _set_transform_buckets(self, cur_strategy: np.array):
        buckets = []
        # First convert strategy to index sets
        strategy_ind_set = matrix_to_index_set(cur_strategy)
        for cur_set in strategy_ind_set:
            if not set().union(*buckets).intersection(cur_set):
                # case 1: intersection is none
                buckets.append(cur_set)
            else:
                new_buckets = []
                # first append everything introduced by the split of q_range
                for bucket_set in buckets:
                    cur_intersect = bucket_set.intersection(cur_set)
                    if cur_intersect:
                        new_buckets.append(cur_intersect)
                        new_buckets.append(bucket_set.difference(cur_set))
                    else:
                        new_buckets.append(bucket_set)
                remaining_set = cur_set.difference(set().union(*new_buckets))
                new_buckets.append(remaining_set)
                buckets = new_buckets
        self.strat_buckets = [x for x in buckets if x]


    def _transform_matrix(self, matrix: np.array) -> np.array:
        strategy_ind_set = matrix_to_index_set(matrix)
        new_matrix = []
        for cur_set in strategy_ind_set:
            row = np.zeros(len(self.strat_buckets))
            for i in range(len(self.strat_buckets)):
                if self.strat_buckets[i].intersection(cur_set):
                    row[i] = 1
            new_matrix.append(row)
        return np.array(new_matrix)

    def _transform_dv(self, dv: np.array) -> np.array:
        new_dv = []
        for b_set in self.strat_buckets:
            cur_sum = 0
            for i in list(b_set):
                cur_sum += dv[i]
            new_dv.append(cur_sum)
        return np.array(new_dv)
