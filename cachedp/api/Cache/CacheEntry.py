class CacheEntry:
    def __init__(self, noisy_value: float, logical_timestamp: int, epsilon: float, noise_parameter: float):
        self.noise_parameter = noise_parameter
        self.noisy_value = noisy_value
        self.logical_timestamp = logical_timestamp
        self.epsilon = epsilon

    def __lt__(self, other):
        if isinstance(other, CacheEntry):
            return self.noise_parameter < other.noise_parameter
        else:
            return NotImplemented

    def __eq__(self, other):
        if isinstance(other, CacheEntry):
            return self.noise_parameter == other.noise_parameter
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, CacheEntry):
            return self.noise_parameter > other.noise_parameter
        else:
            return NotImplemented
