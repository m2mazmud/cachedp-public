from cachedp.api.local_types import *
from bisect import insort
import time
import sys


class Cache(object):
    def __init__(self, is_correlated):
        self.is_correlated = is_correlated
        self.full_data_vectors = dict()
        self.cur_data_vector = None
        self.cur_attr_names = None
        self.cur_dv_key = None
        self.top_level_cache = dict()

    # TODO: May be we can move the data vector up (data vector --> cache mapping) rather than have it as a field here, within the cache.
    # TODO: Probably make this global (for all datavectors - Cache pairs)
    global_ts = 0

    """For each row that is present in the cache, returns an array of CacheEntry items, sorted by the noise_parameter."""
    """free_or_paid_indices: 0 if free and 1 if paid."""
    def get_cur_full_dv(self):
        return self.full_data_vectors[self.cur_dv_key][0]

    def get_cache_size(self):
        size = 0
        for ele in self.top_level_cache.keys():
            size += len(self.top_level_cache[ele])
        return size

    """get the current top level cache"""
    def get_cur_top_level_cache(self, dv_key=None):
        if dv_key is not None and dv_key in self.top_level_cache:
            return self.top_level_cache[dv_key]
        elif self.cur_dv_key in self.top_level_cache:
            return self.top_level_cache[self.cur_dv_key]
        return {}

    """Set this data vector whenever we get a new query"""
    def set_cur_data_vector(self, cur_data_vector):
        self.cur_data_vector = cur_data_vector

    def add_full_data_vector(self, full_data_vector, attribute_names):
        self.cur_attr_names = attribute_names
        self.cur_dv_key = ''.join(attribute_names)
        if self.cur_dv_key not in self.full_data_vectors:
            self.full_data_vectors[self.cur_dv_key] = (full_data_vector, attribute_names)

    """transform small datavector into a bigger one"""
    def to_full_datavector(self, matrix, full_dv):
        workload_tups = self.cur_data_vector.from_matrix_to_tuple(matrix, self.is_correlated)
        full_matrix = []
        for margin in workload_tups:
            row = full_dv.transform_query(self.cur_data_vector.attrbuteName, margin)
            full_matrix.append(row)
        return full_matrix

    def fetch_all_at_timestamp(self, timestamp: int):
        list_strategy_row_cached_entry: List[Tuple[np.array, CacheEntry]] = []

        for strategy_str, all_cached_entries in self.get_cur_top_level_cache().items():
            for cache_entry in all_cached_entries:
                if cache_entry.logical_timestamp == timestamp:
                    strategy_row = self.unindexer(strategy_str)
                    strategy_tuple = (strategy_row, cache_entry)
                    list_strategy_row_cached_entry.append(strategy_tuple)
                    break
        strategy = [strategy_row for (
            strategy_row, _) in list_strategy_row_cached_entry]
        cached_entries = [cached_entry for (
            _, cached_entry) in list_strategy_row_cached_entry]

        return (np.array(strategy), cached_entries)

    def check_noise_param_cached_entries(self, free_paid_vector: List[int],
                                         cache_entries_of_strategy_rows: List[CacheEntry]):
        free_indices = [i for i, x in enumerate(free_paid_vector) if x == FREE]
        assert len(free_indices) == len(cache_entries_of_strategy_rows)
        cache_entries_of_accurate_strategy_rows = []
        noise_parameters_indices = []
        for row_index in free_indices:
            top_entry = cache_entries_of_strategy_rows[row_index][0]
            cache_entries_of_accurate_strategy_rows.append(top_entry)
            insort(noise_parameters_indices, (top_entry.noise_parameter, row_index))
        return ([x[0] for x in noise_parameters_indices],
                [x[1] for x in noise_parameters_indices], cache_entries_of_accurate_strategy_rows)


    def get_expanded_strategy(self, threshold_noise_parameter: float,
                              max_strategy_rows: float,
                              original_strategy: np.array) -> Tuple[np.array, List[CacheEntry]]:
        # Retrieve all cache rows that are at least as accurate as the threshold noise parameter
        # and are not already included in the original free strategy.
        extra_cache_entries_sorted = []
        extra_cache_entries_parent_child = []
        for cache_dv in self.top_level_cache.keys():
            cur_top_level_cache, (cur_full_data_vector, attr_names) = self.top_level_cache[cache_dv], \
                                                                      self.full_data_vectors[cache_dv]
            if attr_names == self.cur_data_vector.attrbuteName[:len(attr_names)]:
                original_strategy_indexed = [self.indexer(row) for row in original_strategy]
                for cache_index in cur_top_level_cache.keys():
                    if cache_index not in original_strategy_indexed:
                        best_cur_cache_entry = cur_top_level_cache[cache_index][0]
                        strategy_row = self.unindexer(cache_index)
                        if attr_names != self.cur_data_vector.attrbuteName:
                            temp_tup = cur_full_data_vector.from_matrix_to_tuple([strategy_row],
                                                                                 is_correlated=self.is_correlated,
                                                                                 attribute_names=attr_names)
                            strategy_row = self.cur_data_vector.transform_query(attr_names, temp_tup[0])
                        if best_cur_cache_entry.noise_parameter <= threshold_noise_parameter:
                            insort(extra_cache_entries_sorted, (best_cur_cache_entry, self.indexer(strategy_row)))
                            if np.sum(original_strategy @ strategy_row.T) > 0:
                                insort(extra_cache_entries_parent_child, (best_cur_cache_entry, self.indexer(strategy_row)))
        max_strategy_rows = int(max(max_strategy_rows, len(original_strategy) * 2))
        extra_strategy_rows_sorted, extra_cache_entries_sorted = \
            [self.unindexer(x[1]) for x in extra_cache_entries_sorted], [x[0] for x in extra_cache_entries_sorted]
        extra_strategy_rows_sorted, extra_cache_entries_sorted = \
            np.array(extra_strategy_rows_sorted[:max_strategy_rows], dtype=object), \
            extra_cache_entries_sorted[:max_strategy_rows]

        extra_strategy_rows_parent_child, extra_cache_entries_parent_child = \
            [self.unindexer(x[1]) for x in extra_cache_entries_parent_child], [x[0] for x in extra_cache_entries_parent_child]
        extra_strategy_rows_parent_child, extra_cache_entries_parent_child = \
            np.array(extra_strategy_rows_parent_child[:max_strategy_rows], dtype=object), \
            extra_cache_entries_parent_child[:max_strategy_rows]
        return extra_strategy_rows_parent_child, extra_cache_entries_parent_child


    def get_entries_plain(self, matrix: np.ndarray, attr_names=None, proactive_margins=None):
        indices_of_cached_strategy_rows = []
        cached_entries_of_strategy_rows = dict()  # TODO: Fix types - may be typeddict?
        row_ctr = 0
        if proactive_margins:
            dv_key = ''.join(attr_names)
            if dv_key not in self.top_level_cache: return ([1], cached_entries_of_strategy_rows)
            full_matrix = [self.full_data_vectors[self.cur_dv_key][0].transform_query(attr_names, proactive_margins)]
        else:
            # if self.cur_dv_key not in self.top_level_cache: return (np.ones(len(matrix)), cached_entries_of_strategy_rows)
            # assert self.cur_dv_key in self.full_data_vectors
            full_matrix = matrix

        for row in full_matrix:
            # Index the row for looking up in the cache
            index, cur_top_level_cache = self.indexer(row), self.get_cur_top_level_cache()
            if index in cur_top_level_cache.keys():
                indices_of_cached_strategy_rows.append(row_ctr)
                cached_row_entries = cur_top_level_cache[index]

                cached_entries_of_strategy_rows[row_ctr] = cached_row_entries

            row_ctr = row_ctr + 1
        # Free paid indices vector: 0 if *free* 1 if *paid*
        free_paid_vector = np.array(
            [FREE if index in indices_of_cached_strategy_rows else PAID for index in range(len(full_matrix))])

        return (free_paid_vector, cached_entries_of_strategy_rows)

    """Inserts each row of the matrix into the cache; computing the sensitivity."""

    def set_entries(self, matrix: np.array, noisy_values: np.array, epsilon: float, noise_parameter: float,
                    proactive: NamedMarginalsList=None, proactive_noisy_values: np.array=None,
                    RP_to_cache: np.array=None, is_correlated=False) -> None:
        # noisy_values has as many rows as cols in matrix.
        # full_matrix = self.to_full_datavector(matrix, self.full_data_vectors[self.cur_dv_key][0])
        cur_full_dv = self.full_data_vectors[self.cur_dv_key][0]
        if RP_to_cache is not None: full_matrix  = RP_to_cache
        else:
            # full_matrix = matrix
            full_matrix = matrix
        self.global_ts = self.global_ts + 1
        logical_timestamp = self.global_ts
        cur_top_level_cache = self.get_cur_top_level_cache()
        for row, noisy_value in zip(full_matrix, noisy_values):
            cache_entry = CacheEntry(
                noisy_value, logical_timestamp, epsilon, noise_parameter)
            index = self.indexer(row)
            cached_entries = cur_top_level_cache.get(index, [])
            insort(cached_entries, cache_entry)  # sorts by noise parameter
            cur_top_level_cache[index] = cached_entries
        self.top_level_cache[self.cur_dv_key] = cur_top_level_cache
        # proactive to cache
        if proactive:
            for row, noisy_value in zip(proactive, proactive_noisy_values):
                dv_key = ''.join(row[1])
                cur_top_level_cache = self.get_cur_top_level_cache(dv_key)
                if is_correlated: row = cur_full_dv.from_tuple_to_matrix([row[0]], is_correlated)[0]
                else: row = cur_full_dv.transform_query(row[1], row[0], to_cache=True)
                cache_entry = CacheEntry(
                    noisy_value, logical_timestamp, epsilon, noise_parameter)
                index = self.indexer(row)
                cached_entries = cur_top_level_cache.get(index, [])
                insort(cached_entries, cache_entry)  # sorts by noise parameter
                cur_top_level_cache[index] = cached_entries
                self.top_level_cache[dv_key] = cur_top_level_cache

    def indexer(self, row: np.array) -> str:
        row = [int(x) for x in row]
        index = ''.join(map(str, row))
        return index

    def unindexer(self, strategy_str) -> np.array:
        list_str = list(strategy_str)
        row = np.zeros(len(strategy_str))
        for index in range(len(strategy_str)):
            row[index] = int(list_str[index])
        return row
