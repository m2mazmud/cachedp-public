import time
import copy
import math
import hashlib
import numpy as np
import pickle
from cachedp.algorithm.main import *
from pioneer.Cache import PioneerCache


def hash(obj):
    p = pickle.dumps(obj, -1)
    return hashlib.md5(p).hexdigest()

def sort_ranges(l):
    end_points_descending = sorted(l, key=lambda tuple: tuple[1], reverse=True)
    starting_points_ascending = sorted(
        end_points_descending, key=lambda tuple: tuple[0])
    return starting_points_ascending

class Logger():
    # past queries is a sorted (workload, alpha) pair with type (str, float)
    past_queries : dict()
    union_queries : list()
    best_alpha : float
    apex_naive_cache_responses : list()

    def __init__(self):
        self.past_queries = {}
        self.union_queries = []
        self.apex_naive_cache_responses = []
        self.best_alpha = math.inf

    def set_best_alpha(self, new_alpha):
        self.best_alpha = new_alpha

    '''Update the logger, True if more accuracte queries have been run'''
    def update_queries(self, queries, alpha):
        if alpha <= self.best_alpha: self.set_best_alpha(alpha)
        query_key = hash(sort_ranges(queries))
        for query in queries:
            if query not in self.union_queries:
                self.union_queries.append(query)
        if query_key in self.past_queries.keys():
            if self.past_queries[query_key] >= alpha:
                self.past_queries[query_key] = alpha
            else:
                # print('Found in APEx cache')
                return True
        else:
            self.past_queries[query_key] = alpha
        return False

    '''Log apex response for naive cache'''
    def log_apex_query(self, true_answer, best_noisy_ans, eps_cost, time_elapse, in_cache, alpha):
        if in_cache:
            best_cost = 0
        else:
            best_cost = eps_cost
        self.apex_naive_cache_responses.append((true_answer, best_noisy_ans, best_cost, '', 0, 0,
                                                time_elapse, len(self.union_queries), 0, 0, alpha))

    def get_apex_query_responses(self):
        return self.apex_naive_cache_responses

class Client():
    '''represents a client with an accuracy requirement and info needed to continue running'''
    unmultiplied_alpha : float
    base_alpha : float
    current_alpha : float
    alpha_multiplier : float
    is_increase_accuracy : float

    '''the past results needed to generate the next set of queries'''
    HD_workloads: list()
    HD_query_granus: list()
    prior_workloads : list() # in the order of systems
    prior_responses : list() # in the order of systems
    complete_statuses : list() # if all three systems have completed
    dfs_branching_workloads : list()
    dfs_levels : list()

    def __init__(self, unmultiplied_alpha, alpha, is_increase_accuracy,
                 system_selections, prior_workloads, HD_queries=None, HD_query_granus=None):
        self.unmultiplied_alpha = unmultiplied_alpha
        self.base_alpha = alpha
        self.current_alpha = alpha
        self.is_increase_accuracy = is_increase_accuracy
        self.prior_workloads = prior_workloads
        num_systems = len(system_selections)
        self.dfs_branching_workloads = [{}] * num_systems
        self.dfs_levels = [1] * num_systems
        self.prior_responses = [np.array([])] * num_systems
        self.complete_statuses = []
        self.alpha_multiplier = 1.0
        self.HD_workloads = HD_queries
        self.HD_query_granus = HD_query_granus
        for ele in system_selections:
            if ele is not None: self.complete_statuses.append(False)
            else: self.complete_statuses.append(True)

    def get_unmultiplied_alpha(self):
        return self.unmultiplied_alpha

    def get_current_alpha(self):
        alpha = self.current_alpha
        self.current_alpha *= self.alpha_multiplier
        return alpha

    def get_cur_hd_workload(self):
        assert len(self.HD_workloads) == len(self.HD_query_granus)
        cur_hd_workload, cur_attr_granus = self.HD_workloads.pop(0), self.HD_query_granus.pop(0)
        if len(self.HD_workloads) == 0:
            for ind in range(len(self.complete_statuses)):
                self.set_system_complete(ind)
        return cur_hd_workload, cur_attr_granus

    def get_prior_workloads(self, system_index):
        return self.prior_workloads[system_index]

    def set_prior_workloads(self, prior_workloads):
        self.prior_workloads = prior_workloads

    def get_prior_responses(self, system_index):
        return self.prior_responses[system_index]

    def set_prior_responses(self, prior_responses):
        self.prior_responses = prior_responses

    def get_dfs_level(self, system_index):
        return self.dfs_levels[system_index]

    def set_dfs_levels(self, levels):
        self.dfs_levels = levels

    def get_dfs_branching_workload(self, system_index):
        return self.dfs_branching_workloads[system_index]

    def set_dfs_branching_workload(self, system_index, branching_workload):
        self.dfs_branching_workloads[system_index] = branching_workload

    def get_system_complete(self, system_index):
        return self.complete_statuses[system_index]

    def set_system_complete(self, system_index):
        self.complete_statuses[system_index] = True

    def get_complete_status(self):
        if False in self.complete_statuses: return False
        return True


class MultipleClients:
    '''Class to include all clients of the system'''
    is_hd: bool
    client_list : list() = []
    threshold_list : list()
    dfs_interval_list : list()
    system_caches: list()  # in the order of systems
    exploration_responses_dpcache : list()
    exploration_responses_apex : list()
    exploration_responses_pioneer : list()
    exploration_responses_apex_naive_cache : list()

    def __init__(self, args, system_selections, prior_workload, database_size, attr_names,
                 data_vector, is_correlated=False, HD_queries=None, HD_query_granus=None):
        self.exploration_responses_dpcache = []
        self.exploration_responses_apex = []
        self.exploration_responses_pioneer = []
        self.exploration_responses_apex_naive_cache = []
        self.client_list = []
        self.threshold_list = []
        self.dfs_interval_list = []
        rng = np.random.default_rng(seed=0)
        alpha_ranges = np.arange(args.alpha_min, args.alpha_max, args.alpha_step_size)
        alphas = rng.choice(alpha_ranges, args.num_users, replace=True)
        is_increase_accuracies = [False] * args.num_users
        prior_workloads = [prior_workload] * len(system_selections)
        for i in range(args.num_users):
            client = Client(alphas[i], alphas[i] * database_size, is_increase_accuracies[i], system_selections,
                            prior_workloads, HD_queries=copy.copy(HD_queries), HD_query_granus=copy.copy(HD_query_granus))
            self.client_list.append(client)
        if HD_queries is not None:
            cache = init(is_correlated)
            self.system_caches = [cache, None, None]
            self.is_hd = True
        else:
            cache = init(is_correlated)
            cache.set_cur_data_vector(data_vector)
            cache.add_full_data_vector(data_vector, attr_names)
            if is_correlated:
                self.system_caches = [cache, None, None]
                self.is_hd = True
            else:
                self.system_caches = [cache, None, PioneerCache(data_vector.data_vector)]
                self.is_hd = False
        thrsholds = np.arange(args.threshold,
                              args.threshold + args.num_thresholds * args.threshold_step,
                              args.threshold_step)
        self.threshold_list = rng.choice(thrsholds, args.num_users, replace=True)
        lengths = np.arange(args.dfs_interval_min, args.dfs_interval_max + args.threshold_step, args.threshold_step)
        self.dfs_interval_list = rng.choice(lengths, args.num_users, replace=True)

    def get_system_caches(self):
        return self.system_caches

    def set_system_caches(self, caches):
        self.system_caches = caches

    '''get the index of the next client with more queries to run, -1 if all queries complete'''
    def get_incomplete_client(self, rng):
        incomplete_list = []
        for i in range(len(self.client_list)):
            if not self.client_list[i].get_complete_status():
                incomplete_list.append(i)
        if len(incomplete_list) > 0:
            client_index = rng.choice(incomplete_list)
            return client_index, self.client_list[client_index], \
                   self.threshold_list[client_index], self.dfs_interval_list[client_index]
        else:
            return -1, 'All clients have completed their queries', -1, -1

    def get_client_threshold(self, client_index):
        return self.threshold_list[client_index]

    def set_client(self, client, ind):
        self.client_list[ind] = client

    def set_exploration_responses(self, responses):
        if responses[0] is not None:
            self.exploration_responses_dpcache.append(responses[0])
        if responses[1] is not None:
            self.exploration_responses_apex.append(responses[1])
        if not self.is_hd and responses[2] is not None:
            self.exploration_responses_pioneer.append(responses[2])

    def set_exploration_response_apex_with_cache(self, response):
        self.exploration_responses_apex_naive_cache = response

    def save_exploration_response(self, dirs):
        if dirs[0] is not None:
            with open(dirs[0], 'wb') as file:
                pickle.dump(self.exploration_responses_dpcache, file)
        if dirs[1] is not None:
            with open(dirs[1], 'wb') as file:
                pickle.dump(self.exploration_responses_apex, file)
            with open(dirs[2], 'wb') as file:
                pickle.dump(self.exploration_responses_apex_naive_cache, file)
        if not self.is_hd and dirs[3] is not None:
            with open(dirs[3], 'wb') as file:
                pickle.dump(self.exploration_responses_pioneer, file)

class Ticker():
    def __init__(self, size, accuracy=3):
        super().__init__()

        self.size = size
        self.index = 0
        self.pct = 0
        self.start_time = time.time()
        self.accuracy = accuracy

    def tick(self, preamble='', postamble=''):
        self.index += 1
        if int((self.index / self.size) * 100 * (10 ** self.accuracy)) > self.pct:
            self.pct = int((self.index / self.size) *
                           100 * (10 ** self.accuracy))
            val = self.index / self.size

            estimate = (self.size - self.index) * \
                ((time.time() - self.start_time) / self.index)
            estimate = math.ceil(estimate / 60)

            if self.accuracy >= 3:
                print(
                    f'{preamble}{val:.3%} (~{estimate}m remaining) {postamble}                   \r', end="")
            elif self.accuracy >= 2:
                print(
                    f'{preamble}{val:.2%} (~{estimate}m remaining) {postamble}                   \r', end="")
            else:
                print(
                    f'{preamble}{val:.1%} (~{estimate}m remaining) {postamble}                   \r', end="")
