import argparse
from timeit import default_timer as timer
from datetime import timedelta
import time
import os

from cachedp.api.data_vector import DataVector
from cachedp.api.helpers_sql_tuple import *
from cachedp.api.Modules.MMM.estimate_MMM import *
from cachedp.api.Modules.MMM.run_MMM import *
from cachedp.api.Modules.RelaxPrivacy.estimate_RP import *
from cachedp.api.Modules.RelaxPrivacy.execute_RP import run_relax_privacy
from cachedp.api.Query import Query

MECHANISMS = ['MMM', 'strategy_expander', 'relax_privacy']
# RESULT_PATH='ripple_results/'
# W_LIST = ['identity', 'overlapping', 'identity_singles',
#           'overlap_singles', 'random', 'random2']
#
# for W in W_LIST:
#     if not os.path.exists(RESULT_PATH + W):
#         os.makedirs(RESULT_PATH + W)

def init(is_correlated=False):
    return Cache(is_correlated)

def check_chosen_module_valid(args, module_index: int):
    if module_index == 0:
        assert args.mmm
    if module_index == 1:
        assert args.mmm and args.strategy_expander
    if module_index == 2:
        assert args.relax_privacy

def answer_queries(args: argparse, unmultiplied_alpha: float, alpha: float, infoTupleList: AttributeInfoTupleList,
                   data_vector: DataVector, workload_tuples: NamedMarginalsList, cache: Cache, logger, rng,
                   variance: float=None, is_correlated=False):
    if os.getenv('VERBOSE') == '1':
        print('Workload Tuples: ', workload_tuples)

    logger.update_queries(workload_tuples, alpha)
    query = Query(workload_tuples, alpha, args.beta, variance, infoTupleList,
                  data_vector, args.se_parameter, rng, is_correlated)
    query.set_free_paid_and_cache_entries(cache)
    # decide if Relax Privacy is applicable at all.
    can_relax_privacy = query.can_relax_privacy()

    # if all 3 modules are turned off, return directly
    modules_available = [args.mmm, args.strategy_expander, args.relax_privacy]
    module_eps = [EPSILON_INFINITY, EPSILON_INFINITY, EPSILON_INFINITY]
    module_est_runtime = [0, 0, 0]
    b_min, b_p = 0, 0
    start = time.time()
    if any(modules_available):
        if args.mmm:
            (module_est_runtime[0], res) = instrument_fn_call(estimate_MMM_cost, query, cache, args)
            module_eps[0], b_min, b_p = res
            if args.strategy_expander and module_eps[0] > 0:
                (module_est_runtime[1], module_eps[1]) = instrument_fn_call(estimate_SE_cost, query, cache, args)

        if module_eps[0] > 0 and module_eps[1] > 0 and args.relax_privacy and can_relax_privacy:
            (module_est_runtime[2], response) = instrument_fn_call(estimate_relax_privacy_cost, query, cache, args)
            if not isinstance(response, str):
                module_eps[2] = response
    else:
        print('Please turn on at least one module to execute the queries')
        exit(0)

    if os.getenv('VERBOSE') == '1':
        print('Module Epsilons: MMM, SE, RP: ', module_eps)

    # Comparison Module
    # proactive, MMM, SE, relax_privacy
    #TODO: Add instrument_fn_call to the rest of these functions.
    chosen_module_index = module_eps.index(min(module_eps))
    check_chosen_module_valid(args, chosen_module_index)
    if chosen_module_index < 2:  # MMM +/- SE
        # args.strategy_expander could be on but we could choose mmm as more cost effective
        run_SE = True if chosen_module_index == 1 else False
        strategy_tuples, free_paid_vector = query.get_chosen_strategy_info(run_SE)
        if args.proactive:
            proactive_tuples = query.set_proactive_strategy(cache, run_SE)
        else:
            proactive_tuples = []
        if os.getenv('VERBOSE') == '1':
            print('In run_mmm')
            print(
                f'\tfree_paid_vector: {sum(free_paid_vector)} paid of {len(free_paid_vector)}')
            print('\tStrategy Used:', strategy_tuples)
            print('\tProactive Queries Obtained', proactive_tuples)
        len_strategy_tuples = len(free_paid_vector) - sum(free_paid_vector)
        strategy_responses = run_mmm(query, cache, run_SE)
        mmm_or_se_or_rp = 1 if run_SE else 0
    else:
        assert args.relax_privacy
        rp_comparer = query.get_rp_comparer()
        rp_strategy_matrix = rp_comparer.get_best_old_strategy().get_strategy()
        rp_strategy_tuples = data_vector.from_matrix_to_tuple(rp_strategy_matrix, is_correlated)
        if os.getenv('VERBOSE') == '1':
            print('In run_relax_privacy')
            print('Workload Tuples:', workload_tuples)
            print('\tStrategy Used:', rp_strategy_tuples)
        len_strategy_tuples = len(rp_strategy_tuples)
        strategy_responses = run_relax_privacy(query, cache)
        mmm_or_se_or_rp = 2

    # Then finally multiply noisy responses with workload appropriately
    workload_response = query.answer_workload(mmm_or_se_or_rp, strategy_responses)

    epsilon_used = max(min(module_eps), 0)
    true_response = query.get_true_response(cache)
    # print(strategy_responses, workload_response)
    end = time.time()
    return true_response, workload_response, epsilon_used, MECHANISMS[module_eps.index(min(module_eps))], \
           len_strategy_tuples, end-start, cache.get_cache_size(), b_min, b_p

def instrument_fn_call(fn, *args):
    #See https://stackoverflow.com/questions/7370801/how-to-measure-elapsed-time-in-python/25823885#25823885
    start = timer()
    output = fn(*args)
    end = timer()
    wall_clock_duration = timedelta(seconds=end-start)
    return wall_clock_duration, output
