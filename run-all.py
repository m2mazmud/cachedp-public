import argparse
import csv
import datetime
import pandas as pd
import matplotlib.pyplot as plt
# from dataset.data_preprocessing import *
# from cachedp.api.util_fns import *
# from apex.run_query import apex_dump_sql
# from scripts.exploration import *
# from scripts.hd_sql_parser import nyc_taxi_dataset_load_info_tuple_lists
# from scripts.sql_benchmark import run_sql_benchmark
# from scripts.synthetic_queries import run_synthetic_experiment
from scripts.gen_plots import *
import os

plt.rcParams.update({'font.size': 14})

col = {
    'APEx': 'C0',
    'APEx with Cache': 'C1',
    'Pioneer': 'C2',
    'CacheDP': 'C3'
}

markers = {
    'APEx': '--',
    'APEx with Cache': '-.',
    'Pioneer': '-',
    'CacheDP': ':'
}

os.system("cd dataset && bash setup.sh")

parser = argparse.ArgumentParser()
parser.add_argument('--mmm', type=int, default=1)
parser.add_argument('--relax_privacy', type=int, default=1)
parser.add_argument('--strategy_expander', type=int, default=1)
parser.add_argument('--proactive', type=int, default=1)
parser.add_argument('--use_cache', type=int, default=1)
parser.add_argument('--use_variance', type=int, default=0)
parser.add_argument('--verbose', type=str, default='0')
parser.add_argument('--dataset', type=str, default='adult')
parser.add_argument('--attribute', type=str, default='age')
parser.add_argument('--alpha_min', type=float, default=0.01)
parser.add_argument('--alpha_max', type=float, default=0.16)
parser.add_argument('--alpha_step_size', type=float, default=0.005)
parser.add_argument('--beta', type=float, default=0.05)
parser.add_argument('--se_parameter', type=float, default=1)
parser.add_argument('--max_strategy', type=int, default=500)
parser.add_argument('--num_users', type=int, default=5)
parser.add_argument('--is_correlated', type=int, default=0)
parser.add_argument('--dfs_interval_min', type=int, default=5)
parser.add_argument('--dfs_interval_max', type=int, default=20)
parser.add_argument('--threshold', type=int, default=1000)
parser.add_argument('--threshold_step', type=int, default=50)
parser.add_argument('--num_thresholds', type=int, default=30)
parser.add_argument('--seed_offset', type=int, default=0)
parser.add_argument('--full_mode', type=int, default=1)
parser.add_argument('--analysis_only', type=int, default=0)
parser.add_argument('--runs', type=int, default=100)
parser.add_argument('--number_workers', type=int, default=3)
args = parser.parse_args()
os.environ['VERBOSE'] = args.verbose  # change to print messages

print("First setup datasets.")
if args.full_mode: args.runs = 100
hyper_list = [0.6, 0.8, 1, 1.2, 1.4, 2]
result_path = 'results/'
exps = ['bfs', 'dfs', 'synthetic', 'sql_benchmark']
if not os.path.exists(result_path):
    os.makedirs(result_path)

if not args.analysis_only:
    df = pd.read_csv('dataset/adult.csv')
    df.insert(0, 'id', range(1, 1 + len(df)))
    df, _ = encode_dataset(df, binary_encoding=False, class_name=None)
    apex_dump_sql(df, 'adult')

    df1, workload_margin_lists, attr_domains, attribute_granularities, per_workload_granularities = \
        nyc_taxi_dataset_load_info_tuple_lists(args.full_mode)
    df1.insert(0, 'id', range(1, 1 + len(df1)))
    df1, _ = encode_dataset(df1, binary_encoding=False, class_name=None)
    apex_dump_sql(df1, 'flight')
    domain_size = 1000
    raw_data_vector = np.arange(0, domain_size)
    df2 = pd.DataFrame(raw_data_vector, columns=['attr'])
    df2.insert(0, 'id', range(1, 1 + len(df2)))
    apex_dump_sql(df2, 'synthetic')

    print("Run evaluation experiments.")
    if args.full_mode: args.num_users = 25
    # bfs queries
    print('Standard BFS queries')
    run_experiment(args, df, 'bfs')
    print('BFS queries with varying hyperparameter')
    for hyper in hyper_list:
        args.se_parameter = hyper
        run_experiment(args, df, 'bfs', varyhyper=True)
    args.relax_privacy = 0
    print('BFS queries without RP')
    run_experiment(args, df, 'bfs', ablation=True)
    args.relax_privacy, args.strategy_expander = 1, 0
    print('BFS queries without SE')
    run_experiment(args, df, 'bfs', ablation=True)
    args.strategy_expander, args.proactive = 1, 0
    print('BFS queries without PR')
    run_experiment(args, df, 'bfs', ablation=True)
    args.proactive = 1
    # dfs queries
    print('Standard DFS queries')
    args.attribute = 'country'
    run_experiment(args, df, 'dfs')
    for hyper in hyper_list:
        args.se_parameter = hyper
        run_experiment(args, df, 'dfs', varyhyper=True)
    args.relax_privacy = 0
    print('DFS queries without RP')
    run_experiment(args, df, 'dfs', ablation=True)
    args.relax_privacy, args.strategy_expander = 1, 0
    print('DFS queries without SE')
    run_experiment(args, df, 'dfs', ablation=True)
    args.strategy_expander, args.proactive = 1, 0
    print('DFS queries without PR')
    run_experiment(args, df, 'dfs', ablation=True)
    args.proactive = 1
    # synthetic queries
    args.dataset = 'synthetic'
    print('Standard synthetic queries')
    run_synthetic_experiment(args, df2, domain_size, raw_data_vector)
    print('Synthetic queries ablation study')
    run_synthetic_experiment(args, df2, domain_size, raw_data_vector, ablation=True)
    # SQL benchmark
    print('SQL benchmark')
    if args.full_mode: args.num_users = 10
    run_sql_benchmark(args, df1, workload_margin_lists, attr_domains, attribute_granularities,
                      per_workload_granularities, 'sql_benchmark')
    # SQL benchmark Details
    print('SQL benchmark Details')
    args.num_users = 1
    run_sql_benchmark(args, df1, workload_margin_lists, attr_domains, attribute_granularities,
                      per_workload_granularities, 'sql_benchmark_details')

print("Plot results.")
cur_time_no_slash = datetime.datetime.now().isoformat()
cur_time = cur_time_no_slash + '/'
experiment_timestamped_dir = 'experiment_graphs/' + cur_time
os.makedirs(experiment_timestamped_dir)
if not os.path.exists(experiment_timestamped_dir):
    os.makedirs(experiment_timestamped_dir)

abl_out, overhead_df, module_selection_df = get_exp_res(args, cur_time)

gen_cumulative_plot_varyhyper(args, cur_time)

# TODO: This is the code to generate sql details exp
gen_detail_sql_benchmark(args, cur_time)
if abl_out: 
    with open(experiment_timestamped_dir + 'ablation.csv', 'w') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(['Tasks', 'Mode', 'Cumulative Privacy Budget', 'Error'])
        for ele in abl_out:
            writer.writerow(ele)

scores_df = pd.read_csv(experiment_timestamped_dir + 'ablation.csv')
gen_abl_plot(scores_df, cur_time)

if overhead_df: 
    with open(experiment_timestamped_dir + 'table2.csv', 'w') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(['Systems', 'RRQ', 'IDEBench', 'DFS', 'IDEBench'])
        systems = ['APEx(Cache)', 'Pioneer', 'CacheDP']
        for i in range(3):
            writer.writerow((systems[i], str(overhead_df['synthetic'][i]), str(overhead_df['sql_benchmark cache size'][i]),
                            str(overhead_df['dfs'][i]), str(overhead_df['sql_benchmark runtime'][i])))

if module_selection_df: 
    with open(experiment_timestamped_dir + 'shepherd.csv', 'w') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(['BFS', 'DFS', 'RRQ', 'IDEBench'])
        systems = ['MMM Paid', 'MMM Free', 'RP Total', 'SE Total', 'SE Free']
        for i in range(5):
            writer.writerow((systems[i], str(module_selection_df['bfs'][i]), str(module_selection_df['dfs'][i]),
                            str(module_selection_df['synthetic'][i]), str(module_selection_df['sql_benchmark'][i])))

zip_name = "results-" + cur_time_no_slash + ".zip"
zip_cmd = "cd experiment_graphs/ && " + \
          "chmod 777 -R \"" + cur_time_no_slash + "\" && " + \
          "zip -r \"" + zip_name + "\" \"" + cur_time_no_slash + "\" && " + \
          "chmod 777 \"" + zip_name + "\""
os.system(zip_cmd)
