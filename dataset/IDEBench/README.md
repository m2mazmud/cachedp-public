## IDE Benchmark
The code for the benchmark can be found at https://github.com/IDEBench/IDEBench-public.

To create the SQL files found in ```/sql_workflows``` we ran 

```python3 idebench.py --run-config runconfig_sample.json```

using the config file and driver included in this directory.

To keep the size of the data vector reasonable, we modified the width of the TAXI_OUT attribute binning from 26 to 27 in these SQL files. We also deleted one larger query. We finally replace all AVG with COUNT(*).

After obtaining the raw SQL for these default workflows, we ran `scripts/hd_sql_parser.py` to create the range queries by splitting disjunctions into workloads of their arguments. This script loads the processed queries into memory to processed at runtime.


