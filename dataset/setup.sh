#!/bin/bash
if [[ ! -f datasets.zip ]]; then
  #Adult dataset
  mkdir adult
  echo "Downloading adult dataset"
  cd adult
  curl -s https://archive.ics.uci.edu/ml/machine-learning-databases/adult/adult.test > adult.test
  curl -s https://archive.ics.uci.edu/ml/machine-learning-databases/adult/adult.data > adult.data
  curl -s https://archive.ics.uci.edu/ml/machine-learning-databases/adult/adult.names > adult.names
  #Grabbing the column labels from adult.names
  grep -v "|" adult.names | grep ":" | cut -d: -f1 | tr '\n' ',' | sed 's/,$/,class\n/' > adult-from-web.csv
  #Dropping the first line (cross validation info) from adult.test and removing the period at the end of each line
  tail -n +2 adult.test | sed 's/.$//' >> adult2.test
  #Appending modified adult.test and then all of adult.data
  cat adult2.test >> adult-from-web.csv
  cat adult.data >> adult-from-web.csv
  python3 ../adult_replace_missing.py

  #Flight dataset
  cd ..
  unzip flight_dataset.zip
else
  unzip -o datasets.zip
fi