from sklearn.preprocessing import MinMaxScaler
from sklearn import preprocessing
import pandas as pd

def import_dataset(filename):
    dataframe = pd.read_csv(filename)
    return dataframe

def encode_dataset(dataframe, binary_encoding=True, class_name='class', permuatation=None):
    # convert class to ints
    if class_name is not None:
        dataframe[class_name] = dataframe[class_name].astype('category')
        dataframe[class_name] = dataframe[class_name].cat.codes
        # Split off the labels
        labels = dataframe[class_name].values
        dataframe.drop(columns=class_name, inplace=True)
    else:
        labels = None

    # get categorical attributes
    columns = dataframe.columns
    numeric_columns = dataframe._get_numeric_data().columns
    if permuatation is None:
        categorical_attributes = list(set(columns) - set(numeric_columns))
    else:
        categorical_attributes = permuatation

    if binary_encoding:
        #print('Converting categorial to numeric using a one hot encoding')
        binary_attributes = []
        other_attributes = []
        for attribute in categorical_attributes:
            if len(dataframe[attribute].unique()) == 2:
                binary_attributes.append(attribute)
            else:
                other_attributes.append(attribute)

        encoded_dataframe = pd.get_dummies(dataframe, columns=other_attributes)
        encoded_dataframe[binary_attributes] = encoded_dataframe[binary_attributes].astype(
            'category')
        encoded_dataframe[binary_attributes] = encoded_dataframe[binary_attributes].apply(
            lambda x: x.cat.codes)
    else:
        print('Converting categorial to numeric using an integer encoding')
        encoded_dataframe = dataframe.copy()
        encoded_dataframe[categorical_attributes] = encoded_dataframe[categorical_attributes].astype(
            'category')
        encoded_dataframe[categorical_attributes] = encoded_dataframe[categorical_attributes].apply(
            lambda x: x.cat.codes)
    return encoded_dataframe, labels

'''(0,1) min max normalization'''
def normalize_dataset(dataframe):
    features = dataframe.values
    scaler = MinMaxScaler()
    return scaler.fit_transform(features)

'''z-score smoothing'''
def scale_dataset(dataframe):
    features = dataframe.values
    return preprocessing.scale(features)
