The adult dataset is the US Adults census data obtained [from UCI ML lab](https://archive.ics.uci.edu/ml/machine-learning-databases/adult/). Running `setup.sh` will download and pre-process this dataset to replace unknown values ('?') with the mean for continuous variables and the mode for discrete variables. The final result is stored in `adult.csv`. We use it for our single-dimension evaluation.


The dataset in `fight_dataset.zip` consists of one CSV file: 
 `fight_dataset.csv`: This is the flights dataset used in the [Interactive Data Exploration benchmark](https://github.com/IDEBench/IDEBench-public/wiki) by Eichmann et al. It was generated using the ```python datagen.py --sample-file data/flights/sample.csv --sample-descriptor data/flights/sample.json --size 500000``` command as specified in the IDEBench Wiki.

Since the latitude and longitude columns in the NYC Taxi dataset has been publicly retracted after our experiments were conducted, we have not published this dataset here. This dataset was used to evaluate our system against correlated two-dimension queries.   
