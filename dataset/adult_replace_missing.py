import pandas as pd
from sklearn.impute import SimpleImputer

filename='adult-from-web.csv'
dataframe = pd.read_csv(filename)
if 'native-country' in dataframe.columns:
    dataframe.rename(columns = {'native-country':'country'}, inplace = True)
imp = SimpleImputer(missing_values=" ?", strategy="most_frequent")
dataframe = pd.DataFrame(imp.fit_transform(dataframe),
                   columns=dataframe.columns,
                   index=dataframe.index)
dataframe.to_csv('adult.csv', index=False)