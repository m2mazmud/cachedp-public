import time
from apex.query import Query
from apex.query.Type import QueryType, MechanismType, QueryReturnType
from apex.privacy import PrivacyEngine
import mysql.connector
from sqlalchemy import create_engine
from apex.query.query_gen import *
import sys
sys.path.append('../')
from cachedp.api.data_vector import *
from cachedp.api.helpers_sql_tuple import *

default_workload_size = 100
default_icq_c_ratio = 0.1

default_topk = 10
alpha_ratio = 0.02

def apex_dump_sql(df, dataset):
    if dataset == 'adult':
        dataset = 'census'
        table = 'income'
    elif dataset == 'flight':
        table = 'flight_info'
    elif dataset == 'synthetic':
        table = 'syn'
    else:
        dataset = 'location'
        table = 'latlong'
    engine = create_engine("mysql+pymysql://root:rootPass@localhost:3306/" + dataset)
    with engine.begin() as connection:
        df.to_sql(table, con=connection, index=False, if_exists="replace")

def apex_answer_query(args, unmultiplied_alpha, alpha, infoTupleList, data_vector,
                      workload_tuples, cache, logger, rng, variance=None, is_correlated=False):
    icq_c, tcq_k, dsize = -1, -1, 1
    mpm_poking = 10
    if variance is not None:
        m_types = [MechanismType.LM]
    else:
        m_types = [MechanismType.LM, MechanismType.LM_SM]
    query_type, query_return_type = QueryType.WCQ, QueryReturnType.COUNT
    cond_list = data_vector.tuple_to_sql(workload_tuples)
    start = time.time()
    in_cache = logger.update_queries(workload_tuples, alpha)
    budget = float('inf')
    if args.dataset == 'adult':
        dataset = 'census'
    else:
        dataset = args.dataset
    pe = PrivacyEngine.PrivacyEngine(budget, dataset, rng)

    if cond_list is None:
        print('ERROR: No query found')
        exit(0)

    crnt_q = Query.Query()

    crnt_q.force = True
    crnt_q.is_hist = True
    crnt_q.set_cond_list(cond_list)
    crnt_q.set_return_type(query_return_type)
    crnt_q.set_data_and_size(dataset, dsize)
    crnt_q.set_query_type(query_type, icq_c, mpm_poking, tcq_k)

    best_noisy_ans = None
    best_cost = math.inf
    for m_type in m_types:
        crnt_q.set_mechanism_type(m_type)

        re_m = pe.run_query(crnt_q, alpha, args.beta)
        if re_m.real_cost < best_cost:
            best_noisy_ans = re_m.query.noisy_answer
            best_cost = re_m.real_cost

    end = time.time()
    logger.log_apex_query(np.array(re_m.query.true_answer), np.array(best_noisy_ans),
                          best_cost, end - start, in_cache, alpha)
    return np.array(re_m.query.true_answer), np.array(best_noisy_ans), best_cost, '', 0, end - start, 0, 0, 0

