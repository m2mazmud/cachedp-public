import math

SMALL_VALUE = 1e-10

def get_query_from_attr_name(workload):
    attribute_query_mappings = {}
    for tup in workload:
        queries, attribute_names = tup[0], tup[1]
        for attr_ind in range(len(attribute_names)):
            if attribute_names[attr_ind] not in attribute_query_mappings.keys():
                attribute_query_mappings[attribute_names[attr_ind]] = [queries[attr_ind]]
            else:
                attribute_query_mappings[attribute_names[attr_ind]].append(queries[attr_ind])
    return attribute_query_mappings

# Get the info tuple list for the full domain with the min granularity
# Min granularity is a dictionary where each of them {attribute name: min granularity}

def gen_info_tuple_list(workload, attr_domains, attribute_granularities):
    unsorted_info_tuple_list = []
    for attr_name in attribute_granularities.keys():
        infotuple = (attr_domains[attr_name][0],
                     attr_domains[attr_name][1],
                     attribute_granularities[attr_name],
                     attr_name)
        unsorted_info_tuple_list.append(infotuple)
    sorted_info_tuple_list = sorted(unsorted_info_tuple_list, key=lambda x: (x[0][1] - x[0][0])/x[2])
    srt = {b[-1]: i for i, b in enumerate(sorted_info_tuple_list)}
    sorted_workload_margins = []
    for query in workload:
        sorted_workload_attribute_pair = sorted(zip(query[0], query[1]), key=lambda x: srt[x[1]])
        sorted_workload_margins.append(([x[0] for x in sorted_workload_attribute_pair],
                                        [x[1] for x in sorted_workload_attribute_pair]))
    return sorted_workload_margins, sorted_info_tuple_list


if __name__ == '__main__':
    # the datavector will somehow hard-code the domain which maps the attribute name to (domain, arity)
    domain = {'col1': ((0, 9), 2),
              'col2': ((0, 9), 3)}
    # Now we will generate dynamically how to find the smallest split of each attribute dynamically
    # the query for workloads are (query, attribute name)
    workload1 = [([(1, 3), (0, 4)], ['col1', 'col2']), ([(2, 6), (4, 6)], ['col1', 'col2'])]
    res1, workload1 = gen_info_tuple_list(workload1, domain)
    workload2 = [([(0, 6), (1, 3)], ['col1', 'col2']), ([(3, 6)], ['col1'])]
    d = {'col1': [0.1, 0.6, 0.7], 'col2': [0.3, 0.4, 0.5]}
    attribute_lists = ['col1', 'col2']
    attribute_ranges = [(-math.inf, 0.6), (0.4, math.inf)]