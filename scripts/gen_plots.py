import numpy as np
import pickle
import seaborn as sns
import scipy
import colorsys
import matplotlib.colors as mc
import matplotlib.pyplot as plt
import os 
from collections import OrderedDict
from matplotlib import ticker

HYPERS = {
    0.6: 'C0',
    0.8: 'C1',
    1: 'C2',
    1.2: 'C3',
    1.4: 'C4',
    2: 'C5'
}

col = {
    'APEx': 'C0',
    'APEx with Cache': 'C1',
    'Pioneer': 'C2',
    'CacheDP': 'C3'
}

markers = {
    'APEx': '--',
    'APEx with Cache': '-.',
    'Pioneer': '-',
    'CacheDP': ':'
}

exp_fig = {
    'bfs': 'fig4a',
    'dfs':  'fig4b',
    'synthetic': 'fig4c',
    'sql_benchmark': 'fig4f'
}

def mean_std(data):
    a = 1.0 * np.array(data)
    if len(a) == 1: return a[0], a[0], a[0]
    m, se = np.mean(a), scipy.stats.sem(a)
    return m, se

def mean_confidence_interval(data, confidence=0.95):
    a = 1.0 * np.array(data)
    if len(a) == 1: return a[0], a[0], a[0]
    n = len(a)
    m, se = np.mean(a), scipy.stats.sem(a)
    h = se * scipy.stats.t.ppf((1 + confidence) / 2., n - 1)
    return m - h, m, m + h

def adjust_lightness(color, amount=0.5):
    try:
        c = mc.cnames[color]
    except:
        c = color
    c = colorsys.rgb_to_hls(*mc.to_rgb(c))
    return colorsys.hls_to_rgb(c[0], max(0, min(1, amount * c[1])), c[2])

def get_cum_arrays(array):
    res = []
    for ele in array:
        if not res: res.append(ele)
        else:
            res.append(ele + res[-1])
    return res

def errplot(x, y, yerr, hue, **kwargs):
    data = kwargs.pop('data')
    p = data.pivot_table(index=x, columns=hue, values=y)
    err = data.pivot_table(index=x, columns=hue, values=yerr)
    p.plot(kind='bar', yerr=err, ax=plt.gca(), **kwargs)

def gen_abl_plot(scores_df, cur_time):
    sns.set_theme()
    sns.set(font_scale = 1.3)
    sns.set_style("whitegrid")
    g = sns.FacetGrid(scores_df, height=5, aspect=1.2)
    g.map_dataframe(errplot, "Tasks", "Cumulative Privacy Budget", "Error", "Mode", color=['C0', 'C3', 'C2', 'C7'], width=0.8)

    g.set_xticklabels(rotation=30, ha="right")
    plt.legend(loc='upper left')
    plt.tight_layout()
    plt.savefig("experiment_graphs/" + cur_time + "Fig5.png", dpi=600)

def gen_double_y_axis_plot_detail_sql_benchmark(ax1, x_axis, x_axis_title, cache_sizes, runtimes): 
    ax1.set_xlabel(x_axis_title)

    ax1.plot(x_axis, [sizes[1] for sizes in cache_sizes],
             color=adjust_lightness('C0', amount=1.3), marker=".", linestyle='dashed')
    ax1.set_ylabel('Cache size (# of entries)', color=adjust_lightness('C0', amount=1.3))
    ax1.fill_between(x_axis, [sizes[0] for sizes in cache_sizes], [sizes[2] for sizes in cache_sizes],
                     alpha=0.3, color=adjust_lightness('C0', amount=1.3))
    ax1.yaxis.set_ticks(np.arange(0, 14000, 2000 ))
    ax1.yaxis.set_major_formatter(ticker.EngFormatter())
    ax1.tick_params(axis='y', labelcolor=adjust_lightness('C0', amount=1.3))
    
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    #Convert to runtimes by hours. 
    runtimes = [(ci_minus/2400, mean/2400, ci_plus/2400) for (ci_minus, mean, ci_plus) in runtimes]
    ax2.plot(x_axis, [times[1] for times in runtimes],
             color=adjust_lightness('C1', amount=1.3), marker=".", linestyle='dashed')
    ax2.set_ylabel('Runtime (s)', color=adjust_lightness('C1', amount=1.3))
    ax2.fill_between(x_axis, [times[0] for times in runtimes], [times[2] for times in runtimes],
                     alpha=0.3, color=adjust_lightness('C1', amount=1.3))
    ax2.tick_params(axis='y', labelcolor=adjust_lightness('C1', amount=1.3))


def gen_cumulative_plot_varyhyper(args, cur_time):
    result_path = 'results/'
    for exp in ['bfs_extension', 'dfs_extension']:
        plt.xlabel('Workload Index')
        plt.ylabel('Cumulative Privacy Budget')
        plt.rcParams.update({'font.size': 14})
        for hyper in HYPERS.keys():
            cumulative_cost = []
            for run in range(args.runs):
                res = pickle.load(open(result_path + exp + '/result_se_' + str(hyper) + '_' + str(run) + '.p', 'rb'))
                cur_cost = [x[2] for x in res]
                cumulative_cost.append(cur_cost)
            lengths = [len(ele) for ele in cumulative_cost]
            if lengths:
                max_length = max(lengths)
            query_ind_list = np.arange(1, max_length + 1, 1)
            query_by_runs, query_by_indices = [], []
            for ele in cumulative_cost:
                cumulative_array = [sum(ele[:j]) for j in range(len(ele))]
                query_by_runs.append(cumulative_array)
            for j in range(max_length):
                tmp = []
                for ele in query_by_runs:
                    if len(ele) > j:
                        tmp.append(ele[j])
                query_by_indices.append(tmp)

            cost_array = [mean_confidence_interval(values) for values in query_by_indices]
            if cost_array:
                plt.plot(query_ind_list, [eps[1] for eps in cost_array], ':',
                         color=adjust_lightness(HYPERS[hyper], amount=1.3), label='Hyper parameter: ' + str(hyper))
                plt.fill_between(query_ind_list, [eps[0] for eps in cost_array], [eps[2] for eps in cost_array],
                                 alpha=0.3,
                                 color=adjust_lightness(HYPERS[hyper], amount=1.3))
        plt.legend()
        if exp == 'dfs_extension':
            plt.ylim(0, 0.2)
        plt.show()
        plt.savefig('experiment_graphs/' + cur_time + exp + '.png', dpi=600)
        plt.clf()

def gen_detail_sql_benchmark(args, cur_time):
    result_path, num_queries = 'results/', 27
    attribute_involved = [1, 1, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 6, 7, 8, 8, 8, 8, 8]
    cache_sizes, runtimes, cache_size_by_query, runtime_by_query = [], [], [], []
    for run in range(args.runs):
        res = pickle.load(open(result_path + 'sql_benchmark_details/result_' + str(run) + '.p', 'rb'))
        runtime = [x[5] for x in res] #x[6] in the old log format (?) 
        runtimes.append(runtime)
        cache_usage = [x[6] for x in res] #x[7] in the old log format (?) 
        cache_sizes.append(cache_usage)
    # [[runtime for the first query over 100 runs], [runtime for the second query over 100 runs]..]
    for q_id in range(num_queries):
        cache_size_by_query.append([x[q_id] for x in cache_sizes])
        runtime_by_query.append([x[q_id] for x in runtimes])
    #Grab the ith element of each row for the ith run, and then sum over all 
    runtime_by_query = np.cumsum(runtime_by_query, axis=0)

    mean_cache_sizes = [mean_confidence_interval(cache_size) for cache_size in cache_size_by_query]
    mean_runtimes = [mean_confidence_interval(runtime) for runtime in runtime_by_query]

    #Code for attribute-wise plot
    numbers_of_attributes = list(OrderedDict.fromkeys(attribute_involved))
    start_query_ids_by_attr = [attribute_involved.index(attr_count) for attr_count in numbers_of_attributes]

    query_ranges_by_attributes = []
    for prev,cur in zip(start_query_ids_by_attr, start_query_ids_by_attr[1:]): 
        query_ranges_by_attributes.append((prev, cur))
    last_attribute_query_start = query_ranges_by_attributes[-1][1]
    query_ranges_by_attributes.append((last_attribute_query_start, num_queries-1))

    cache_size_by_attr, runtime_by_attr = [], []
    for attr_count, query_range in zip(numbers_of_attributes, query_ranges_by_attributes):   
        query_start_index, query_end_index = query_range  

        cache_sizes_for_attr = mean_cache_sizes[query_start_index:query_end_index]
        max_cache_size = max(cache_sizes_for_attr, key=lambda x:x[1])
        cache_size_by_attr.append(max_cache_size)

        runtimes_for_attr = mean_runtimes[query_start_index:query_end_index]
        max_runtime = max(runtimes_for_attr, key=lambda x:x[1])
        runtime_by_attr.append(max_runtime)

    query_ind_list = np.arange(num_queries)
    fig, ax1 = plt.subplots()
    gen_double_y_axis_plot_detail_sql_benchmark(ax1, query_ind_list, 'Query index', mean_cache_sizes, mean_runtimes)
    for attr_count, query_id in zip(numbers_of_attributes, start_query_ids_by_attr):
        str_to_annotate = str(attr_count)
        x = query_ind_list[query_id]
        y = mean_cache_sizes[query_id][1]
        if attr_count < 5: 
            point = (x, y*1.05)
        else:
            point = (x, y)
        ax1.annotate(str_to_annotate, point, fontsize=9)
    plt.savefig('experiment_graphs/' + cur_time + 'sql_benchmark_details-by-queries.png', dpi=600, bbox_inches='tight')
    plt.clf()
    
    fig, ax1 = plt.subplots()
    gen_double_y_axis_plot_detail_sql_benchmark(ax1, numbers_of_attributes, 'Number of attributes', cache_size_by_attr, runtime_by_attr)
    plt.savefig('experiment_graphs/' + cur_time + 'sql_benchmark_details-by-attr.png', dpi=600, bbox_inches='tight')
    plt.clf()


def get_exp_res(args, cur_time):
    result_path = 'results/'
    supported_exps = ['bfs', 'dfs', 'synthetic', 'sql_benchmark']
    exps = []
    for exp in supported_exps: 
        if os.path.isdir(result_path + exp):
            exps.append(exp)
    overhead_df, module_selection_df = {}, {}
    abl_out = []
    # cumulative privacy budgets
    for exp in exps:
        plt.xlabel('Workload Index')
        plt.ylabel('Cumulative Privacy Budget')
        plt.rcParams.update({'font.size': 14})
        rp_off_costs, se_off_costs, pr_off_costs = [], [], []
        cache_dp_costs, apex_costs, apex_cache_costs, pioneer_costs = [], [], [], []
        cache_dp_runtimes, apex_runtimes, pioneer_runtimes = [], [], []
        cache_dp_usages, apex_cache_usages, pioneer_cache_usages = [], [], []
        module_usage_total = []
        for run in range(args.runs):
            if exp in ['bfs', 'dfs', 'synthetic']:
                rp_off_res = pickle.load(open(result_path + exp + '/result_rp_off_' + str(run) + '.p', 'rb'))
                se_off_res = pickle.load(open(result_path + exp + '/result_se_off_' + str(run) + '.p', 'rb'))
                pr_off_res = pickle.load(open(result_path + exp + '/result_pr_off_' + str(run) + '.p', 'rb'))
                rp_off_costs.append([x[2] for x in rp_off_res])
                se_off_costs.append([x[2] for x in se_off_res])
                pr_off_costs.append([x[2] for x in pr_off_res])
            apex_res = pickle.load(open(result_path + exp + '/apex_result_' + str(run) + '.p', 'rb'))
            apex_cache_res = pickle.load(open(result_path + exp + '/apex_cache_result_' + str(run) + '.p', 'rb'))
            res = pickle.load(open(result_path + exp + '/result_' + str(run) + '.p', 'rb'))
            if apex_res: 
                apex_cost, apex_runtime = [x[2] for x in apex_res], [x[5] for x in apex_res]
                apex_costs.append(apex_cost)
                apex_runtimes.append(np.mean(apex_runtime))
            if apex_cache_res: 
                apex_cache_used, apex_cache_cost = [x[6] for x in apex_cache_res], [x[2] for x in apex_cache_res]
                apex_cache_usages.append(apex_cache_used[-1])
                apex_cache_costs.append(apex_cache_cost)
            if res: 
                cachedp_cost, cache_dp_cache_used, cache_dp_runtime = [x[2] for x in res], \
                                                                    [x[6] for x in res], \
                                                                    [x[5] for x in res]
            module_usage = {'MMM_free': 0, 'SE_free': 0}
            for query in res:
                if query[2] > 0 or query[3] != 'MMM':
                    if query[3] in module_usage:
                        module_usage[query[3]] += 1
                    else:
                        module_usage[query[3]] = 1
                    if query[2] == 0 and query[3] == 'strategy_expander':
                        module_usage['SE_free'] += 1
                else:
                    module_usage['MMM_free'] += 1
            module_usage_total.append(module_usage)
            cache_dp_costs.append(cachedp_cost)
            cache_dp_usages.append(cache_dp_cache_used[-1])
            cache_dp_runtimes.append(np.mean(cache_dp_runtime))
            if exp != 'sql_benchmark':
                pioneer_res = pickle.load(open(result_path + exp + '/pioneer_result_' + str(run) + '.p', 'rb'))
                pioneer_cost, pioneer_cache_used, pioneer_runtime = [sum(x[2]) for x in pioneer_res], \
                                                                    [x[6] for x in pioneer_res], \
                                                                    [x[5] for x in pioneer_res]
                pioneer_cache_usages.append(pioneer_cache_used[-1])
                pioneer_costs.append(pioneer_cost)
                pioneer_runtimes.append(np.mean(pioneer_runtime))

        if exp == 'dfs':
            runtimes = [apex_runtimes, pioneer_runtimes, cache_dp_runtimes]
            overhead_df[exp] = [mean_std(x) for x in runtimes]
        elif exp == 'synthetic':
            cache_sizes = [apex_cache_usages, pioneer_cache_usages, cache_dp_usages]
            overhead_df[exp] = [mean_std(x) for x in cache_sizes]
        elif exp == 'sql_benchmark':
            overhead_df[exp + ' cache size'] = [mean_std(apex_cache_usages), '-',
                                                mean_std(cache_dp_usages)]
            overhead_df[exp + ' runtime'] = [mean_std(apex_runtimes), '-',
                                             mean_std(cache_dp_runtimes)]
        module_selection_df[exp] = [mean_std([exp_per['MMM'] for exp_per in module_usage_total]),
                                    mean_std([exp_per['MMM_free'] for exp_per in module_usage_total]),
                                    mean_std([exp_per['relax_privacy'] for exp_per in module_usage_total]),
                                    mean_std([exp_per['strategy_expander'] for exp_per in module_usage_total]),
                                    mean_std([exp_per['SE_free'] for exp_per in module_usage_total])]
        if exp in ['bfs', 'dfs', 'synthetic']:
            std_ci = mean_confidence_interval([sum(x) for x in cache_dp_costs])
            pq_off_ci = mean_confidence_interval([sum(x) for x in pr_off_costs])
            rp_off_ci = mean_confidence_interval([sum(x) for x in rp_off_costs])
            se_off_ci = mean_confidence_interval([sum(x) for x in se_off_costs])
            abl_out.append((exp, 'Standard', round(std_ci[1], 3), round(std_ci[1] - std_ci[0], 3)))
            abl_out.append((exp, 'PQ off', round(pq_off_ci[1], 3), round(pq_off_ci[1] - pq_off_ci[0], 3)))
            abl_out.append((exp, 'SE off', round(se_off_ci[1], 3), round(se_off_ci[1] - se_off_ci[0], 3)))
            abl_out.append((exp, 'RP off', round(rp_off_ci[1], 3), round(rp_off_ci[1] - rp_off_ci[0], 3)))
        if pioneer_costs:
            c_arrays = [apex_costs, apex_cache_costs, pioneer_costs, cache_dp_costs]
            plot_names = ['APEx', 'APEx with Cache', 'Pioneer', 'CacheDP']
        else:
            c_arrays = [apex_costs, apex_cache_costs, cache_dp_costs]
            plot_names = ['APEx', 'APEx with Cache', 'CacheDP']
        for i in range(len(c_arrays)):
            lengths = [len(ele) for ele in c_arrays[i]]
            max_length = 0
            if lengths: 
                max_length = max(lengths)
            query_ind_list = np.arange(1, max_length + 1, 1)
            query_by_runs, query_by_indices = [], []
            for ele in c_arrays[i]:
                cumulative_array = [sum(ele[:j]) for j in range(len(ele))]
                query_by_runs.append(cumulative_array)
            for j in range(max_length):
                tmp = []
                for ele in query_by_runs:
                    if len(ele) > j:
                        tmp.append(ele[j])
                query_by_indices.append(tmp)
            
            cost_array = [mean_confidence_interval(values) for values in query_by_indices]
            if cost_array:
                plt.plot(query_ind_list, [eps[1] for eps in cost_array], markers[plot_names[i]],
                        color=adjust_lightness(col[plot_names[i]], amount=1.3), label=plot_names[i])
                plt.fill_between(query_ind_list, [eps[0] for eps in cost_array], [eps[2] for eps in cost_array], alpha=0.3,
                                color=adjust_lightness(col[plot_names[i]], amount=1.3))
        plt.legend()
        plt.savefig('experiment_graphs/' + cur_time + exp_fig[exp] + '.png', dpi=600)
        plt.clf()
    return abl_out, overhead_df, module_selection_df
