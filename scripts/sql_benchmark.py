import os
import multiprocessing
import numpy as np
from cachedp.api.util_fns import *
from cachedp.algorithm.main import answer_queries
from apex.run_query import apex_answer_query
from scripts.high_dimension import gen_info_tuple_list
from cachedp.api.data_vector import DataVector


def sql_benchmark(params):
    (answer_queries_funcs, sql_domains, args, df, attribute_granularities, run, rng, clients,
     dp_cache_logger, apex_logger, result_path) = params
    caches = clients.get_system_caches()
    ind, cache, apex_cache = None, caches[0], None
    loggers = [dp_cache_logger, apex_logger]
    while True:
        ind, client, _, _ = clients.get_incomplete_client(rng)
        if isinstance(client, str): break
        workload_margin_list, per_workload_gran = client.get_cur_hd_workload()
        sorted_workload_margins, sorted_info_tuple_list = gen_info_tuple_list(workload_margin_list, sql_domains,
                                                                              per_workload_gran)
        attr_names = [x[-1] for x in sorted_info_tuple_list]
        full_data_vector = DataVector(df, attr_names, sql_domains,
                                      [attribute_granularities[a] for a in attr_names], is_full=True)
        # hd_data_vector = DataVector(df, attr_names, sql_domains, [per_workload_gran[a] for a in attr_names])
        cache.set_cur_data_vector(full_data_vector)
        cache.add_full_data_vector(full_data_vector, attr_names)
        # try:
        results = []
        for func_ind in range(len(answer_queries_funcs)):
            if answer_queries_funcs[func_ind] is None:
                results.append(None)
                continue
            result = answer_queries_funcs[func_ind](args,
                          unmultiplied_alpha=client.get_current_alpha(),
                          alpha=client.get_current_alpha(),
                          infoTupleList=sorted_info_tuple_list,
                          data_vector=full_data_vector,
                          workload_tuples=sorted_workload_margins,
                          cache=cache,
                          logger=loggers[func_ind],
                          rng=rng,
                          variance=None,
                          is_correlated=args.is_correlated)
            results.append(result + (client.get_current_alpha(), ))
        clients.set_exploration_responses(results)
        clients.set_client(client, ind)
        # except:
        #    error_results = [args.sequential, args.bfs, args.proactive, args.strategy_expander, run]
        #    with open('error.p', 'wb') as file:
        #        pickle.dump(error_results, file)
    dirs = [None, None, None]
    dirs[0] = 'results/' + result_path + '/result_{}.p'.format(run)
    clients.set_exploration_response_apex_with_cache(apex_logger.get_apex_query_responses())
    if answer_queries_funcs[1]:
        dirs[1] = 'results/' + result_path + '/apex_result_{}.p'.format(run)
        dirs[2] = 'results/' + result_path + '/apex_cache_result_{}.p'.format(run)
    clients.save_exploration_response(dirs)


def run_sql_benchmark(args, df, workload_margin_lists, attr_domains, attribute_granularities,
                      per_workload_granularities, result_path):
    if result_path == 'sql_benchmark_details':
        hd_system_selections = [answer_queries, None]
    else:
        hd_system_selections = [answer_queries, apex_answer_query]
    sql_domains = {}
    jobs = []
    if not os.path.exists('results/' + result_path):
        os.makedirs('results/' + result_path)
    args.dataset = 'flight'
    for ele in attribute_granularities.keys():
        sql_domains[ele] = (attr_domains[ele][0], attr_domains[ele][1], attribute_granularities[ele])
    for run in range(args.runs):
        dp_cache_logger, apex_logger = Logger(), Logger()
        rng = np.random.default_rng(seed=run + args.seed_offset)
        clients = MultipleClients(args, hd_system_selections, None, len(df), None, args.is_correlated,
                                  HD_queries=workload_margin_lists, HD_query_granus=per_workload_granularities)
        params = (hd_system_selections, sql_domains, args, df, attribute_granularities,
                  run,
                  rng,
                  clients,
                  dp_cache_logger,
                  apex_logger,
                  result_path)
        jobs.append(params)
    pool = multiprocessing.Pool(processes=args.number_workers)
    pool.map(sql_benchmark, jobs)
    pool.close()
    pool.join()