# Note the in 1n_1.sql, it will always group by aliases
# TODO: Specifically, the marginal attributes of the queries are before the aggregators (COUNT),
#  and after the WHERE keyword but before the GROUPBY keyword
import math
from pandasql import sqldf
import pandas as pd
from cachedp.api.helpers_sql_tuple import get_range_tuple
from cachedp.api.data_vector import *
from dataset.data_preprocessing import encode_dataset

pysqldf = lambda q: sqldf(q, globals())

def get_all_workloads():
    with open('dataset/IDEBench/sql_workflows/1n_1.sql', 'r') as fin:
        wordloads = fin.readlines()
    return wordloads

# Pass the attribute min granularity hardcoded here
def parse_workloads(df, workloads, attribute_domains, state_map):
    per_workload_granularities, attr_granularities = [], {}
    workload_margin_lists = []
    for workload in workloads:
        workload = workload.strip('\n').strip('\t')
        attr_names, attr_margins, attribute_granularities = parse_one_workload(df, workload, attribute_domains, state_map)
        per_workload_granularity = {}
        for i in range(len(attr_names)):
            attr_name = attr_names[i]
            per_workload_granularity[attr_name] = attribute_granularities[i]
            if attr_name in attr_granularities:
                attr_granularities[attr_name] = math.gcd(int(attribute_granularities[i]), attr_granularities[attr_name])
            else:
                attr_granularities[attr_name] = attribute_granularities[i]
        per_workload_granularities.append(per_workload_granularity)
        workload_margin_list = get_cartesian_product(attr_names, attr_margins)
        workload_margin_lists.append(workload_margin_list)
    return workload_margin_lists, attr_granularities, per_workload_granularities

def get_attribute_min_max(df, attribute_name):
    attr_arr = df[attribute_name].to_numpy()
    return np.min(attr_arr), np.max(attr_arr)

# returns the info tuple list
def get_cartesian_product(attribute_names, attribute_ranges):
    current_margins = None
    for i in range(len(attribute_names)):
        if not current_margins:
            current_margins = [([query], [attribute_names[i]]) for query in attribute_ranges[i]]
        else:
            cur_attribute_name = attribute_names[i]
            prev_ind = attribute_names.index(cur_attribute_name)
            cur_attribute_range = attribute_ranges[i]
            new_margins = []
            for old_margin in current_margins:
                if prev_ind < i:
                    appended_new_margins = []
                    for query in attribute_ranges[i]:
                        if query[0] >= old_margin[0][prev_ind][1] or query[1] < old_margin[0][prev_ind][0]:
                            continue
                        else:
                            new_query_range = (max(query[0], old_margin[0][prev_ind][0]),
                                               min(query[1], old_margin[0][prev_ind][1]))
                            if new_query_range[0] == new_query_range[1]: continue
                        old_margin[0][prev_ind] = new_query_range
                        appended_new_margins.append(old_margin)
                else:
                    appended_new_margins = [(old_margin[0] + [query], old_margin[1] + [cur_attribute_name])
                                            for query in attribute_ranges[i]]
                new_margins += appended_new_margins
            current_margins = new_margins
    return current_margins

# requires the attribute min granularity to be hardcoded
def get_margin_with_granularity(attribute_domains, attribute_name, granularity):
    (a_min, a_max) = attribute_domains[attribute_name][0]
    range_tuples = get_range_tuple(a_max, a_min, granularity)
    margins = []
    cur_query_start = None
    margins = [(range_tuples[i], range_tuples[i+1]) for i in range(len(range_tuples)-1)]
    return margins

def parse_one_workload(df, workload, attribute_domains, state_map):
    attributes_involved = []
    attribute_granularities = []
    sql_sep = workload.split('COUNT(*) as count')
    # print(sql_sep)
    attributes_involved_with_alias = sql_sep[0].split(',')[:-1]
    for ele in attributes_involved_with_alias:
        if ' FLOOR' in ele:
            attribute = ele.split('FLOOR(')[1].split('/')[0]
            granularity = int(float(ele.split('FLOOR(')[1].split('/')[1].split(')')[0]))
            attributes_involved.append(attribute)
            attribute_granularities.append(granularity)
        else:
            if 'SELECT' not in ele: attribute = ele.split('AS')[0].strip(' ')
            else: attribute = ele.split('SELECT')[1].split('AS')[0].strip(' ')
            attributes_involved.append(attribute)
            attribute_granularities.append(1)
    # At this stage attribute_involved is the name of the attributes in the select statement
    # attribute granularity is the granularity of the select attributes
    attribute_margins = []
    for i in range(len(attributes_involved)):
        attr_name = attributes_involved[i]
        margins = get_margin_with_granularity(attribute_domains, attr_name, attribute_granularities[i])
        attribute_margins.append(margins)
    condition_tuples = []
    if 'WHERE' in workload:
        where_conditions = workload.split('WHERE')[1].strip(' ').split('GROUP BY')[0].strip(' ')[2: -2]
        if 'or' in where_conditions:
            condition_list = [x.strip(' ')[1:-1] for x in where_conditions.split('or')]
        else:
            condition_list = [where_conditions.strip(' ')[1:-1]]
        condition_tuples_dem = []
        after_splitpt, before_dem_attr_name, after_dem_attr_name = False, None, None
        before_dem_granu, after_dem_granu = 1, 1
        for condition in condition_list:
            if ')' in condition:
                after_splitpt = True
                conds = condition.split('and ((')
                cond1, cond2 = conds[0].strip(' ')[:-2], conds[1]
                attr_name = cond1.split('>=')[0].strip(' ')
                l_bound = float(cond1.split('>=')[1].split('and')[0].strip(' '))
                u_bound = float(cond1.split('<')[1].strip(' '))
                before_dem_attr_name = attr_name
                condition_tuples.append((l_bound, u_bound))
                condition = cond2
            if "'" in condition:
                tmp = condition.split('=')
                attr_name = tmp[0].strip(' ')
                year_or_cat_val = tmp[1].strip(' ').strip("'")
                if year_or_cat_val in state_map:
                    l_bound = state_map[year_or_cat_val]
                else:
                    l_bound = int(year_or_cat_val)
                u_bound = l_bound + 1
            else:
                attr_name = condition.split('>=')[0].strip(' ')
                l_bound = float(condition.split('>=')[1].split('and')[0].strip(' '))
                u_bound = float(condition.split('<')[1].strip(' '))
            if after_splitpt:
                condition_tuples_dem.append((l_bound, u_bound))
                after_dem_attr_name, after_dem_granu = attr_name, u_bound-l_bound
            else:
                condition_tuples.append((l_bound, u_bound))
                before_dem_attr_name, before_dem_granu = attr_name, u_bound-l_bound
        # if not attribute_name in attributes_involved:
        # From the sqls, it seems the where condition acts on same granularity
        if before_dem_attr_name in attributes_involved:
            attr_ind = attributes_involved.index(before_dem_attr_name)
            attr_gran = attribute_granularities[attr_ind]
            before_dem_granu = math.gcd(attr_gran, int(before_dem_granu))
            attribute_granularities[attr_ind] = before_dem_granu
        attributes_involved.append(before_dem_attr_name)
        attribute_granularities.append(before_dem_granu)
        attribute_margins.append(condition_tuples)
        if after_splitpt:
            if after_dem_attr_name in attributes_involved:
                attr_ind = attributes_involved.index(after_dem_attr_name)
                attr_gran = attribute_granularities[attr_ind]
                after_dem_granu = math.gcd(attr_gran, int(after_dem_granu))
                attribute_granularities[attr_ind] = after_dem_granu
            attributes_involved.append(after_dem_attr_name)
            attribute_granularities.append(after_dem_granu)
            attribute_margins.append(condition_tuples)
    return attributes_involved, attribute_margins, attribute_granularities

def nyc_taxi_dataset_load_info_tuple_lists(full_mode=False):
    df_plain = pd.read_csv('dataset/flight_dataset.csv')
    state_map = {}
    df, _ = encode_dataset(df_plain, binary_encoding=False, class_name=None)
    for i in range(len(df)):
        state_map[df_plain['ORIGIN_STATE_ABR'][i]] = df['ORIGIN_STATE_ABR'][i]
    workloads = get_all_workloads()
    if not full_mode: workloads = workloads[:10]
    attribute_domains = {
        'YEAR_DATE': ((1995, 2017), 2),
        'UNIQUE_CARRIER': ((0, 26), 2),
        'ORIGIN': ((0, 361), 2),
        'ORIGIN_STATE_ABR': ((0, 53), 2),
        'DEST': ((0, 362), 2),
        'DEST_STATE_ABR': ((0, 53), 2),
        'DEP_DELAY': ((1092, 2646), 2),
        'TAXI_OUT': ((0, 1450), 2),
        'TAXI_IN': ((0, 1400), 2),
        'ARR_DELAY': ((880, 2230), 2),
        'AIR_TIME': ((480, 1770), 2),
        'DISTANCE': ((0, 5264), 2)
    }
    workload_margin_lists, attribute_granularities, per_workload_granularities = parse_workloads(df, workloads,
                                                                                                 attribute_domains,
                                                                                                 state_map)
    return df, workload_margin_lists, attribute_domains, attribute_granularities, per_workload_granularities

if __name__ == '__main__':
    df = pd.read_csv('multi-attribute_queries/flight_dataset.csv')
    # df = pd.read_csv('dataset/adult.csv')
    # df.insert(0, 'id', range(1, 1 + len(df)))
    df, _ = encode_dataset(df, binary_encoding=False, class_name=None)
    workloads = get_all_workloads()
    attribute_arities = {
        'YEAR_DATE': 2,
        'UNIQUE_CARRIER': 2,
        'ORIGIN': 2,
        'ORIGIN_STATE_ABR': 2,
        'DEST': 2,
        'DEST_STATE_ABR': 2,
        'DEP_DELAY': 2,
        'TAXI_OUT': 2,
        'TAXI_IN': 2,
        'ARR_DELAY': 2,
        'AIR_TIME': 2,
        'DISTANCE': 2
    }
    infotuplelists = parse_workloads(df, workloads, attribute_arities)
    # print(df.head())
    # df1 = sqldf('SELECT cast(TAXI_OUT/26.0 as int) AS bin_TAXI_OUT,COUNT(*) as count FROM df GROUP BY bin_TAXI_OUT', locals())
    # print(df1.head())