import math
import pickle
import multiprocessing
from cachedp.api.util_fns import *
from pioneer.Cache import PioneerCache
from cachedp.api.data_vector import DataVector
from cachedp.algorithm.main import init, answer_queries
from apex.run_query import apex_answer_query
from pioneer.main import answer_queries_pioneer

def synthetic_experiment(params):
    (args, answer_queries_pioneer, apex_answer_query, answer_queries, rng, infoTupleList, data_vector,
     raw_data_vector, domain_size, run, apex_logger, cache_dp_logger, ablation) = params
    cache = PioneerCache(raw_data_vector)
    if ablation:
        cache_rp_off, cache_se_off, cache_pr_off = init(False), init(False), init(False)
        cache_rp_off.set_cur_data_vector(data_vector)
        cache_se_off.set_cur_data_vector(data_vector)
        cache_pr_off.set_cur_data_vector(data_vector)
        cache_rp_off.add_full_data_vector(data_vector, ['attr'])
        cache_se_off.add_full_data_vector(data_vector, ['attr'])
        cache_pr_off.add_full_data_vector(data_vector, ['attr'])
    else:
        cache_sys = init(False)
        cache_sys.set_cur_data_vector(data_vector)
        cache_sys.add_full_data_vector(data_vector, ['attr'])
    res_pioneer, res_cache_dp, res_apex, res_apex_cache = [], [], [], []
    res_rp_off, res_se_off, res_pr_off = [], [], []
    for q_ind in range(500):
        s_start = int(rng.normal(domain_size / 2, 10))
        while s_start < 0 or s_start >= domain_size:
            s_start = int(rng.normal(domain_size / 2, 10))
        l_range = int(rng.normal(320, 10))
        while l_range < 0:
            l_range = int(rng.normal(320, 10))

        if s_start + l_range <= domain_size:
            query = [(s_start, s_start + l_range)]
        else:
            query = [(s_start, domain_size)]

        target_variance = rng.normal(250000, 25000)
        while target_variance <= 0:
            target_variance = rng.normal(250000, 25000)

        b = math.sqrt(target_variance / 2)
        alpha = (-b) * math.log(args.beta, math.e)

        if not ablation:
            cur_res_cache_dp = answer_queries(args, None, alpha, infoTupleList, data_vector,
                                              [(query, ['attr'])], cache_sys, cache_dp_logger, rng, target_variance)
            res_cache_dp.append(cur_res_cache_dp)

            cur_res_pioneer = answer_queries_pioneer(args, None, alpha, infoTupleList, data_vector,
                                                     [(query, ['attr'])], cache, cache_dp_logger, rng, target_variance)
            res_pioneer.append(cur_res_pioneer)
            cur_res_apex = apex_answer_query(args, None, alpha, infoTupleList, data_vector,
                                             [(query, ['attr'])], None, apex_logger, rng, target_variance)
            cur_res_apex_cache = apex_logger.get_apex_query_responses()[-1]
            res_apex.append(cur_res_apex)
            res_apex_cache.append(cur_res_apex_cache)
        else:
            args.relax_privacy = 0
            cur_res_rp_off = answer_queries(args, None, alpha, infoTupleList, data_vector,
                                            [(query, ['attr'])], cache_rp_off, cache_dp_logger, rng, target_variance)
            res_rp_off.append(cur_res_rp_off)
            args.relax_privacy, args.strategy_expander = 1, 0
            cur_res_se_off = answer_queries(args, None, alpha, infoTupleList, data_vector,
                                            [(query, ['attr'])], cache_se_off, cache_dp_logger, rng, target_variance)
            res_se_off.append(cur_res_se_off)
            args.strategy_expander, args.proactive = 1, 0
            cur_res_pr_off = answer_queries(args, None, alpha, infoTupleList, data_vector,
                                            [(query, ['attr'])], cache_pr_off, cache_dp_logger, rng, target_variance)
            res_pr_off.append(cur_res_pr_off)
            args.proactive = 1

    if ablation:
        with open('results/synthetic/result_rp_off_{}.p'.format(run), 'wb') as file:
            pickle.dump(res_rp_off, file)

        with open('results/synthetic/result_se_off_{}.p'.format(run), 'wb') as file:
            pickle.dump(res_se_off, file)

        with open('results/synthetic/result_pr_off_{}.p'.format(run), 'wb') as file:
            pickle.dump(res_pr_off, file)
    else:
        with open('results/synthetic/result_{}.p'.format(run), 'wb') as file:
            pickle.dump(res_cache_dp, file)

        with open('results/synthetic/pioneer_result_{}.p'.format(run), 'wb') as file:
            pickle.dump(res_pioneer, file)

        with open('results/synthetic/apex_result_{}.p'.format(run), 'wb') as file:
            pickle.dump(res_apex, file)

        with open('results/synthetic/apex_cache_result_{}.p'.format(run), 'wb') as file:
            pickle.dump(res_apex_cache, file)


def run_synthetic_experiment(args, df, domain_size, raw_data_vector, ablation=False):
    jobs = []
    if not os.path.exists('results/synthetic'):
        os.makedirs('results/synthetic')
    infoTupleList = [((0, domain_size), 2, 1, 'attr')]
    data_vector = DataVector(df, ['attr'], {'attr': ((0, domain_size), 2)}, [1])
    for run in range(args.runs):
        rng = np.random.default_rng(seed=run)
        apex_logger, cache_dp_logger = Logger(), Logger()
        params = (args, answer_queries_pioneer, apex_answer_query, answer_queries, rng, infoTupleList,
                  data_vector, raw_data_vector, domain_size, run,
                  apex_logger, cache_dp_logger, ablation)
        jobs.append(params)
    pool = multiprocessing.Pool(processes=args.number_workers)
    pool.map(synthetic_experiment, jobs)
    pool.close()
    pool.join()