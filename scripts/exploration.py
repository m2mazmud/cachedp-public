import os
import numpy as np
import multiprocessing
from cachedp.api.util_fns import *
from cachedp.algorithm.main import answer_queries
from cachedp.api.data_vector import DataVector
from apex.run_query import apex_answer_query
from pioneer.main import answer_queries_pioneer
from cachedp.api.RangeTupleHelpers import *
#
#
"""Simulates data exploration process by the analyst.
Returns an array of epsilons used by all algorithms at each level.
For the base case (root), input an empty response.
"""
def exploration(answer_queries_funcs, args, threshold, caches, data_vector, infoTupleList,
                loggers, rng, client, interval_length, is_correlated=False):
    attr_list_only = [x[-1] for x in infoTupleList]
    prior_workloads = [None] * len(answer_queries_funcs)
    prior_responses = [None] * len(answer_queries_funcs)
    responses = [None] * len(answer_queries_funcs)
    for func_ind in range(len(answer_queries_funcs)):
        if answer_queries_funcs[func_ind] is not None and not client.get_system_complete(func_ind):
            prior_workload = client.get_prior_workloads(func_ind)
            prior_response = client.get_prior_responses(func_ind)
            alpha = client.get_current_alpha()
            if prior_response.size == 0:    
                (true_response, prior_responses[func_ind], eps_used, module_used, strategy_rows, runtime, cache_size,
                 b_min, b_p) = answer_queries_funcs[func_ind](args,
                                                              unmultiplied_alpha=client.get_unmultiplied_alpha(),
                                                              alpha=alpha,
                                                              infoTupleList=infoTupleList,
                                                              data_vector=data_vector,
                                                              workload_tuples=prior_workload,
                                                              cache=caches[func_ind],
                                                              logger=loggers[func_ind],
                                                              rng=rng,
                                                              is_correlated=is_correlated)
                prior_workloads[func_ind] = prior_workload
                responses[func_ind] = (true_response, prior_responses[func_ind], eps_used, module_used,
                                       strategy_rows, runtime, cache_size, b_min, b_p, alpha)
            else:
                remaining_queries = []
                for (query, response) in zip(prior_workload, prior_response):
                    if response >= threshold:
                        remaining_queries.append(query)

                new_queries = list()
                # Split on remaining workloads
                for query in remaining_queries:
                    sets_of_children_ranges = []
                    for tup, infoTuple in zip(query[0], infoTupleList):
                        children_ranges = get_children_ranges_helper(infoTuple[1], infoTuple[2], tup[0], tup[1])
                        if children_ranges == []: children_ranges = [tup]
                        sets_of_children_ranges.append(children_ranges)

                    own_children_ranges = cross_product_for_marginals(sets_of_children_ranges)
                    if len(own_children_ranges) > 1:
                        new_queries += [(x, attr_list_only) for x in own_children_ranges]

                # We've reached the finest granularity for all queries or No queries pass the threshold - go no further
                if len(new_queries) <= 1:
                    client.set_system_complete(func_ind)
                    continue

                (true_response, prior_responses[func_ind], eps_used, module_used, strategy_rows, runtime, cache_size,
                 b_min, b_p) = answer_queries_funcs[func_ind](args,
                                                              unmultiplied_alpha=client.get_unmultiplied_alpha(),
                                                              alpha=alpha,
                                                              infoTupleList=infoTupleList,
                                                              data_vector=data_vector,
                                                              workload_tuples=new_queries,
                                                              cache=caches[func_ind],
                                                              logger=loggers[func_ind],
                                                              rng=rng,
                                                              is_correlated=is_correlated)
                prior_workloads[func_ind] = new_queries
                responses[func_ind] = (true_response, prior_responses[func_ind], eps_used, module_used,
                                       strategy_rows, runtime, cache_size, b_min, b_p, alpha)
    client.set_prior_responses(prior_responses)
    client.set_prior_workloads(prior_workloads)
    return responses

# usecase: we are looking for a granularity that is within a threshold value
# example: suppose we have a number of vaccines in stock, and we want to find a group of people in an age range
# that have a total count below the number of vaccines.
# Note that the level of the root is 1
def exploration_depth_first(answer_queries_funcs, args, threshold, caches, data_vector,
                            infoTupleList, loggers, rng, client, interval_length, is_correlated=False):
    attr_list_only = [x[-1] for x in infoTupleList]
    prior_workloads = [None] * len(answer_queries_funcs)
    prior_responses = [None] * len(answer_queries_funcs)
    responses = [None] * len(answer_queries_funcs)
    levels = [0] * len(answer_queries_funcs)
    for func_ind in range(len(answer_queries_funcs)):
        if answer_queries_funcs[func_ind] is not None and not client.get_system_complete(func_ind):
            prior_workload = client.get_prior_workloads(func_ind)
            prior_response = client.get_prior_responses(func_ind)
            branching_workload = client.get_dfs_branching_workload(func_ind)
            level = client.get_dfs_level(func_ind)
            alpha = client.get_current_alpha()
            if prior_response.size == 0:
                (true_response, prior_responses[func_ind], eps_used, module_used, strategy_rows, runtime, cache_size,
                 b_min, b_p) = answer_queries_funcs[func_ind](args,
                                                              unmultiplied_alpha=client.get_unmultiplied_alpha(),
                                                              alpha=alpha,
                                                              infoTupleList=infoTupleList,
                                                              data_vector=data_vector,
                                                              workload_tuples=prior_workload,
                                                              cache=caches[func_ind],
                                                              logger=loggers[func_ind],
                                                              rng=rng,
                                                              is_correlated=is_correlated)
                prior_workloads[func_ind] = prior_workload
                responses[func_ind] = (true_response, prior_responses[func_ind], eps_used, module_used,
                                       strategy_rows, runtime, cache_size, b_min, b_p, alpha)
            else:
                if level not in branching_workload.keys(): branching_workload[level] = []
                workload_responses = sorted(zip(prior_workload, prior_response), key=lambda x: x[1])
                _, sorted_responses = zip(*workload_responses)
                workload_response_pair = []
                found_outlier = False
                for i in range(len(workload_responses)):
                    query = workload_responses[i][0]
                    if sorted_responses[i] >= threshold and sorted_responses[i] < threshold + interval_length:
                        found_outlier = True
                        client.set_system_complete(func_ind)
                        break
                    elif sorted_responses[i] >= threshold + interval_length:
                        for tup, infoTuple in zip(query[0], infoTupleList):
                            children_ranges = get_children_ranges_helper(infoTuple[1], infoTuple[2], tup[0], tup[1])
                            if len(children_ranges) > 0:
                                workload_response_pair.append(workload_responses[i])
                                break
                if found_outlier: continue
                if len(workload_response_pair) > 0: 
                    expanding_query = workload_response_pair[0][0]
                    workload_response_pair.pop(0)
                    for pair in workload_response_pair:
                        branching_workload[level].append(pair)
                    client.set_dfs_branching_workload(func_ind, branching_workload)
                    level += 1
                else:
                    is_remaining = False
                    for key in branching_workload.keys():
                        if len(branching_workload[key]) > 0 and key < level:
                            is_remaining = True
                    # No branching node left, so we explored everything but none of them works
                    if not is_remaining:
                        client.set_system_complete(func_ind)
                        continue
                    # Branch to a random level in branching_workload
                    else:
                        random_level = rng.integers(2, level)
                        while len(branching_workload[random_level]) == 0:
                            random_level = rng.integers(2, level)
                        level = random_level + 1
                        # select the branching node and clear it from available nodes
                        available_pairs = sorted(branching_workload[random_level], key=lambda x: x[1])
                        expanding_query = available_pairs[0][0]
                        branching_workload[random_level] = available_pairs[1:]
                        client.set_dfs_branching_workload(func_ind, branching_workload)

                # Find the next queries
                new_queries = list()

                sets_of_children_ranges = []
                for tup, infoTuple in zip(expanding_query[0], infoTupleList):
                    children_ranges = get_children_ranges_helper(infoTuple[1], infoTuple[2], tup[0], tup[1])
                    if children_ranges == []: children_ranges = [tup]
                    sets_of_children_ranges.append(children_ranges)

                own_children_ranges = cross_product_for_marginals(sets_of_children_ranges)
                new_queries += [(x, attr_list_only) for x in own_children_ranges]
                assert(len(new_queries) > 0)

                (true_response, prior_responses[func_ind], eps_used, module_used, strategy_rows, runtime, cache_size,
                 b_min, b_p) = answer_queries_funcs[func_ind](args,
                                                              unmultiplied_alpha=client.get_unmultiplied_alpha(),
                                                              alpha=alpha,
                                                              infoTupleList=infoTupleList,
                                                              data_vector=data_vector,
                                                              workload_tuples=new_queries,
                                                              cache=caches[func_ind],
                                                              logger=loggers[func_ind],
                                                              rng=rng,
                                                              is_correlated=is_correlated)
                prior_workloads[func_ind] = new_queries
                responses[func_ind] = (true_response, prior_responses[func_ind], eps_used, module_used,
                                       strategy_rows, runtime, cache_size, b_min, b_p, alpha)
            levels[func_ind] = level
    client.set_prior_responses(prior_responses)
    client.set_prior_workloads(prior_workloads)
    client.set_dfs_levels(levels)
    return responses

def generate_dirs(args, run, exp, ablation, vary_hyper):
    dirs = [None, None, None, None]
    if ablation:
        if args.relax_privacy == 0:
            dirs[0] = 'results/' + exp + '/result_rp_off_{}.p'.format(run)
        elif args.strategy_expander == 0:
            dirs[0] = 'results/' + exp + '/result_se_off_{}.p'.format(run)
        else:
            dirs[0] = 'results/' + exp + '/result_pr_off_{}.p'.format(run)
    elif vary_hyper:
        dirs[0] = 'results/' + exp + '/result_se_{}_{}.p'.format(args.se_parameter, run)
    else:
        dirs[0] = 'results/' + exp + '/result_{}.p'.format(run)
        dirs[1] = 'results/' + exp + '/apex_result_{}.p'.format(run)
        dirs[2] = 'results/' + exp + '/apex_cache_result_{}.p'.format(run)
        dirs[3] = 'results/' + exp + '/pioneer_result_{}.p'.format(run)

    return dirs

def run_exploration(params):
    (answer_queries_funcs, func, exp, args, infoTupleList, data_vector, run,
     rng, clients, loggers, ablation, varyhyper) = params
    ind = None
    while True:
        ind, client, threshold, interval_length = clients.get_incomplete_client(rng)
        if isinstance(client, str): break
        caches = clients.get_system_caches()
        result = func(answer_queries_funcs, args, threshold, caches, data_vector, infoTupleList,
                      loggers, rng, client, interval_length, args.is_correlated)
        clients.set_exploration_responses(result)
        clients.set_client(client, ind)
        clients.set_system_caches(caches)

    if answer_queries_funcs[1] is not None:
        clients.set_exploration_response_apex_with_cache(loggers[1].get_apex_query_responses())
    dirs = generate_dirs(args, run, exp, ablation, varyhyper)
    clients.save_exploration_response(dirs)

def run_experiment(args, df, exp, ablation=False, varyhyper=False):
    jobs = []
    domains = {'age': ((17, 91), 2), 'country': ((0, 41), 2)}
    system_selections = [answer_queries, apex_answer_query, answer_queries_pioneer]
    if ablation or varyhyper:
        system_selections = [answer_queries, None, None]
    if not os.path.exists('results/' + exp):
        os.makedirs('results/' + exp)
    infoTupleList = [(domains[args.attribute][0], 2, 1, args.attribute)]
    data_vector = DataVector(df, [args.attribute], domains, [1])
    prior_workload = [([domains[args.attribute][0]], [args.attribute])]
    df_size = len(df)
    for run in range(args.runs):
        loggers = [Logger(), Logger(), Logger()]
        rng = np.random.default_rng(seed=run + args.seed_offset)
        clients = MultipleClients(args, system_selections, prior_workload, df_size, [args.attribute],
                                  data_vector, args.is_correlated)
        params = (system_selections, exploration_depth_first, exp, args, infoTupleList, data_vector,
                  run,
                  rng,
                  clients,
                  loggers,
                  ablation,
                  varyhyper)
        jobs.append(params)
    pool = multiprocessing.Pool(processes=args.number_workers)
    pool.map(run_exploration, jobs)
    pool.close()
    pool.join()